---
jupyter:
  jupytext:
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.13.8
  kernelspec:
    display_name: Python 3 (ipykernel)
    language: python
    name: python3
---

```python
from __future__ import print_function
import qpc_exp2sim.tools.data_handling as dh
from qpc_exp2sim.tools import plotting_sim, plotting_exp
import numpy as np
from ipywidgets import interact, interactive, fixed, interact_manual
import ipywidgets as widgets
import matplotlib.pyplot as plt
%matplotlib notebook
```

```python
from qpc_exp2sim.tools.data_handling import read_file
```

```python
filename = 'files_data/data_1D_mK.csv'
```

1D measurements are only performed on 'Y3' samples. The following combinations can be made:
* X1 - Y3: A samples
* X2 - Y3: B samples
* X5 - Y3: A samples
* X6 - Y3: C samples


## 1) Comparison subtraction series resistance

```python
limits_v = [-2.6,-0.8]
qpc = 2

def f(X):    
    fig, ax = plt.subplots()
    plotting_exp.plot_1D_map(filename, X, 'Y3', qpc_n = [qpc], hlines=np.arange(8), limits_v=limits_v, 
                             label=['exp with series R'], subtract_resistance=False, ax = ax)
    plotting_exp.plot_1D_map(filename, X, 'Y3', qpc_n = [qpc], hlines=np.arange(8), limits_v=limits_v, 
                             label=['exp series R subtracted'], subtract_resistance=True, ax = ax)
    plt.show()
    
interact(f, X = widgets.Dropdown(options=[('X1 - A samples', 'X1'), 
                                          ('X2 - B samples', 'X2'), 
                                          ('X5 - A samples', 'X5'), 
                                          ('X6 - C samples', 'X6')], 
                                 description='X coordinate in format X1 to X6:'), 
                                 value=('X2 - B samples', 'X2'))
```

## 2) Plotting conductance ifo Vg for sets of qpc's

```python
def f(X):
    plotting_exp.plot_1D_map(filename, X, 'Y3', qpc_n = [1,2,3,4], hlines=np.arange(8), subtract_resistance=False
                            )#,limits_v=[-2.4,-0.8])
    
interact(f, X = widgets.Dropdown(options=[('X1 - A samples', 'X1'), 
                                          ('X2 - B samples', 'X2'), 
                                          ('X5 - A samples', 'X5'), 
                                          ('X6 - C samples', 'X6')], 
                                 description='X coordinate in format X1 to X6:'), 
                                 value=('X2 - B samples', 'X2'))
```

## 3) Comparison experiment and simulation

```python
widget_shape = widgets.Dropdown(options=['a', 'b'], description='qpc shape:', value='a')
widget_n = widgets.Dropdown(options=range(1,5), description='qpc number:', value=1)

def get_files(shape, n):
    file_geom = '{}{}_grid10'.format(shape, n)
    
    files_sim = ['V128_{}{}_pi_bulk'.format(shape, n), 
                 'V128_{}{}_nr_lat_m100_it5'.format(shape, n)]
                 #'V128_{}{}_fsc_nr_m100_it5'.format(shape, n)]
    return ['files_data/' + file_geom + '_' + file for file in files_sim]
    
labels = ['sim: pi_bulk', 'sim: nr_lat', 'sim: nr_fsc']
def f(shape, n):
    fig, ax = plt.subplots()
    
    limits = [0,0.8]

    for i, file in enumerate(get_files(shape, n)):
        data = dh.read_file(file, file_type = 'simulation')

        V_gates_list= data.p_sim['V_gates_list']
        conductance = data.conductance.flatten()
        V3 = data.V3[1]
        plotting_sim.plot_conductance(V_gates_list, conductance, V3=V3, linestyle='-', 
                     hlines=np.arange(6), limits_v=limits, label=[labels[i]], ax=ax)

    # For the a type qpcs there are multiple devices with the same geometry. The experiemental data for both 
    # types is plotted.
    X_all = {'a':['X1','X5'], 'b':['X2']}[shape]
    #X_all = {'a':['X1'], 'b':['X2']}[shape]
    
    for X in X_all:
        #plotting_exp.plot_1D_map(filename, X, 'Y3', qpc_n = n, hlines=np.arange(8), subtract_resistance=False,
        #            limits_v=limits, label=['exp with series R'], V3_zero = True, ax =ax, linestyle='--')
        plotting_exp.plot_1D_map(filename, X, 'Y3', qpc_n = n, hlines=np.arange(8), subtract_resistance=True, 
                    limits_v=limits, label=['exp: sample {}Y3'.format(X)], V3_zero = True, ax=ax, linestyle='--')
    
    ax.set_title('Conductance QPC {}{}'.format(shape, n))
    plt.show()
    
    fig.savefig('conductance_{}{}.pdf'.format(shape, n))
    
    
interact(f, shape=widget_shape, n=widget_n)
```

## 4) Current ifo top and bottom gate voltage

```python
filename_2D = 'data_2D_mK_TB.csv'

plotting_exp.plot_2D_map(filename_2D, 'X1', 'Y3')
```

```python

```
