---
jupyter:
  jupytext:
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.13.8
  kernelspec:
    display_name: Python 3 (ipykernel)
    language: python
    name: python3
---

```python
from __future__ import print_function
from ipywidgets import interact, interactive, fixed, interact_manual
import ipywidgets as widgets
import matplotlib.pyplot as plt
from matplotlib.lines import Line2D
import numpy as np
from scipy.optimize import curve_fit, fsolve, approx_fprime
from scipy.interpolate import interp1d
from scipy import signal
import scipy.constants as c
from functools import partial


import qpc_exp2sim.tools.data_handling as dh
import qpc_exp2sim.tools.plotting_sim as plotting_sim
from qpc_exp2sim.pescado_kwant import plotting_pes, extension_pes
from qpc_exp2sim.simulation import solver

%matplotlib notebook

files_a_bulk = ['files_data/{0}_grid10_V128_{0}_pi_bulk'.format(file) for file in ['a1', 'a2', 'a3', 'a4']]
files_b_bulk = ['files_data/{0}_grid10_V128_{0}_pi_bulk'.format(file) for file in ['b1', 'b2', 'b3', 'b4']]

files_a_fsc = ['files_data/{0}_grid10_V128_{0}_fsc_nr_m100_it5'.format(file) for file in ['a1', 'a2', 'a3']]
files_b_fsc = ['files_data/{0}_grid10_V128_{0}_fsc_nr_m100_it5'.format(file) for file in ['b1', 'b2', 'b3', 'b4']]
```

## Conductance with linear fit

### Linear fit of the conductance

```python
file = files_a_fsc[0]

data = dh.read_file(file)
V_gates_list = data.p_sim['V_gates_list']
conductance = data.conductance
plotting_sim.linear_fit_conductance(V_gates_list, conductance.flatten(), show_slope=False)
```

### Slope for each conductance step

```python
plt.figure()

files = files_a_fine + files_b_fine

for file in files:
    data = dh.read_file(file)
    V_gates_list = data.p_sim['V_gates_list']
    conductance = data.conductance.flatten()
    _, _, infliction_points = solver.crossings(V_gates_list, conductance)
    plt.plot(np.arange(len(infliction_points[:,0])) + 1, infliction_points[:,2], 
             label = data.p_geom['qpc_name'])

plt.xlabel('step nb')
plt.ylabel('slope')
#plt.xlim(1,6)
plt.legend()
plt.show()

plt.figure()
for file in files:
    data = dh.read_file(file)
    V_gates_list = data.p_sim['V_gates_list']
    conductance = data.conductance.flatten()
    _, _, infliction_points = solver.crossings(V_gates_list, conductance)
    plt.plot(np.arange(len(infliction_points[:,0])) + 1, infliction_points[:,2]/infliction_points[0,2], 
             label = data.p_geom['qpc_name'])

plt.xlabel('step nb')
plt.ylabel('slope/initial slope')
#plt.xlim(1,6)
plt.legend()
plt.show()
```

### 'Plateau width' and 'rise width'

```python
#%matplotlib notebook
file = 'a1_grid10_fine'

colors = ['tab:blue', 'tab:orange', 'tab:green', 'tab:red', 'tab:purple', 'tab:brown', 
          'tab:pink', 'tab:gray', 'tab:olive', 'tab:cyan']

fig, ax = plt.subplots()

for index, file in enumerate(files):
    data = dh.read_file(file)
    V_gates_list = data.p_sim['V_gates_list']
    conductance = data.conductance.flatten()
    _, cross_points, _ = solver.crossings(V_gates_list, conductance)
    width = cross_points[1:,0] - cross_points[:-1, 0]

    width_plateau = width[::2]
    width_rise = width[1::2]
    ax.plot(np.arange(len(width_rise))+1, width_rise, color = colors[index], label=data.p_geom['qpc_name'])
    ax.plot(np.arange(len(width_plateau))+1, width_plateau, linestyle = '--', color = colors[index])

# 2nd custom legend
ax_legend = ax.twinx()
ax_legend.set_yticks([])
custom_lines = [Line2D([0], [0], linestyle='--', color='k', label='width rise'),
                Line2D([0], [0], linestyle='-', color='k', label='width plateau')]
ax_legend.legend(handles = custom_lines, loc='lower right')
    
ax.set_ylabel('width [V]')
ax.set_xlabel('step number [-]')
#plt.xlim(1,6)
ax.legend()
plt.show()
```

## Butikker fit
Based on the paper: Quantized transmission of a saddle point-constriction


### Playing with the model

```python
V0 = 0
V = np.linspace(0,12,100)

widget_wx = widgets.IntSlider(value=1e15, min=0, max=10e15, step=1e15, continuous_update=False, description=r'$\omega_x$:')
widget_wy = widgets.IntSlider(value=3e15, min=0, max=10e15, step=1e15, continuous_update=False, description=r'$\omega_y$:')
widget_n = widgets.IntSlider(value=4, min=1, max=10, step=1, continuous_update=False, description=r'$n$:')
widget_E = widgets.IntSlider(value=8, min=0, max=30, step=1, continuous_update=False, description=r'$E/e$:')
widget_V0 = widgets.IntSlider(value=0, min=0, max=10, step=1, continuous_update=False, description=r'$V0$:')

#The file below is just used to obtain a coord array
data = dh.read_file(files_a_bulk[1])
coord = data.coord
cell_area = np.product(data.p_geom['grid'][0:-1]) * 1e-18
index_2DEG = np.where(coord[:,2] == 0)
coord_2DEG = coord[index_2DEG][:,0:2]

```

```python
def u_buttiker(pos, wx, wy, V0):
    """
    V0: float
        Treated as a voltage, this is a bit different than V0 in the buttiker paper, where it is
        an energy.
    """
    m_rel = 0.067
    m = m_rel*c.electron_mass
    
    x = pos[:,0] * 1e-9
    y = pos[:,1] * 1e-9
    
    result = (-V0*c.elementary_charge) - (1/2 * m * wx**2 * x**2) + (1/2 * m * wy**2 * y**2)
    return result


def T(E_scaled, wx, wy, n):
    """
    n: list 
        wanted steps
    V: np.ndarray
        range
    """

    def en(E_scaled, wx, wy, n):
        """
        E_scaled : np.ndarray
            (E - V0*e) / (hbar*wx)
        wx : float
        wy : float
        n : float
        """
        return 2 * (E_scaled - (wy/wx) * (n+1/2))

    def T(en):
        return 1/(1+ np.exp(-c.pi * en))

    return np.sum(np.array([T(en(E_scaled, wx, wy, n_val)) for n_val in n]), axis=0)


def trans_buttiker(wx, wy, n):
    print(r'wy / wx = ', wy/wx)
    print("Colormap doesn't show voltage but potential in .. (J?) -> potential well where the e- flow")
    plotting_pes.colormap(coord_2DEG, u_buttiker(coord_2DEG, wx=wx, wy=wy, V0=V0))
    
    E_scaled_max = (wy / wx) * n
    E_scaled = np.linspace(0, E_scaled_max, 100)
    n=np.arange(n)
        
    plt.figure()
    plt.plot(E_scaled, T(E_scaled, wx, wy, n))
    
    setting_lines = {'color':'k', 'linestyle':'--', 'linewidth':0.5}
    vertical_lines = (wy/wx) * (n+1/2)
    for line in vertical_lines:
        plt.axvline(line, **setting_lines)
        
    plt.ylabel(r'conductance $[2e^2/h]$') #Notice that in the buttiker paper they only mention e^2/h and not 2e^2/h
    plt.xlabel(r'$(E-V_0)/\hbar\omega_x$') #Notice that in the buttiker paper they only mention e^2/h and not 2e^2/h
    plt.show()
    
interact(trans_buttiker, wx=widget_wx, 
                    wy=widget_wy,
                    n = widget_n,
                    E = widget_E, 
                     V0 = widget_V0)
```

To get an idea about the value of wx we approximate e/(hbar*wx) ~ 1 (see x axis, V0 is an energy!)


### Fitting with the current simulation data

```python
def vg2energy(vg, V_gates_list, coord, voltage):
    """
    vg : float
    
    returns energy in meV
    """
    _, V0 = plotting_pes.data_cross_section(coord, voltage, [0,0,0])
    U0 = V0.flatten() * (-1e3) # To have the potential in meV
    
    f = interp1d(V_gates_list, U0, kind='cubic', fill_value='extrapolate')
    
    U0_filtered = f(vg)
    E_diff = 0-U0_filtered
    return E_diff

def split_steps(V_gates_list, conductance, voltage, min_count=10):
    """
    Splits the conductance data in separate arrays for each step (starting at 0, ending at 1).
    """
    
    split_index = np.searchsorted(conductance.flatten(), 
                                  np.arange(1, int(np.max(conductance))+1))
    
    splitted_V_gates_list = [elem for elem in np.split(V_gates_list, split_index) 
                            if len(elem) >= min_count]
        
    splitted_conductance = [elem -i for i, elem in enumerate(np.split(conductance, split_index))
                            if len(elem) >= min_count]
        
    splitted_voltage = [elem for elem in np.split(voltage, split_index) 
                            if len(elem) >= min_count]
    
    return splitted_V_gates_list, splitted_conductance, splitted_voltage


def T_fit_step(fixed, wx, wy):
    """
    fixed : np.ndarray
        containing E_diff = E-U0, but with as last element and integer for n !
    wx : float
    wy : float
    """
    E_diff = fixed[:-1]
    n = fixed[-1]
    en = 2 * (E_diff/(c.hbar*wx) - (wy/wx) * (n+1/2))
    T = 1/(1+ np.exp(-c.pi * en))
    return T 


def fit_step(coord, V_gates_list, conductance, voltage, n, initial_val = [1e12, 1e12]):
    _, V0 = plotting_pes.data_cross_section(coord, voltage, [0,0,0])
    U0 = - V0.flatten() * c.elementary_charge
    E_diff = 0 - U0
    popt, pcov = curve_fit(T_fit_step, np.append(E_diff, n), conductance, p0=initial_val)
    wx, wy = popt
    
    print('Standard deviation on the estimate: ', np.sqrt(np.diag(pcov)))
    
    return wx, wy


def recombine_fitted_curve(coord, voltage, wx, wy):
    """
    voltage in splitted format!
    
    returns conductance in splitted format
    """
    
    conductance = []
    
    for n, (voltage_elem, wx, wy) in enumerate(zip(voltage, wx, wy)):
        _, V0 = plotting_pes.data_cross_section(coord, voltage_elem, [0,0,0])
        U0 = - V0.flatten() * c.elementary_charge
        E_diff = 0 - U0
        
        conductance.append(T_fit_step(np.append(E_diff, n), wx, wy))
    
    return conductance


def buttiker_fit_conductance(coord, V_gates_list, conductance, voltage, initial_val = [1e12, 1e12]):
    
    vg_split, cond_split, volt_split = split_steps(V_gates_list, conductance.flatten(), voltage)

    wx, wy = [], []
    for n in range(len(vg_split)):
        wx_, wy_ = fit_step(coord, vg_split[n], cond_split[n], volt_split[n], n, initial_val)
        
        # We reuse the obtained fit to initialize the next fit
        #initial_val = [wx_, wy_]
        wx.append(wx_)
        wy.append(wy_)

        print('wx: {:e}'.format(wx_))
        print('wy: {:e}'.format(wy_))
        print('wy/wx: {}'.format(wy_/wx_))


    cond_fitted_split = recombine_fitted_curve(coord, volt_split, wx, wy)
    
    return vg_split, cond_fitted_split, wx, wy
```

```python
data = dh.read_file(files_b_fsc[0])
#data = dh.read_file('files_data/b3_grid10_V128_b3_nr_lat_m100_it5')

# unpacking the data
p_geom = data.p_geom
p_sim = data.p_sim
V_gates_list = p_sim['V_gates_list']
coord = data.coord
voltage = data.voltage
charge = data.charge
conductance = data.conductance.flatten()
voltage_shift = data.voltage_shift
V3 = data.V3

# Get the coordinates from the 2DEG
index_2DEG = np.where(coord[:,2] == 0)
coord_2DEG = coord[index_2DEG][:,0:2]

# Get the unit conversion between pescado voltage and kwant energy
conversion_unit = c.elementary_charge / p_geom['t']

# Get the voltage in the center of the 2DEG ifo Vg
coord_0D, V0 = plotting_pes.data_cross_section(coord, voltage, [0,0,0])
V0 = V0.flatten()

U0 = - V0 * c.elementary_charge
E_diff = 0 - U0
```

```python
fig = plt.figure(figsize=(6,6))

ax1 = plt.subplot(221)
ax2 = plt.subplot(222)
ax3 = plt.subplot(224)

ax1.plot(conductance, E_diff, 'tab:orange')
ax3.plot(V_gates_list, conductance, color='tab:orange')
ax2.plot(V_gates_list, E_diff, color='tab:blue')

# ax3 = plt.subplot(224)
# ax2 = plt.subplot(222, sharex = ax3)
# ax1 = plt.subplot(221, sharey = ax2)

ax1.invert_xaxis()
ax3.invert_yaxis()

ax1.spines['top'].set_visible(False)
ax1.spines['left'].set_visible(False)

ax2.spines['top'].set_visible(False)
ax2.spines['right'].set_visible(False)

ax3.spines['right'].set_visible(False)
ax3.spines['bottom'].set_visible(False)

ax1.set_xlabel(r'$G~[2e^2/h]$')

ax2.set_ylabel(r'                            $E_F-U_0~[meV]$')

ax3.xaxis.tick_top()
ax3.xaxis.set_label_position('top') 
ax3.set_xlabel(r'$V_g$')
ax3.set_ylabel(r'$G~[2e^2/h]$')

ax1.set_yticks([])

ax1.set_xticklabels([])
ax1.set_yticklabels([])

ax2.set_xticklabels([])
ax2.set_yticklabels([])

ax3.set_xticklabels([])
ax3.set_yticklabels([])


index_v = 40

ax1.set_xlim(right = 0)
ax1.set_ylim(bottom = np.min(E_diff))

ax2.set_xlim(left=np.min(V_gates_list))
ax2.set_ylim(bottom=np.min(E_diff))

ax3.set_xlim(left=np.min(V_gates_list))
ax3.set_ylim(top=0)

# Plot line showing the non linear lever arm
linestyle = {'color':'k', 'linewidth':0.5, 'linestyle':'--'}

ax1.plot([0, conductance[index_v]], [E_diff[index_v], E_diff[index_v]], **linestyle)
ax2.plot([np.min(V_gates_list), V_gates_list[index_v], V_gates_list[index_v]], 
          [E_diff[index_v], E_diff[index_v], np.min(E_diff)], **linestyle)
ax3.plot([V_gates_list[index_v], V_gates_list[index_v]], [0, conductance[index_v]], **linestyle)

ax1.set_xticks(np.arange(7))
ax3.set_yticks(np.arange(7))

# Remove horizontal space between axes
fig.subplots_adjust(hspace=0, wspace=0)

fig.savefig('non_linear_lever_arm.pdf')
```

```python
fig, ax = plt.subplots()
plotting_sim.plot_conductance(V_gates_list, conductance, linestyle='.-',
                              hlines=np.arange(7), limits_v=None, ax =ax)
ax_buttiker = ax.twiny()
ax_buttiker.set_xlim(ax.get_xlim())

ax_buttiker.set_xticklabels(['{:.2}'.format(elem) for elem in 
                             vg2energy(ax_buttiker.get_xticks(), V_gates_list, coord, voltage)])

ax_buttiker.set_xlabel(r'$(E-U_0)$ [meV]')

plt.show()

# Notice that the top scale is not linear!
```

```python
fig, ax = plt.subplots()
U0 = V0 * (-1e3) # To have the potential in meV
ax.plot(V_gates_list, 0-U0)
ax.set_xlabel(r'$V_g$ [V]')
ax.set_ylabel('$E_F - U_0 [meV]$')
ax.set_title('Non linear lever arm \n determined at center site (= E - V0 in the buttiker paper)')
ax.axhline(0, linewidth = 0.5, linestyle = '--', color='k')

ax.set_ylim(bottom=-0.20)
plt.show()
```

```python
# Get the fitted data
V_gates_list_split, conductance_fit_split, wx, wy = buttiker_fit_conductance(
    coord, V_gates_list, conductance, voltage)

# Get the points of maximum derivative in the conductance
_, _, infliction_points = plotting_sim.crossings(V_gates_list, conductance.flatten())
infliction_points = infliction_points[:,0]
```

```python
fig, (ax1, ax2) = plt.subplots(2,1, figsize=(5,7))

# Figure containing the fitted conductance steps
ax1.plot(V_gates_list, conductance.flatten(), linestyle='-')

for n in range(len(V_gates_list_split)):
    ax1.plot(V_gates_list_split[n], conductance_fit_split[n] + n, '--', color='tab:orange')
    ax1.plot(V_gates_list_split[n], conductance_fit_split[n], '--', color='tab:orange')

ax1.legend(['simulation', 'fit'])
ax1.set_ylabel('conductance [2e²/h]')


# Figure containing information on the saddle point potential
ax2.plot(infliction_points, np.array(wx) * c.hbar/c.elementary_charge * 1000, '1', label=r'$\hbar\omega_x$')
ax2.plot(infliction_points, np.array(wy) * c.hbar/c.elementary_charge * 1000, 'x', label=r'$\hbar\omega_y$')
ax2.set_xlabel(r'$V_g$ [V]')
ax2.set_ylabel(r'$\hbar\omega$ [meV]')
ax2.legend(loc = 'lower left')

ax3 = ax2.twinx()
ax3.plot(infliction_points, np.array(wy) / np.array(wx), 'v', label=r'$\omega_y/\omega_x$',
         color='tab:green', mfc='none')
ax3.set_ylabel(r'$\omega_y/\omega_x$')
ax3.legend(loc = 'upper right')

xlim = [min(min(ax1.get_xlim()), min(ax2.get_xlim())), 
        max(max(ax1.get_xlim()), max(ax2.get_xlim()))]

ax1.set_xlim(*xlim)
ax2.set_xlim(*xlim)

ax1.set_xticklabels([])

plt.tight_layout()
plt.show()
```

```python
fig.savefig('buttiker_fit.pdf')
```

Shape of the conductance steps is not constant ifo Vg due to changing shape of the potential landscape which is 
quantified by the non constant values of wx and wy. Moreover we are dealing with a non linear lever arm providing the relation between the applied gate voltage and the potential at the center of the 2DEG.

```python
step_nb = 4

index = np.where(np.array(V_gates_list) == infliction_points[step_nb])[0][0]
print('The gate voltage is : {} V'.format(V_gates_list[index]))

# Making cross sections and obtaining buttiker data
_, voltage_2D = plotting_pes.data_cross_section(coord, voltage[index], section=[None, None, 0])
pot_2D_pes = voltage_2D.flatten() * (-conversion_unit)
pot_2D_buttiker = u_buttiker(coord_2DEG, wx=wx[step_nb], wy=wy[step_nb], V0=V0[index])/p_geom['t']

cbar_range = [np.min([pot_2D_pes, pot_2D_buttiker]), np.max([pot_2D_pes, pot_2D_buttiker])]

# Initiate the plot
f, (ax1, ax2) = plt.subplots(1, 2, figsize = (10,5), gridspec_kw={'width_ratios': [1, 1]})

# Plotting simulation from pescado
plotting_pes.colormap(coord_2DEG, pot_2D_pes, ax=ax1, aspect_equal=True, cbar_range=cbar_range, 
                           title='Pescado')

# Plotting fit from buttiker
plotting_pes.colormap(coord_2DEG, pot_2D_buttiker, ax=ax2, aspect_equal=True, cbar_range=cbar_range, 
                      title='Buttiker fit')

plt.tight_layout()
plt.show()
```

```python
fig, (ax1, ax2) = plt.subplots(1,2, figsize=(8, 4))

# The pescado potentials are taken at the gate voltage where the derivative of the conductance is maximal
indices = extension_pes.indices_from_coordinates(np.array(V_gates_list)[:,None], infliction_points[:,None])
pot_2D_buttiker = [u_buttiker(coord_2DEG, wx=wx[step_nb], wy=wy[step_nb], V0=V0[indices[step_nb]])/p_geom['t'] for 
                   step_nb in range(len(wx))]

# Making cross section (fixed y value)
coord_1D, vol_1D_pes = plotting_pes.data_cross_section(coord, voltage[indices], section=[None, 0, 0])
coord_1D, pot_1D_buttiker = plotting_pes.data_cross_section(coord_2DEG, pot_2D_buttiker, section=[None, 0])

pot_1D_pes = vol_1D_pes[:,:,0] * (-1e3)
pot_1D_buttiker = pot_1D_buttiker[:,:,0] * p_geom['t'] / c.elementary_charge * 1e3

ax1.plot(coord_1D, pot_1D_pes, '--', label='Pescado')
colors = [line.get_color() for line in ax1.lines]
for i, color in enumerate(colors):
    ax1.plot(coord_1D, pot_1D_buttiker[:,i], label='Buttiker fit', color=color)
ax1.set_xlabel(r'$x~[nm]$')
ax1.set_ylabel(r'$U~[meV]$')
ax1.set_title(r'$cross~section~at~y=0~nm$')

ax1.set_xlim(-150, 150)
ax1.set_ylim(-8,-1)

#ax1.legend()

# Making cross section (fixed x value)
coord_1D, vol_1D_pes = plotting_pes.data_cross_section(coord, voltage[indices], section=[0, None, 0])
coord_1D, pot_1D_buttiker = plotting_pes.data_cross_section(coord_2DEG, pot_2D_buttiker, section=[0, None])

pot_1D_pes = vol_1D_pes[:,:,0] * (-1e3)
pot_1D_buttiker = pot_1D_buttiker[:,:,0] * p_geom['t'] / c.elementary_charge * 1e3

for i, color in enumerate(colors):
    ax2.plot(coord_1D, pot_1D_pes[:,i], '--', color=color)
    ax2.plot(coord_1D, pot_1D_buttiker[:,i], color=color, label='step {}'.format(i))
ax2.set_xlabel(r'$y~[nm]$')
ax2.set_ylabel(r'$U~[meV]$')
ax2.set_title(r'$cross~section~at~x=0~nm$')

ax2.set_xlim(-150, 150)
ax2.set_ylim(-7,6)

# Creating legend 1
ax2.legend(loc = 'upper left')

# Creating legend 2
ax_legend = ax2.twinx()
ax_legend.set_yticks([])
custom_lines = [Line2D([0], [0], linestyle='--', color='k', label='Pescado'),
                Line2D([0], [0], linestyle='-', color='k', label='Buttiker fit')]
ax_legend.legend(handles = custom_lines)

plt.tight_layout()
plt.show()
```

```python
fig.savefig('buttiker_potential_fit.pdf')
```

## Figures report

```python
conductance = []
coord = []
V_gates_list = []
voltage = []
labels = []

for file in [*files_a_fsc, *files_b_fsc]:
    data = dh.read_file(file)
    
    if data.conductance.shape[1] != 1:
        conductance_loc = data.conductance[:, np.argmin(np.abs(data.p_sim['energy_list']))]
    else:
        conductance_loc = data.conductance
    conductance.append(conductance_loc.flatten())
    
    coord.append(data.coord)
    V_gates_list.append(data.p_sim['V_gates_list'])
    voltage.append(data.voltage)
    labels.append(data.p_geom['qpc_name'])
```

```python
# Performing the buttiker fit

V_gates_list_split = []
conductance_split = []
wx = []
wy = [] 

for coord_, V_gates_list_, conductance_, voltage_ in zip(coord, V_gates_list, conductance, voltage):
        
    V_gates_list_split_, conductance_split_, wx_, wy_ = buttiker_fit_conductance(
        coord_, V_gates_list_, conductance_, voltage_)
    
    V_gates_list_split.append(V_gates_list_split_)
    conductance_split.append(conductance_split_)
    wx.append(wx_)
    wy.append(wy_)
```

```python
# Calculating the width of the plateaus, defined as the spacing between the maximal derivatives

accuracy = 1e-3 # mV accuracy

width = []

for V_gates_list_, conductance_ in zip(V_gates_list, conductance):
    f = interp1d(V_gates_list_, conductance_, 'cubic', bounds_error=False, fill_value='extrapolate')

    vmin = V_gates_list_[0]
    vmax = V_gates_list_[-1]

    slope = []
    vg_extended = np.linspace(vmin, vmax, 1+int((vmax - vmin)/accuracy))
    for vg in vg_extended:
        slope.append(approx_fprime(vg, lambda x:float(f(x)), epsilon=1.4901161193847656e-08))

    slope=np.array(slope).flatten()

    index_flat = signal.argrelextrema(slope, np.greater)
    infliction_points = vg_extended[index_flat]

    infliction_points = infliction_points[np.where(f(infliction_points)>0.1)]

    width.append(np.diff(infliction_points))
```

```python
fig, axs = plt.subplots(1,4, figsize=(10,6))

marker = ['s', 'x', 'v','s', 'x', 'v', 'o']
color = ['tab:blue']*3 + ['tab:red']*4

for wx_, wy_, width_, label_, marker_, color_ in zip(wx, wy, width, labels, marker, color):
    axs[0].plot(range(len(wx_)), 1e3*c.hbar*np.array(wx_)/c.elementary_charge, label=label_,
                marker=marker_, color=color_, linewidth=0.5, markerfacecolor='none')
    axs[1].plot(range(len(wy_)), 1e3*c.hbar*np.array(wy_)/c.elementary_charge,
                marker=marker_, color=color_, linewidth=0.5, markerfacecolor='none')
    
    axs[2].plot(range(len(wx_)), np.array(wy_)/np.array(wx_), 
                marker=marker_, color=color_, linewidth=0.5, markerfacecolor='none')
    
    axs[3].plot(range(len(width_)), np.array(width_)*1000, 
                marker=marker_, color=color_, linewidth=0.5, markerfacecolor='none')

axs[0].set_xlabel(r'step number')
axs[0].set_title(r'$\hbar\omega_x~[meV]$')
axs[0].legend()

axs[1].set_xlabel(r'step number')
axs[1].set_title(r'$\hbar\omega_y~[meV]$')

axs[2].set_xlabel(r'step number')
axs[2].set_title(r'$\omega_y/\omega_x~[-]$')

axs[3].set_xlabel(r'step number')
axs[3].set_title(r'$width~step~[mV]$')

plt.tight_layout()

```

```python
fig.savefig('buttiker_fit_wxwy.pdf')
```
