---
jupyter:
  jupytext:
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.13.8
  kernelspec:
    display_name: Python 3 (ipykernel)
    language: python
    name: python3
---

```python
import scipy.constants as c
import numpy as np
import matplotlib.pyplot as plt
from  qpc_exp2sim.tools import plotting_sim, plotting_exp
import qpc_exp2sim.tools.data_handling as dh
from qpc_exp2sim.pescado_kwant import plotting_pes, pescado_kwant, extension_pes, exact_ldos
import matplotlib as mpl
%matplotlib notebook
```

## Overview LDOS, voltage, charge


### LDOS, voltage, charge ifo position

```python
data.p_geom
```

```python
# Gathering data for figure below
# Load a file for which the iterations are stored away
geom_file = 'a2_grid10'
sim_file = 'V4_a2_fsc_nr_m100_it20'

data = dh.read_file('files_data/'+ geom_file + '_' + sim_file)
#data = dh.read_file('files_data/b3_grid10_V128_b3_fsc_nr_m100_it5')

# unpacking the data for easy acces to everything
p_geom = data.p_geom
p_sim = data.p_sim
file_geom = data.file_geom
file_sim = data.file_sim
V_gates_list = p_sim['V_gates_list']
energy_list = p_sim['energy_list']
coord = data.coord
coord_kwant = data.coord_kwant
coord_kpm = data.coord_kpm
voltage = data.voltage
charge = data.charge
conductance = data.conductance
voltage_shift = data.voltage_shift
dopant_dens = data.dopant_dens
V3 = data.V3
ldos = data.ldos

cell_area = p_geom['grid'][0] * p_geom['grid'][1] * 1e-18
index_e = 0
iteration = -1
print('The energy is {} t'.format(energy_list[index_e]))

potential = voltage * (-1) # Unit: [eV]

print('\n', data)
```

```python
# Evaluation ldos and ildos ifo space

# Running this cell might take some time!
ldos_val = []
#ildos_val = []
coord_val = []

iteration = 5

for index_v in range(len(V_gates_list)):
    ldos_loc = ldos[index_v][iteration]
    coord_loc = coord[ldos_loc.indices][:,0:2]
    voltage_loc = voltage[index_v][iteration][ldos_loc.indices]
    origin_loc, slope_loc = ldos_loc.quantum_functional_kwant(voltage_loc)
    
    coord_val.append(coord_loc)
    ldos_val.append(slope_loc)
    #ildos_val.append(origin_loc + slope_loc * voltage_loc)
```

```python
def plot_section(coord, data_elem, section, ax, color):
    coord_1D, ldos_1D = plotting_pes.data_cross_section(coord, [data_elem], section=section)
    ax.plot(coord_1D[:,0], ldos_1D[:,0,0], color=color)
    
    label = ['x', 'y']
    n = section.index(None)
    m = (n + 1) % 2
    ax.set_xlabel('{} [nm]'.format(label[n]))
    #ax.set_ylabel(ylabel)
```

```python
fig, axs = plt.subplots(5,3, figsize=(10, 15))

# Voltage used for the top 3 rows:
index_v = 2
print('The gate voltage is {} V'.format(V_gates_list[index_v]))

# Points where cross sections are made
x1, x2 = 0, 200
y1, y2 = 0, 200
colors = ['tab:blue', 'tab:orange']

################################################################################################################
# 0) TOP ROW COLORMAPS
################################################################################################################

# [0.0] ldos
plotting_pes.colormap(coord_val[index_v], ldos_val[index_v]*(1e-3/cell_area), aspect_equal=True, 
                      ax=axs[0,0], title=r'LDOS [$m^{-2}~meV^{-1}$]', xlabel='x [nm]') 
axs[0,0].set_ylabel('y [nm]', labelpad=-10)

axs[0,0].axvline(x1, color=colors[0])
axs[0,0].axvline(x2, color=colors[1])
axs[0,0].axhline(y1, color=colors[0])
axs[0,0].axhline(y2, color=colors[1])

# [0,1] potential
section = [None, None, 0]
coord_2D, potential_2D = plotting_pes.data_cross_section(coord, potential[index_v][iteration]*1e3, section=section)

plotting_pes.colormap(coord_2D, potential_2D, aspect_equal=True, 
                      imshow = True, ax=axs[0,1], title=r'Potential U [$meV$]', 
                      xlabel='x [nm]')    

axs[0,1].set_yticklabels([])

# [0,2] charge
coord_2D, charge_2D = plotting_pes.data_cross_section(coord, charge[index_v][iteration], section=section)

plotting_pes.colormap(coord_2D, charge_2D, cell_area=cell_area, aspect_equal=True, 
                      imshow=True, ax=axs[0,2], title=r'Charge density n [$m^{-2}$]', cmap = plt.cm.plasma,
                      xlabel='x [nm]')

axs[0,2].set_yticklabels([])

################################################################################################################
# 1) ROW 1 & 2 DIFFERENT CROSS SECTIONS OF COLORMAP ABOVE
################################################################################################################

# [1.0] ldos cross sections ifo x at different y
for section, color in zip([[None, y1], [None, y2]], colors):
    plot_section(coord_val[index_v], ldos_val[index_v]*(1e-3/cell_area), section, axs[1,0], color)

# [2.O] ldos cross sections ifo y at different x
for section, color in zip([[x1, None], [x2, None]], colors):
    plot_section(coord_val[index_v], ldos_val[index_v]*(1e-3/cell_area), section, axs[2,0], color)

# [1.1] potential cross sections ifo x at different y
for section, color in zip([[None, y1 , 0], [None, y2, 0]], colors):
    plot_section(coord, potential[index_v,iteration]*1e3, section, axs[1,1], color)

# [2.1] potential cross sections ifo y at different x
for section, color in zip([[x1, None, 0], [x2, None, 0]], colors):
    plot_section(coord, potential[index_v,iteration]*1e3, section, axs[2,1], color)

# [1.2] charge cross sections ifo x at different y
for section, color in zip([[None, y1, 0], [None, y2, 0]], colors):
    plot_section(coord, charge[index_v,iteration]*(1/cell_area), section, axs[1,2], color)

# [2.2] charge cross sections ifo y at different x
for section, color in zip([[x1, None, 0], [x2, None, 0]], colors):
    plot_section(coord, charge[index_v,iteration]*(1/cell_area), section, axs[2,2], color)
    
    
axs[1,2].legend([r'$y = 0~nm$', r'$y = 200~nm$'], loc='upper right', fontsize='small')
axs[2,2].legend([r'$x = 0~nm$', r'$x = 200~nm$'], loc='upper right', fontsize='small')

################################################################################################################
# 2) ROW 3 & 4 DIFFERENT GATE VOLTAGES
################################################################################################################

colors = 'tab:green', 'tab:red', 'tab:blue'

# [3.0] ldos cross sections ifo x at different gate voltages
for index_v, color in zip(range(3), colors):
    plot_section(coord_val[index_v], ldos_val[index_v]*(1e-3/cell_area), [None, 0], axs[3,0], color)


# [4.O] ldos cross sections ifo y at different gate voltages
for index_v, color in zip(range(3), colors):
    plot_section(coord_val[index_v], ldos_val[index_v]*(1e-3/cell_area), [0, None], axs[4,0], color)


# [3.1] potential cross sections ifo x at different gate voltages
for index_v, color in zip(range(3), colors):
    plot_section(coord, potential[index_v, iteration]*1e3, [None, 0, 0], axs[3,1], color)


# [4.1] potential cross sections ifo y at different gate voltages
for index_v, color in zip(range(3), colors):
    plot_section(coord, potential[index_v, iteration]*1e3, [0, None, 0], axs[4,1], color)


# [3.2] charge cross sections ifo x at different gate voltages
for index_v, color in zip(range(3), colors):
    plot_section(coord, charge[index_v, iteration]*(1/cell_area), [None, 0, 0], axs[3,2], color)


# [4.2] charge cross sections ifo y at different gate voltages
for index_v, color in zip(range(3), colors):
    plot_section(coord, charge[index_v, iteration]*(1/cell_area), [0, None, 0], axs[4,2], color)
    
axs[3,2].legend([r'$V_g = -1.9~V$', r'$V_g = -1.8~V$', r'$V_g = -1.4~V$'], loc='upper right', fontsize='small')
axs[4,2].legend([r'$V_g = -1.9~V$', r'$V_g = -1.8~V$', r'$V_g = -1.4~V$'], loc='upper right', fontsize='small')

labels = [column + row + ')' for row in [r'I', r'II', r'III', r'IV', r'V'] for column in [r'a', r'b', r'c']]

for ax, label in zip(axs.flatten(), labels):
    # label physical distance to the left and up:
    trans = mpl.transforms.ScaledTranslation(-20/72, 7/72, fig.dpi_scale_trans)
    ax.text(0.0, 1.0, label, transform=ax.transAxes + trans,
            fontsize='medium', va='bottom', fontfamily='latin modern')

plt.tight_layout()
plt.show()
```

```python
fig.savefig('overview_electrostatic_results.pdf')
```

### LDOS, voltage, charge ifo gate voltage

```python
# Helper functions

def uncal2cal(x):
    return x + voltage_shift_

def cal2uncal(x):
    return x - voltage_shift_

def format_plot(axs, v_shift, v3):
    linestyle = {'linestyle':'--', 'color':'k', 'linewidth':0.5}

    for ax in axs:
        ax.axvline(0, **linestyle)
        ax.axvline(-v_shift, **linestyle)
        ax.axvline(v3, **linestyle)
    
    axs[1].axhline(0, **linestyle)
    
    axs[0].set_xlim(axs[-1].get_xlim())
    axs[1].set_xlim(axs[-1].get_xlim())

    ax_uncalibrated = axs[0].secondary_xaxis('top', functions=(uncal2cal, cal2uncal))
    ax_uncalibrated.set_xlabel(r'$V_{g,uncalibrated}~[V]$')
    
    # Add calibration voltage as label
    ax_uncalibrated.set_xticks(np.append(axs[0].get_xticks(), v_shift))
    xticklabels = ['{:.1f}'.format(xtick) for xtick in axs[0].get_xticks()]
    xticklabels.append(mpl.text.Text(0, v_shift, r'$V_{cal}$'))
    ax_uncalibrated.set_xticklabels(xticklabels)
    
    # Add pinch off voltage as label
    axs[0].set_xticks([-2, v3, -1.5, -1, -0.5, 0, 0.5])
    axs[0].set_xticklabels(['', r'-2.0 $V_{3}$    ', '-1.5', '-1.0', '-0.5', '0.0', '0.5'])
        
    
def get_data_from_files(files, points):
    ldos_0D = []
    potential_0D = []
    charge_0D = []
    voltage_shift = []
    V_gates_list = []
    V3 = []
    
    for file in files:
    
        data = dh.read_file(file)
        cell_area = np.product(data.p_geom['grid'])*1e-18
        
        # Cross section voltage and charge
        _, voltage_0D_ = plotting_pes.data_cross_section(data.coord, data.voltage, section=points)
        _, charge_0D_ = plotting_pes.data_cross_section(data.coord, data.charge, section=points)
        
        # Obtaining ldos at required points (if possible)
        
        if data.ldos is None: 
            if data.p_sim['setting_solver'] == 'pi_bulk':
                ldos_0D_ = np.where(voltage_0D_ <= 0, 0, data.p_sim['helmholtz_coef'])
            else:
                print('There is no valid interpretation of the ldos')
            
        elif isinstance(data.ldos, exact_ldos.ExactLdos):
            ldos_0D_ = []
            for potential in voltage_0D_[0] * data.ldos.conversion_unit:
                ldos_0D_.append(data.ldos(energy=potential)[:,0])
            ldos_0D_ = 2* np.array(ldos_0D_) * data.ldos.conversion_unit # To have the result in pescado units
            
        else:
            ldos_0D_ = np.empty((len(data.p_sim['V_gates_list']), len(points)))
            ldos_0D_[:] = np.NaN

            for index_v in range(len(data.p_sim['V_gates_list'])):
                ldos_loc = data.ldos[index_v]
                indices = extension_pes.indices_from_coordinates(data.coord, points)

                # It might be that the ldos is not defined for a certain site since the 2DEG is depleted at that site
                for i,index in enumerate(indices):
                    try:
                        local_index = ldos_loc._local_index([index])
                        energy = voltage_0D_[0][index_v][i] * ldos_loc.conversion_unit - ldos_loc.potential[local_index]
                        ldos_0D_[index_v, i] = ldos_loc(energy=energy, index=index)
                    except:
                        pass
                    
            ldos_0D_ = 2* ldos_0D_ * ldos_loc.conversion_unit # To have the result in pescado units
            
        ldos_0D_ = ldos_0D_ * (1e-3/cell_area) # To have the ldos in /meV/m^-2
        potential_0D_ = voltage_0D_[0] * (-1e3) # To have the potential in meV
        charge_0D_ = charge_0D_[0] * (1/cell_area) # To have the charge in m^-2

        ldos_0D.append(ldos_0D_)
        potential_0D.append(potential_0D_)
        charge_0D.append(charge_0D_)
        voltage_shift.append(data.voltage_shift)
        V_gates_list.append(data.p_sim['V_gates_list'])   
        V3.append(data.V3)
        
    return ldos_0D, potential_0D, charge_0D, voltage_shift, V_gates_list, V3
```

```python
# Loading the data
points = np.array([[0,0,0],[0,200,0], [500,0,0]])

ldos_0D, potential_0D, charge_0D, voltage_shift, V_gates_list, V3 = \
    get_data_from_files(['files_data/a2_grid10_V128_a2_full_pi_bulk', 
                         'files_data/a2_grid10_V128_a2_full_nr_lat_m100_it5', 
                         'files_data/a2_grid10_V128_a2_fsc_nr_m100_it5'], points)
```

```python
# Plotting the data
legend = [str(point) for point in points]
colors = ['tab:blue', 'tab:orange', 'tab:green']
linestyles = ['-.', '--', '-']

fig, (ax1, ax2, ax3) = plt.subplots(3,1, figsize = (6,9), sharex=True)

for ldos_0D_, potential_0D_, charge_0D_, voltage_shift_, V_gates_list_, linestyle in \
    zip(ldos_0D, potential_0D, charge_0D, voltage_shift, V_gates_list, linestyles):

    for ldos_elem, potential_elem, charge_elem, color in \
        zip(ldos_0D_.transpose(), potential_0D_.transpose(), charge_0D_.transpose(), colors):
        
        ax1.plot(V_gates_list_, ldos_elem, color=color, linewidth = 1 , linestyle=linestyle)
        ax2.plot(V_gates_list_, potential_elem, color=color, linewidth= 1, linestyle=linestyle)
        ax3.plot(V_gates_list_, charge_elem, color=color, linewidth= 1, linestyle=linestyle)

ax1.set_ylabel(r'$\varrho~[m^{-2}~meV^{-1}]$')
        
ax2.set_ylim([-17, 8])
ax2.set_ylabel(r'$U~[meV]$')

ax3.set_xlabel(r'$V_{g,calibrated}~[V]$')
ax3.set_ylabel(r'$n~[m^{-2}]$')

format_plot((ax1, ax2, ax3), voltage_shift[0], V3[0][0][0])

# Custom legend regarding positions
custom_lines = [mpl.lines.Line2D([0], [0], color=colors[0]),
                mpl.lines.Line2D([0], [0], color=colors[1]),
                mpl.lines.Line2D([0], [0], color=colors[2])]
ax1.legend(custom_lines, [r'$center$', r'$gated$', r'$ungated$'])

# Custom legend regarding methods
custom_lines = [mpl.lines.Line2D([0], [0], color='k', linestyle=linestyles[0]),
                mpl.lines.Line2D([0], [0], color='k', linestyle=linestyles[1]),
                mpl.lines.Line2D([0], [0], color='k', linestyle=linestyles[2])]
ax2.legend(custom_lines, [r'$DOS~cont$', r'$DOS~TB_{0}$', r'$SCQE$'])


plt.tight_layout(h_pad=0.3)
plt.show()

# It's clear that the helmholtz coefficient is the ratio between the density and voltage in the non depleted regions.
```

```python
fig.savefig('ldos_volt_charge_ifo_vg.pdf')
```

```python
voltage_shift
```

## Conductance 
### Comparison between QPCs

```python
def plot_cond_comp(files, axs, xlimits_top=None, ylimits_top=[0,7], xlimits_bottom=[-0.05,0.4], ylimits_bottom=[0,2.2],
                   title=None):
    
    for file in files:
        data = dh.read_file(file)
        V_gates_list = data.p_sim['V_gates_list']
        conductance = data.conductance
        label = data.p_geom['qpc_name']
        
        axs[0].plot(V_gates_list, conductance, label=label)
        if title is not None:
            axs[0].set_title(title)
        if xlimits_top is not None:
            axs[0].set_xlim(xlimits_top)
        axs[0].set_ylim(ylimits_top)
        axs[0].legend(loc='upper left')
        axs[0].set_xlabel(r'$V_g~[V]$')
        axs[0].set_ylabel(r'$G~[2e^2/h]$')

        axs[1].plot(V_gates_list - data.V3[1], conductance)
        axs[1].set_xlim(xlimits_bottom)
        axs[1].set_ylim(ylimits_bottom)
        axs[1].set_xlabel(r'$V_g - V_{3, qpc}~[V]$')
        axs[1].set_ylabel(r'$G~[2e^2/h]$')

        
fig, axs = plt.subplots(2,2, figsize = (10,6))

files_a = ['files_data/{0}_grid10_V128_{0}_fsc_nr_m100_it5'.format(file) for file in ['a1', 'a2', 'a3', 'a4']]
files_b = ['files_data/{0}_grid10_V128_{0}_fsc_nr_m100_it5'.format(file) for file in ['b1', 'b2', 'b3', 'b4']]

plot_cond_comp(files_a, axs[:,0])
plot_cond_comp(files_b, axs[:,1])

labels = ['a)', 'b)', 'c)', 'd)']
for ax, label in zip(axs.flatten(), labels):
    # label physical distance to the left and up:
    trans = mpl.transforms.ScaledTranslation(-20/72, 7/72, fig.dpi_scale_trans)
    ax.text(0.0, 1.0, label, transform=ax.transAxes + trans,
            fontsize='medium', va='bottom', fontfamily='latin modern')
    
axs[0,1].set_ylabel(None)
axs[1,1].set_ylabel(None)

axs[0,1].set_yticklabels([])
axs[1,1].set_yticklabels([])

plt.tight_layout()
plt.show()
```

```python
fig.savefig('conductance_types.pdf')
```

### Influence methods on conductance + comparison with experiment
Creating this figure might take some time since 24 different data files have to be loaded to obtain all the simulation results.

```python
from qpc_exp2sim.simulation.QPC_geometry import qpc_data

```

```python
fig, ax = plt.subplots()

for r in [2, 1.75, 1.5, 1.3, 1, 0.7]:
    plot_1D_map(file_exp, 'X1', 'Y3', qpc_n = 4, hlines=np.arange(8), subtract_resistance=True, 
                ylimits=[0,7], label=[str(r)], V3_zero = True, ax=ax, linestyle='--', 
                resistance_factor=r)

plt.show()
```

```python
fig, axs = plt.subplots(4,2, figsize=(10,15), sharey=True)


def get_files(shape, n):
    file_geom = '{}{}_grid10'.format(shape, n)
    
    files_sim = ['V128_{}{}_pi_bulk'.format(shape, n), 
                 'V128_{}{}_nr_lat_m100_it5'.format(shape, n),
                 'V128_{}{}_fsc_nr_m100_it5'.format(shape, n)]
    return ['files_data/' + file_geom + '_' + file for file in files_sim]

    
labels_leg = [r'$sim:~DOS~cont$', r'$sim:~DOS~TB_0$', r'$sim:~SCQE$']
labels_fig =  ['a)', 'b)', 'c)', 'd)', 'e)', 'f)', 'g)', 'h)']

qpcs = [shape + str(n) for n in range(1,5) for shape in ['a', 'b']]
ylimits = [0,7]

file_exp = 'files_data/data_1D_mK.csv'

for label_fig, qpc, ax in zip(labels_fig, qpcs, axs.flatten()):

    for label_leg, file in zip(labels_leg, get_files(*qpc)):
        try: 
            data = dh.read_file(file, file_type = 'simulation')

            V_gates_list= data.p_sim['V_gates_list']
            conductance = data.conductance.flatten()
            V3 = data.V3[1]

            ax.plot(V_gates_list - V3, conductance, label=label_leg)
            ax.set_ylim(ylimits)
        except:
            ax.plot([0],[0])
        
    for val in range(7):
        ax.axhline(val, color = 'gray', linewidth = 0.1, linestyle='--')
                
    # For the a type qpcs there are multiple devices with the same geometry. The experiemental data for both 
    # types is plotted.
    shape, n = qpc
    
    if shape == 'b': 
        color = 'tab:brown'
    else:
        color = None
    
    for X in {'a':['X1','X5'], 'b':['X2']}[shape]:
        plotting_exp.plot_1D_map(file_exp, X, 'Y3', qpc_n = int(n), ylimits=[0,7], 
                                 label=[r'$exp:~sample~{}Y3$'.format(X)], V3_zero = True, ax=ax, linestyle='--', color=color, 
                                 legend=False)
        
        ax.set_xlim((-0.1, ax.get_xlim()[1]))
    
    if qpc == 'a1':
        handles, labels = ax.get_legend_handles_labels()
        handles.append(mpl.lines.Line2D([0],[0], color='tab:brown', linestyle='--'))
        labels.append(r'$exp:~sample~X2Y3$')
        ax.legend(handles, labels)
        
    if qpc in ['a3', 'a4']:
        ax.set_xlim(ax.get_xlim()[0]+0.05, ax.get_xlim()[1]+0.05)
        
    if shape == 'b':
        ax.set_ylabel(None)
        
    ax.set_title('QPC {}{}'.format(shape, n))
    ax.set_xlabel(r'$V_g - V_{3,qpc}~[V]$')

labels = ['a)', 'b)', 'c)', 'd)', 'e)', 'f)', 'g)', 'h)']
for ax, label in zip(axs.flatten(), labels):
    # label physical distance to the left and up:
    trans = mpl.transforms.ScaledTranslation(-20/72, 7/72, fig.dpi_scale_trans)
    ax.text(0.0, 1.0, label, transform=ax.transAxes + trans,
            fontsize='medium', va='bottom', fontfamily='latin modern')
    
plt.tight_layout()
plt.show()

```

```python
fig.savefig('conductance_exp_sim.pdf')
```

```python

```
