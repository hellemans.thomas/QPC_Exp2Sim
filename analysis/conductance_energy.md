---
jupyter:
  jupytext:
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.13.8
  kernelspec:
    display_name: Python 3 (ipykernel)
    language: python
    name: python3
---

```python
import numpy as np
from qpc_exp2sim.tools import plotting_sim, data_handling as dh
import scipy.constants as c
import warnings
import matplotlib.pyplot as plt
import matplotlib as mpl

%matplotlib notebook
```

```python
data = dh.read_file('files_data/a2_grid10_V128_a2_fsc_nr_m100_it5')

# unpacking the data
p_geom = data.p_geom
p_sim = data.p_sim
V_gates_list = p_sim['V_gates_list']
coord = data.coord
voltage = data.voltage
charge = data.charge
conductance = data.conductance
voltage_shift = data.voltage_shift
V3 = data.V3
```

```python
def cmap_mesh(x,y):
    """
    Only for rectangular meshes
    """
    def cmap_points(data):
        data = np.unique(data)
        diff = np.diff(data)
        X = data - np.insert(diff, 0, diff[0])/2
        X = np.append(X, data[-1]+diff[-1]/2)
        return X
    return np.meshgrid(cmap_points(x), cmap_points(y))

def plot_cond_energy(conductance, V_gates_list, energy_list, nb_steps=7, filtered = False, derivative=False, ylabel=None):
    fig, ax = plt.subplots()
    ax.set_xlabel(r'$V_g [V]$')
    if ylabel is None:
        ax.set_ylabel('energy [t]')
    else:
        ax.set_ylabel(ylabel)
    label = r'$G~[2e^2/h]$'

    cmap = plt.get_cmap('hot')
    X,Y = cmap_mesh(V_gates_list, energy_list)

    if filtered:
        if derivative:
            warnings.warn('Plotting derivative only possible for "filtered=False", normal conductance plotted')
        levels = np.insert(np.arange(0.5, nb_steps+0.5, 1), 0, 0)
        norm = mpl.colors.BoundaryNorm(levels, ncolors=cmap.N, clip=True)
        ax.pcolormesh(X,Y, conductance.transpose(), cmap=cmap, norm=norm)
        cf = ax.contourf(V_gates_list, energy_list, conductance.transpose(), levels=levels, cmap=cmap)
        fig.colorbar(cf, ax=ax, ticks=np.arange(nb_steps), label = label)
    else:
        if derivative:
            conductance = np.gradient(conductance, V_gates_list[1] - V_gates_list[0], axis=0)
            label = r'$dG/dV_g$'
        norm = mpl.colors.Normalize(vmin=0, vmax=np.nanmax(conductance))
        scalarmappable = plt.cm.ScalarMappable(norm=norm, cmap=cmap)
        fig.colorbar(scalarmappable, label = label)
        ax.pcolormesh(X,Y, conductance.transpose(), cmap=cmap, norm=norm)
        
        
    fig.savefig('conductance_energy_map.pdf')
```

```python
plot_cond_energy(conductance, V_gates_list, p_sim['energy_list'], filtered=False, derivative=False)
plot_cond_energy(conductance, V_gates_list, p_sim['energy_list'], filtered=False, derivative=True)
plot_cond_energy(conductance, V_gates_list, p_sim['energy_list'], filtered=True, derivative=False)

# Check how these energy levels correspond to the energy level spacing according to the buttiker model

# From this graph we see that there's a non linear relationship between energy and V_g, this enforces a non constant 
# lever arm alpha
```

```python
def cond_vsd(conductance, energy_list, t, Vsd=500e-6):

    Esd = Vsd * c.elementary_charge/t
        
    index = np.where(np.all([-Esd/2 <= energy_list, energy_list<= Esd/2], axis=0))[0]
    conductance = np.average(conductance[:,index], axis=1)
    
    return conductance

# Not ideal since the edge of the data points migth not be equal to Vsd -> introduce weigth in the average function
single_cond = cond_vsd(conductance, p_sim['energy_list'], p_geom['t'])
```

```python
conductance_vsd = []
Vsd_list = np.linspace(0,0.005,128)
for Vsd in Vsd_list:
    conductance_vsd.append(cond_vsd(conductance, p_sim['energy_list'], p_geom['t'], Vsd=Vsd))

conductance_vsd = np.array(conductance_vsd).transpose()

#plot_cond_energy(conductance_vsd, V_gates_list, Vsd_list*1000, filtered=False, derivative=False, ylabel=r'$V_{sd} [mV]$')
cond_vsd(conductance, p_sim['energy_list'], p_geom['t'], Vsd=Vsd)
plot_cond_energy(conductance_vsd, V_gates_list, Vsd_list*1000, filtered=False, derivative=True, ylabel=r'$V_{b} [mV]$')
```

```python
fig, ax = plt.subplots()

ax.plot(V_gates_list, conductance[:, np.argmin(np.abs(p_sim['energy_list']))], label=r'$No~V_{sd}$')

cond_vsd_val = cond_vsd(conductance, p_sim['energy_list'], p_geom['t'], Vsd=500e-6)
ax.plot(V_gates_list, cond_vsd_val, label=r'$V_{sd} = 0.5~mV$')

cond_vsd_val = cond_vsd(conductance, p_sim['energy_list'], p_geom['t'], Vsd=1000e-6)
ax.plot(V_gates_list, cond_vsd_val, label=r'$V_{sd} = 1~mV$')

ax.set_xlabel(r'$V_g~[V]$')
ax.set_ylabel(r'$G~[2e^2/h]$')

ax.legend()
ax.plot()
```

```python
p_sim
```

```python

```
