---
jupyter:
  jupytext:
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.13.8
  kernelspec:
    display_name: Python 3 (ipykernel)
    language: python
    name: python3
---

```python
from qpc_exp2sim.pescado_kwant import plotting_pes
from qpc_exp2sim.simulation.builder import gates_shape
import matplotlib.pyplot as plt
%matplotlib notebook
```

```python
L = 1500
W = 1500
L_narrow_gate = 50
qpc_center = [0,400]
d_contact = 1
d_up = 0

qpc_names = [['a1', 'a2', 'a3', 'a4'], ['b1', 'b2', 'b3', 'b4']]
```

```python
fig, ax = plt.subplots(2,4, figsize=(8,4), sharey = True, sharex = True)

for i in range(len(ax)):
    for j in range(len(ax[0])):
        ax_loc = ax[i,j]

        shape_loc = gates_shape(W, L_narrow_gate, qpc_center, qpc_names[i][j], d_contact, d_up)
        plotting_pes.shape([shape_loc], colors=['grey'], aspect_equal=True, section = [None, None, 0], 
                           imshow=True, ax=ax_loc)

        ax_loc.plot([-L/2, -L/2, L/2, L/2, -L/2], [-W/2, W/2, W/2, -W/2, -W/2],
                color = 'k', linestyle = '--', linewidth = 0.5)
        ax_loc.set_xticks([-L/2, -L/4, 0 , L/4, L/2])
        #ax_loc.set_yticks([-W/2, -W/4, 0 , W/4, W/2])
        if i==0:
            ax_loc.set_title('Number {}'.format(j+1))
        if i == 1:
            ax_loc.set_xlabel('x [nm]')
        if j == 0:
            ax_loc.set_ylabel('Type {}'.format(['a', 'b'][i]) + '\ny [nm]')
        ax_loc.set_ylim(ax_loc.get_xlim())
        
plt.tight_layout()
plt.show()

```

```python
fig.savefig('overview_qpc.pdf')
```
