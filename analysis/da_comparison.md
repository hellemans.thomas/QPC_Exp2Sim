---
jupyter:
  jupytext:
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.13.8
  kernelspec:
    display_name: Python 3 (ipykernel)
    language: python
    name: python3
---

```python
from __future__ import print_function
import qpc_exp2sim.tools.data_handling as dh
from qpc_exp2sim.tools import plotting_sim
from qpc_exp2sim.simulation import solver
from qpc_exp2sim.pescado_kwant import plotting_pes
import numpy as np
from ipywidgets import interact, interactive, fixed, interact_manual
import ipywidgets as widgets
import matplotlib.pyplot as plt
import matplotlib as mpl
from types import SimpleNamespace
%matplotlib notebook

files_a = ['files_data/{}_grid10_V128_full_pi_bulk'.format(file) for file in ['a1', 'a2', 'a3', 'a4']]
files_a_fine = ['files_data/{0}_grid10_V128_{0}_pi_bulk'.format(file) for file in ['a1', 'a2', 'a3', 'a4']]
files_b = ['files_data/{}_grid10_V128_full_pi_bulk'.format(file) for file in ['b1', 'b2', 'b3', 'b4']]
files_b_fine = ['files_data/{0}_grid10_V128_{0}_pi_bulk'.format(file) for file in ['b1', 'b2', 'b3', 'b4']]
```

## Influence grid

```python
files = ['b3_grid{}_V128_b3_pi_bulk'.format(grid) for grid in [6,8,10,12,15,20]]

conductance = []
V_gates_list = []
labels = []

fig, ax = plt.subplots()

for file in files:
    data = dh.read_file(file)
    V_gates_list.append(data.p_sim['V_gates_list'])
    conductance.append(data.conductance)
    labels.append('{} nm'.format(data.p_geom['grid'][0]))

plotting_sim.plot_conductance(V_gates_list, conductance, label = labels, linestyle='-', 
                 hlines=np.arange(6), ax=ax)

plt.show()
```

# Overview conductance for small qpc's

```python
for files in [files_a, files_b, files_a_fine, files_b_fine]:
    conductance = []
    V_gates_list = []
    labels = []

    for file in files:
        data = dh.read_file(file)
        V_gates_list.append(data.p_sim['V_gates_list'])
        conductance.append(data.conductance)
        labels.append(data.p_geom['qpc_name'])

    plotting_sim.plot_conductance(V_gates_list, conductance, linestyle='-', label=labels,
                                  hlines=np.arange(7), limits_v=None)
```

```python
for files in [files_a_fine, files_b_fine]:
    conductance = []
    V_gates_list = []
    labels = []

    for file in files:
        data = dh.read_file(file)
        V_gates_list.append(data.p_sim['V_gates_list'] - data.V3[0][0])
        conductance.append(data.conductance)
        labels.append(data.p_geom['qpc_name'])

    plotting_sim.plot_conductance(V_gates_list, conductance, linestyle='.-', label=labels,
                     hlines=np.arange(6), limits_v=None)
```

## Comparison V3 simulations and experiments

```python
from qpc_exp2sim.simulation.QPC_geometry import qpc_data

def plot_V3(files, figsize=(10,7)):
    
    if not isinstance(files[0], list):
        files = [files]
    
    fig, axs = plt.subplots(len(files), figsize=figsize)
    
    for i, files_elem in enumerate(files):
        # Read the data
        
        if len(files) == 1:
            axs = [axs]
        
        V3_exp = []
        V3_exp_std = []
        V3_sim_dens = []
        V3_sim_cond = []
        V3_sim_el = []
        x = []
        labels = []
    
        for file in files_elem:
            data = dh.read_file(file)
            V3_sim_dens.append(data.V3[0][0])
            V3_sim_cond.append(data.V3[1])
            qpc = qpc_data[data.p_geom['qpc_name']]
            x_param = {'a':'L', 'b':'R', 'c':'L'}[data.p_geom['qpc_name'][0]]
            x.append(qpc[x_param])
            V3_sim_el.append(np.average(qpc['V3_sim_el']))
            V3_exp.append(np.average(qpc['V3_exp']))
            V3_exp_std.append(np.std(qpc['V3_exp']))
            labels.append(data.p_geom['qpc_name'])
    
        # Plot the data
            
        hlines = [-2.5, -2.25, -2,-1.75, -1.5, -1.25, -1]
        for line in hlines:
            axs[i].axhline(line, color = 'k', linestyle = '--', linewidth =0.3)
    
    
        axs[i].errorbar(x[0], V3_exp[0], yerr=V3_exp_std[0], color='tab:blue', label='exp: W = 250 nm')
        axs[i].errorbar(x[1:], V3_exp[1:], yerr=V3_exp_std[1:],  color='tab:orange', label='exp: W = 300 nm')
    
        axs[i].plot(x[0], V3_sim_dens[0], marker='o', markerfacecolor='none', color='k', label="sim: DOS cont")
        axs[i].plot(x[1:], V3_sim_dens[1:], marker='o', markerfacecolor='none', color='k', linestyle='--')
    
#         axs[i].plot(x[0], V3_sim_cond[0], marker='d', markerfacecolor='none', color='k', label="sim: conductance")
#         axs[i].plot(x[1:], V3_sim_cond[1:], marker='d', markerfacecolor='none', color='k', linestyle='--')
        
        axs[i].plot(x[0], V3_sim_el[0], marker='x', markerfacecolor='none', color='r', label="sim: nextnano++")
        axs[i].plot(x[1:], V3_sim_el[1:], marker='x', markerfacecolor='none', color='r', linestyle='--')
        
        xlabel = {'L':'Length L [nm]', 'R': 'Radius R [nm]'}[x_param]
        axs[i].set_xlabel(xlabel)
        axs[i].set_ylabel(r'$V_3 [V]$')
        # axs[i].set_title("Pinch off voltage V3 for QPC shape '{}'".format(data.p_geom['qpc_name'][0]))
        axs[i].set_xscale('log')
        
        ax_labels = axs[i].twiny()
        ax_labels.set_xscale('log')
        ax_labels.set_xlim(axs[i].get_xlim())
        ax_labels.set_xticks(x)
        ax_labels.set_xticklabels(labels)

    
    axs[-1].legend(loc='lower right')
    
    for ax, label in zip(axs, ('a)', 'b)')):
        # label physical distance to the left and up:
        trans = mpl.transforms.ScaledTranslation(-20/72, 7/72, fig.dpi_scale_trans)
        ax.text(0.0, 1.0, label, transform=ax.transAxes + trans,
                fontsize='medium', va='bottom', fontfamily='latin modern')

    plt.subplots_adjust(hspace=0.5)
    plt.show()
    
    fig.savefig('V3.pdf')
```

```python
plot_V3([files_a_fine, files_b_fine])
```

```python

```

```python

```
