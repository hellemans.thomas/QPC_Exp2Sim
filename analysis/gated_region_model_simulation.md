---
jupyter:
  jupytext:
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.13.8
  kernelspec:
    display_name: Python 3 (ipykernel)
    language: python
    name: python3
---

```python
import qpc_exp2sim.tools.data_handling as dh
import qpc_exp2sim.tools.plotting_sim as plotting
from qpc_exp2sim.simulation import builder, solver

import numpy as np
from __future__ import print_function
from ipywidgets import interact, interactive, fixed, interact_manual
import ipywidgets as widgets
from types import SimpleNamespace
import scipy.constants as c
import matplotlib.pyplot as plt
#%matplotlib notebook
```

```python
data_3D = dh.read_file('system_3D/calibration/full_gate_calibration_fine_sc')
p_geom = data_3D.p_geom
p_sim = data_3D.p_sim
```

```python
%matplotlib notebook
V = np.linspace(-2,2,100)

# Quantum model with linear helmholtz equation
D1 = (p_geom.d1 + p_geom.d_2DEG/2 + p_geom.d2/2)*1e-9
D2 = (p_geom.d2/2 + p_geom.d3 + p_geom.d4 + p_geom.d_contact/2)*1e-9

rho = p_geom.effective_mass * c.electron_mass /(c.pi * c.hbar **2)
c_quan = c.elementary_charge**2 * rho
c_geom = c.epsilon_0 *13 / (D1+D2)
c_eq = 1/(1/c_quan + 1/c_geom)
V_1 = -D2*c.elementary_charge*p_sim.DEG_dens/(c.epsilon_0*13)
V_calibration = (1/c_quan + D1/(c.epsilon_0 * 13))*c.elementary_charge*p_sim.DEG_dens

n_model = c_eq*(V-V_1)/c.elementary_charge
slope_model = c_eq/c.elementary_charge

# factor = c.elementary_charge**2 * rho * d1 / (c.epsilon_0 *13 + c.elementary_charge**2 * rho * (d1 + d2))
# n0 = factor*p_sim.DEG_dens

# slope_model = factor * c.epsilon_0 * 13 / (c.elementary_charge*d1)
# depletion_voltage = - c.elementary_charge * d1 * p_sim.DEG_dens / (c.epsilon_0 * 13)
# calibration_voltage = (1-1/factor)*depletion_voltage
#calibration_voltage = (p_sim.DEG_dens - n0)/slope_model

#n_quantum = n0 + slope_model * V

print('CAPACITOR MODEL:')
print("- Slope density curve: {:e}".format(slope_model))
print("- Depletion voltage:", V_1)
print("- Calibration voltage:", V_calibration)

# PESCADO MODEL IN 3D (FULL GATE)
data_3D = dh.read_file('system_3D/calibration/full_gate_calibration_fine_sc')

# unpacking the data
p_geom_3D = data_3D.p_geom
V_gates_list_3D = data_3D.p_sim.V_gates_list
coord_3D = data_3D.coord
charge_3D = data_3D.charge
V3_3D = data_3D.V3

_, charge_3D = plotting.data_cross_section(coord_3D, charge_3D, section=[0,0,0])
charge_3D_m2 = charge_3D[0,:,0] / np.product(np.array(p_geom_3D.grid_fine[:-1])) * 1e18
slope_pes_3D = np.gradient(charge_3D_m2, V_gates_list_3D[1] - V_gates_list_3D[0])[-1]

print('PESCADO 3D:')
print("- Slope density curve: {:e}".format(slope_pes_3D))
print("- Depletion voltage:", V3_3D[0][0])
print("- Calibration voltage:", )

# PESCADO MODEL IN 1D 
data_1D = dh.read_file('system_1D/sc_cal_false')

# unpacking the data
p_geom_1D = data_1D.p_geom
V_gates_list_1D = data_1D.p_sim.V_gates_list
coord_1D = data_1D.coord
charge_1D = data_1D.charge
V3_1D = data_1D.V3

_, charge_1D = plotting.data_cross_section(coord_1D, charge_1D, section=[0])
charge_1D_m2 = charge_1D[0,:,0] / np.product(np.array(p_geom_1D.grid_fine[:-1])) * 1e18
slope_pes_1D = np.gradient(charge_1D_m2, V_gates_list_1D[1] - V_gates_list_1D[0])[-1]

print('PESCADO 1D:')
print("- Slope density curve: {:e}".format(slope_pes_1D))
print("- Depletion voltage:", V3_1D[0][0])
print("- Calibration voltage:", )


# Plotting

setting_lines = {'color':'k', 'linestyle':'--', 'linewidth':0.5}

fix, ax = plt.subplots(figsize=[7,7])
ax.plot(V, n_model, label='cap model')
plotting.plot_line(V_gates_list_3D, charge_3D, p_geom_3D, ax=ax, setting_y=2, setting_x=3, show=False)
plotting.plot_line(V_gates_list_1D, charge_1D, p_geom_1D, ax=ax, setting_y=2, setting_x=3, show=False)

# Vertical reference lines
ax.axvline(V_calibration, **setting_lines)
ax.axvline(V_1, **setting_lines)
ax.axvline(0, **setting_lines)

# Horizontal reference lines
ax.axhline(0, **setting_lines)
ax.axhline(2.8e15, **setting_lines)
ax.set_xlim([-0.3,0.3])
#plt.xlim([0.2632,0.2634])
ax.set_ylim([-1e14, 2.9e15])
plt.legend()
plt.show();
```

```python
rho = p_geom.effective_mass * c.electron_mass /(c.pi * c.hbar **2)
```

```python
p_sim.DEG_dens
```

```python
p_sim.DEG_dens/(rho*c.elementary_charge)*60
```

```python
D2
```

```python

```
