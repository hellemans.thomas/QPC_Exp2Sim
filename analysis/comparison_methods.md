---
jupyter:
  jupytext:
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.13.8
  kernelspec:
    display_name: Python 3 (ipykernel)
    language: python
    name: python3
---

# Comparison different approximations

In this notebook we compare the results of the following methods for the same problem:
* The linear helmholtz solver ('lhh')
* The piecewise linear solver for the bulk ldos (step function) ('pi_bulk')
* The Newton-Raphson solver with lattice (i)ldos ('nr_lat')
* The full self consistent solver using Newton Raphson. A recalculation of the complete ldos is done in each iteration. ('nr_fsc')

```python
import pickle
import matplotlib.pyplot as plt
import numpy as np
import scipy.constants as c
from scipy.interpolate import interp1d
import warnings
import matplotlib as mpl

from qpc_exp2sim.pescado_kwant import plotting_pes, extension_pes
from qpc_exp2sim.tools import plotting_sim
import qpc_exp2sim.tools.data_handling as dh
%matplotlib notebook
```

## 0) Reading input data

```python
#file_geom = 'a4_grid10'
#files_sim = ['V64_a4_lhh', 'V64_a4_pi_bulk', 'V64_a4_nr_lat', 'V64_a4_fsc_nr']

qpc = 'a2'
file_geom = '{}_grid10'.format(qpc)
files_sim = [file.format(qpc) for file in 
             ['V4_{}_pi_bulk', 'V4_{}_nr_lat_m100_it5', 'V4_{}_fsc_nr_m100_it20']]

# files_sim = ['V3_lhh', 'V3_pi_bulk', 'V3_nr_lat', 'V3_fsc_nr_m100_it10']

files = ['files_data/' + file_geom + '_' + file_sim for file_sim in files_sim]
labels = [r'$DOS~cont$', r'$DOS~TB_0$', r'$SCQE$']
```

```python
datas = []
coords = []
V_gates_lists= []
ldoss = []
voltages= []
charges = []
conductances = []

for file in files:
    data_elem = dh.read_file(file)
    datas.append(data_elem)
    
    # If there is no voltage/charge/ldos stored raise a warning
    if data_elem.voltage is None:
        voltages.append(None)
        charges.append(None)
        ldoss.append(None)
        warnings.warn("File {} doesn't have any voltage, charge or ldos stored.".format(file))
    
    # We only consider the last iteration if the iteration data is stored
    elif isinstance(data_elem.voltage[0], (list, np.ndarray)):
        voltages.append(data_elem.voltage[:,-1])
        charges.append(data_elem.charge[:,-1])
        ldoss.append(data_elem.ldos[:,-1])
    else:
        voltages.append(data_elem.voltage)
        charges.append(data_elem.charge)
        ldoss.append(data_elem.ldos)
    coords.append(data_elem.coord)
    conductances.append(data_elem.conductance)
    V_gates_lists.append(data_elem.p_sim['V_gates_list'])
    
V_gates_list = data_elem.p_sim['V_gates_list']
helmholtz_coef = data_elem.p_sim['helmholtz_coef']
conversion_unit = c.elementary_charge/data_elem.p_geom['t']
cell_area = data_elem.p_geom['grid'][0] * data_elem.p_geom['grid'][1] * 1e-18
```

```python
index_v = 2
print('The gate voltage is {} V'.format(V_gates_list[index_v]))
```

## 1) LDOS and ILDOS ifo energy

```python
cell_area
```

```python
fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(8,4))

left, bottom, width, height = [0.59, 0.58, 0.2, 0.3]
ax_inset = fig.add_axes([left, bottom, width, height])


# Obtaining the correct ldos data
margin = 0.999
ldos_nr_lat = ldoss[-2] # -2 necessary to select the lattice ildos (ldoss = [None, lat ildos, list of iteration ildos])
bottom_band_nr_lat = (-ldos_nr_lat.a + ldos_nr_lat.b) * margin
top_band_nr_lat = (ldos_nr_lat.a + ldos_nr_lat.b)  * margin
energy_nr_lat = np.linspace(bottom_band_nr_lat, top_band_nr_lat, 1000)

ldos_nr_fsc = ldoss[-1][index_v]
bottom_band_nr_fsc = (-ldos_nr_fsc.a + ldos_nr_fsc.b) * margin
top_band_nr_fsc = (ldos_nr_fsc.a + ldos_nr_fsc.b) * margin
energy_nr_fsc = np.linspace(bottom_band_nr_fsc, top_band_nr_fsc, 1000)

# Linear helmholtz case
#ldos_lhh = np.array([[-1, 1], [helmholtz_coef, helmholtz_coef]])
#ildos_lhh = np.array([[-1, 0, 1], [-helmholtz_coef, 0, helmholtz_coef]])

#ax1.plot(ldos_lhh[0], ldos_lhh[1])
#ax2.plot(ildos_lhh[0], ildos_lhh[1])

# Piecewise bulk lattice case
ldos_pi_bulk = np.array([[-1, 0, 0, 1], [0, 0, helmholtz_coef, helmholtz_coef ]])
ildos_pi_bulk = np.array([[-1, 0, 1], [0, 0, helmholtz_coef]])

ax1.plot(1e3*ldos_pi_bulk[0], ldos_pi_bulk[1]*(1e-3/cell_area))
ax_inset.plot(1e3*ildos_pi_bulk[0], ildos_pi_bulk[1]/cell_area)

# Newton Raphson Lattice
ildos_nr_lat = 2 * ldos_nr_lat.integrate(energy_nr_lat)
ldos_nr_lat = 2 * conversion_unit * ldos_nr_lat(energy_nr_lat)
voltage_nr_lat = energy_nr_lat/conversion_unit
voltage_nr_lat = np.insert(voltage_nr_lat, 0, -1)
ildos_nr_lat = np.insert(ildos_nr_lat, 0, 0)

ax1.plot(1e3*voltage_nr_lat[1:], ldos_nr_lat*(1e-3/cell_area))
ax_inset.plot(1e3*voltage_nr_lat, ildos_nr_lat/cell_area)

# Newton Raphson Full Self Consistent
index = extension_pes.indices_from_coordinates(coords[-1], np.array([[0,0,0]]))[0]

assert (index in ldos_nr_fsc.indices), ('The asked index is not in the '
    'ExactLdos object, this might be due to the fact that the specific '
    'location is depleted for the asked gate voltega (index_v).')

ildos_nr_fsc = 2 * ldos_nr_fsc.integrate(energy_nr_fsc, index)
ldos_nr_fsc = 2 * conversion_unit * ldos_nr_fsc(energy_nr_fsc, index)
voltage_nr_fsc = energy_nr_fsc/conversion_unit + voltages[-1][index_v][index]
voltage_nr_fsc = np.insert(voltage_nr_fsc, 0, -1)
ildos_nr_fsc = np.insert(ildos_nr_fsc, 0, 0)

ax1.plot(1e3*voltage_nr_fsc[1:], ldos_nr_fsc*(1e-3/cell_area))
ax_inset.plot(1e3*voltage_nr_fsc, ildos_nr_fsc/cell_area)

ax1.set_xlim([-20, 50])
ax_inset.set_xlim([-20, 50])
#ax3.set_xlim([-0.02, 0.05])
#ax4.set_xlim([-0.02, 0.05])

ax_inset.set_ylim([-1e14, 2.1/cell_area])

ax1.set_xlabel('$\mu~[meV]$')
ax2.set_xlabel('$\mu~[meV]$')

ax1.set_ylabel('$LDOS~[m^{-2}~meV^{-1}]$')
ax2.set_ylabel('$ILDOS~[m^{-2}]$')

# To make the difference between Newton Raphson Lattice and Full Self consistent
# we need to interpolate one of the 2 curves since they are not defined in the 
# same points
# f_ldos_nr_fsc = interp1d(voltage_nr_fsc[1:], ldos_nr_fsc[:,0], 'cubic')
# ax3.plot(voltage_nr_lat[1:], f_ldos_nr_fsc(voltage_nr_lat[1:]) - ldos_nr_lat[:,0])
# ax3.set_ylabel(r'$\Delta$ldos')

# f_ildos_nr_fsc = interp1d(voltage_nr_fsc, ildos_nr_fsc, 'cubic')
# ax4.plot(voltage_nr_lat, f_ildos_nr_fsc(voltage_nr_lat) - ildos_nr_lat)
# ax4.set_ylabel(r'$\Delta$ildos')

ax1.legend(labels, loc='upper left')

ax2.plot(1e3*ildos_pi_bulk[0], ildos_pi_bulk[1]/cell_area)
ax2.plot(1e3*voltage_nr_lat, ildos_nr_lat/cell_area)
ax2.plot(1e3*voltage_nr_fsc, ildos_nr_fsc/cell_area)

ax2.set_xlim([-2, 10])
ax2.set_ylim([0, 3e15])


ax_inset.indicate_inset_zoom(ax2, edgecolor="tab:red")
ax_inset.set_xticks([])
ax_inset.set_yticks([])


axs = ax1, ax2
labels_fig = 'a)', 'b)'

for ax, label in zip(axs, labels_fig):
    # label physical distance to the left and up:
    trans = mpl.transforms.ScaledTranslation(-20/72, 7/72, fig.dpi_scale_trans)
    ax.text(0.0, 1.0, label, transform=ax.transAxes + trans,
            fontsize='medium', va='bottom', fontfamily='latin modern')

plt.tight_layout()
plt.show()
```

```python
fig.savefig('ldos_ildos_methods.pdf')
```

### 1.1) ILDOS curves for the Pescado section in the report

#### Various profiles for the ILDOS curve

```python
import matplotlib.transforms as mtransforms

fig, axs = plt.subplots(1, 3, figsize=(9,3))

# Linear helmholtz problem
shift = 0.4
ildos_lhh = np.array([[-1, 0, 1], [-helmholtz_coef, 0, helmholtz_coef]])
ildos_lhh[1] += shift
axs[0].plot(ildos_lhh[0], ildos_lhh[1])
axs[0].scatter(0, shift, marker='x', color='r')

plotting_sim.AngleAnnotation(ildos_lhh[:,1], (1,0), ildos_lhh[:,2], ax=axs[0], size=40, text=r"$\rho$",
                linestyle="-", color="r", textposition="outside",
                text_kw=dict(fontsize=12, color="r"))

axs[0].plot([-0.01, 0.01], [shift, shift], linestyle='--', linewidth=0.5, color='k')
axs[0].annotate(r'$N^d$', 
                (0, shift),
                textcoords="offset points", # how to position the text
                xytext=(-20,5),
                fontsize=12,
                color='r')

# Piecewise linear helmholtz problem
axs[1].plot(ildos_pi_bulk[0], ildos_pi_bulk[1])

plotting_sim.AngleAnnotation((0,0), (1,0), (1, helmholtz_coef), ax=axs[1], size=40, text=r"$\rho$",
                linestyle="-", color="r", textposition="outside",
                text_kw=dict(fontsize=12, color="r"))

# Smooth helmholtz problem
axs[2].plot(voltage_nr_lat, ildos_nr_lat)

labels_fig = ['a)', 'b)', 'c)']

for ax, label_fig in zip(axs, labels_fig):
    # Move the left and bottom spines to x = 0 and y = 0, respectively.
    ax.spines[["left", "bottom"]].set_position(("data", 0))
    # Hide the top and right spines.
    ax.spines[["top", "right"]].set_visible(False)

    # Draw arrows (as black triangles: ">k"/"^k") at the end of the axes.  In each
    # case, one of the coordinates (0) is a data coordinate (i.e., y = 0 or x = 0,
    # respectively) and the other one (1) is an axes coordinate (i.e., at the very
    # right/top of the axes).  Also, disable clipping (clip_on=False) as the marker
    # actually spills out of the axes.
    ax.plot(1, 0, ">k", transform=ax.get_yaxis_transform(), clip_on=False)
    ax.plot(0, 1, "^k", transform=ax.get_xaxis_transform(), clip_on=False)
    
    # Remove the ticks from the axes
    ax.set_xticks([])
    ax.set_yticks([])
    
    # Include ax labels
    ax.set_xlabel(r'$\mu$', loc='right')
    ax.set_ylabel(r'$ILDOS$', rotation='horizontal', loc='top')
    
    # Setting the correct limits
    ax.set_xlim([-0.02, 0.05])
    ax.set_ylim([-0.3, 2.1])
    
    # label physical distance to the left and up:
    trans = mtransforms.ScaledTranslation(-20/72, 7/72, fig.dpi_scale_trans)
    ax.text(0.0, 1.0, label_fig, transform=ax.transAxes + trans,
            fontsize='medium', va='bottom', fontfamily='latin modern')
    
plt.tight_layout()
plt.show()
```

```python
fig.savefig('overview_ildos_possibilities.pdf')
```

#### Iteration scheme Newton Raphson solver

```python
# Smooth helmholtz problem
fig, axs = plt.subplots(1, 3, figsize=(9,3))

f_ildos_nr_lat = interp1d(voltage_nr_lat, ildos_nr_lat, 'cubic')
ldos_nr_lat = ldoss[-2]

xrange = 0.01
    
# Data iteration 0
mu0 = 0
N0 = f_ildos_nr_lat(mu0)
rho0 = 2 * conversion_unit * float(ldos_nr_lat(mu0*conversion_unit))
x_tangent0 = np.array([-0.01, 0.03])
y_tangent0 = N0 + (x_tangent0 - mu0) * rho0

# Data iteration 1
mu1 = 0.022
N1 = f_ildos_nr_lat(mu1)
rho1 = 2 * conversion_unit * float(ldos_nr_lat(mu1*conversion_unit))
x_tangent1 = np.array([mu1-xrange, mu1+xrange])
y_tangent1 = N1 + (x_tangent1 - mu1) * rho1

# Data Iteration 2
mu2 = 0.016
N2 = f_ildos_nr_lat(mu2)
rho2 = 2 * conversion_unit * float(ldos_nr_lat(mu2*conversion_unit))
x_tangent2 = np.array([mu2-xrange, mu2+xrange])
y_tangent2 = N2 + (x_tangent2 - mu2) * rho2

sol1 = N0 + (mu1-mu0)*rho0
sol2 = N1 + (mu2-mu1)*rho1

mu3 = mu2 + 0.0025
sol3 = N2 + (mu3-mu2)*rho2

# Plotting iteration 1
axs[0].scatter(mu0, N0, marker='x', color='tab:orange')
axs[0].scatter(mu1, sol1, marker='o', color='tab:orange',  facecolors='none')
axs[0].plot(x_tangent0, y_tangent0, linestyle='-', linewidth=0.5, color='tab:orange')

axs[0].plot([mu1, mu1], [0, sol1], linestyle='--', linewidth=0.5, color='gray' )

axs[0].set_xticks([mu0, mu1])
axs[0].set_xticklabels([r'$\mu_0$     ', r'$\mu_1$'])

# Plotting iteration 2
axs[1].scatter(mu0, N0, marker='x', color='tab:orange')
axs[1].scatter(mu1, sol1, marker='o', color='tab:orange',  facecolors='none')
axs[1].plot(x_tangent0, y_tangent0, linestyle='-', linewidth=0.5, color='tab:orange')

axs[1].plot([mu1, mu1], [0, N1], linestyle='--', linewidth=0.5, color='gray' )
axs[1].plot([mu2, mu2], [0, sol2], linestyle='--', linewidth=0.5, color='gray' )

axs[1].scatter(mu1, N1, marker='x', color='tab:green')
axs[1].scatter(mu2, sol2, marker='o', color='tab:green',  facecolors='none')
axs[1].plot(x_tangent1, y_tangent1, linestyle='-', linewidth=0.5, color='tab:green')

axs[1].set_xticks([mu1, mu2])
axs[1].set_xticklabels([r'$\mu_1$', r'$\mu_2$'])

# Plotting iteration 3
axs[2].scatter(mu1, N1, marker='x', color='tab:green')
axs[2].scatter(mu2, sol2, marker='o', color='tab:green',  facecolors='none')
axs[2].plot(x_tangent1, y_tangent1, linestyle='-', linewidth=0.5, color='tab:green')

axs[2].plot([mu2, mu2], [0, N2], linestyle='--', linewidth=0.5, color='gray' )
axs[2].plot([mu3, mu3], [0, sol3], linestyle='--', linewidth=0.5, color='gray' )

axs[2].scatter(mu2, N2, marker='x', color='tab:red')
axs[2].scatter(mu3, sol3, marker='o', color='tab:red',  facecolors='none')
axs[2].plot(x_tangent2, y_tangent2, linestyle='-', linewidth=0.5, color='tab:red')

axs[2].set_xticks([mu2, mu3])
axs[2].set_xticklabels([r'   $\mu_2$ $\mu_3$', ''])

# Layout figures
for n, (ax, label_fig) in enumerate(zip(axs, labels_fig)):
    # Plot the basic ILDOS curve
    ax.plot(voltage_nr_lat, ildos_nr_lat)
    
    # Move the left and bottom spines to x = 0 and y = 0, respectively.
    ax.spines[["left", "bottom"]].set_position(("data", 0))
    # Hide the top and right spines.
    ax.spines[["top", "right"]].set_visible(False)

    # Draw arrows (as black triangles: ">k"/"^k") at the end of the axes.  In each
    # case, one of the coordinates (0) is a data coordinate (i.e., y = 0 or x = 0,
    # respectively) and the other one (1) is an axes coordinate (i.e., at the very
    # right/top of the axes).  Also, disable clipping (clip_on=False) as the marker
    # actually spills out of the axes.
    ax.plot(1, 0, ">k", transform=ax.get_yaxis_transform(), clip_on=False)
    ax.plot(0, 1, "^k", transform=ax.get_xaxis_transform(), clip_on=False)
    
    # Remove the ticks from the axes
    ax.set_yticks([])
    
    # Include ax labels
    ax.set_xlabel(r'$\mu$', loc='right', labelpad = -11)
    ax.set_ylabel(r'$ILDOS$', rotation='horizontal', loc='top', labelpad = 0)
    
    # Setting the correct limits
    ax.set_xlim([-0.02, 0.05])
    ax.set_ylim([-0.2, 2.1])
    
    # label physical distance to the left and up:
    trans = mtransforms.ScaledTranslation(-20/72, 7/72, fig.dpi_scale_trans)
    ax.text(0.0, 1.0, label_fig, transform=ax.transAxes + trans,
            fontsize='medium', va='bottom', fontfamily='latin modern')
    
    ax.set_title('Iteration {}'.format(n+1), y=-0.15, fontweight = 'bold', color=['tab:orange', 'tab:green', 'tab:red'][n])

plt.tight_layout()
plt.show()
```

```python

```

```python
fig.savefig('newton_raphson_iterations.pdf')
```

## 2) Ldos, voltage and charge

```python
section = [None, 0, 0]

fig, ((ax1, ax2, ax3), (ax4, ax5, ax6)) = plt.subplots(2,3, figsize=[10,5], gridspec_kw={'height_ratios': [1, 1]}, sharex=True)


# Voltage and charge results
for voltage, charge, coord in zip(voltages, charges, coords):
    coord_1D, voltage_1D = plotting_pes.data_cross_section(coord, voltage[index_v], section=section)
    coord_1D, charge_1D = plotting_pes.data_cross_section(coord, charge[index_v], section=section)
    
    ax2.plot(coord_1D[:,0], -1e3*voltage_1D[:,0,0])
    ax3.plot(coord_1D[:,0], charge_1D[:,0,0]/cell_area)
    
coord_1D_nr_lat, voltage_1D_nr_lat = plotting_pes.data_cross_section(coords[-2], voltages[-2][index_v], section=section)
coord_1D_nr_lat, charge_1D_nr_lat = plotting_pes.data_cross_section(coords[-2], charges[-2][index_v], section=section)

coord_1D_nr_fsc, voltage_1D_nr_fsc = plotting_pes.data_cross_section(coords[-1], voltages[-1][index_v], section=section)
coord_1D_nr_fsc, charge_1D_nr_fsc = plotting_pes.data_cross_section(coords[-1], charges[-1][index_v], section=section)

assert np.all(coord_1D_nr_fsc == coord_1D_nr_lat)

ax5.plot(coord_1D_nr_lat[:,0], -1e3*(voltage_1D_nr_fsc - voltage_1D_nr_lat)[:,0,0], 'k', linewidth = 0.8)
ax6.plot(coord_1D_nr_lat[:,0], (charge_1D_nr_fsc - charge_1D_nr_lat)[:,0,0]/cell_area, 'k',  linewidth = 0.8)

label = ['x', 'y']
n = section.index(None)
m = (n + 1) % 2

# LDOS results
ldos_tb = ldoss[1](energy = voltage_1D_nr_lat.flatten() * conversion_unit)

ldos_scqe = ldoss[2][index_v]
coord_scqe = coords[2][ldos_scqe.indices][:,0:2]
voltage_scqe = voltages[2][index_v][ldos_scqe.indices]
origin_scqe, slope_scqe = ldos_scqe.quantum_functional_kwant(voltage_scqe)
coord_1D_scqe, ldos_1D_scqe = plotting_pes.data_cross_section(coord_scqe, [slope_scqe], section=section[0:2])

ax1.plot(coord_1D_nr_lat, np.zeros(coord_1D_nr_lat.shape)+datas[0].p_sim['helmholtz_coef']*(1e-3/cell_area))
ax1.plot(coord_1D_nr_lat, 2*ldos_tb*(1e-3*conversion_unit/cell_area)) # From kwant indices to /meV/m^2 taking into account spin degeneracy
ax1.plot(coord_1D_scqe, ldos_1D_scqe.flatten()*(1e-3/cell_area)) # From pescado units to /meV/m^2

ax4.plot(coord_1D_scqe, ldos_1D_scqe.flatten()*(1e-3/cell_area) -  2*ldos_tb.flatten()*(1e-3*conversion_unit/cell_area), 
         'k',  linewidth = 0.8)

# Setting the correct labels
ax1.set_title(r'$LDOS~\varrho$')
ax2.set_title('Potential U')
ax3.set_title('Charge density n')

ax4.set_xlabel('{} [nm]'.format(label[n]))
ax5.set_xlabel('{} [nm]'.format(label[n]))
ax6.set_xlabel('{} [nm]'.format(label[n]))

ax1.set_ylabel(r'$\varrho~[m^{-2}~meV^{-1}]$')
ax2.set_ylabel(r'$U~[meV]$')
ax3.set_ylabel(r'$n~[m^{-2}]$')
ax4.set_ylabel(r'$\Delta\varrho~[m^{-2}~meV^{-1}]$')
ax5.set_ylabel(r'$\Delta U~[meV]$')
ax6.set_ylabel(r'$\Delta n~[m^{-2}]$')

ax3.legend(labels, loc='upper right')

axs = ax1, ax2, ax3, ax4, ax5, ax6
labels_fig = 'a)', 'b)', 'c)', 'd)', 'e)', 'f)'

for ax, label in zip(axs, labels_fig):
    # label physical distance to the left and up:
    trans = mpl.transforms.ScaledTranslation(-20/72, 7/72, fig.dpi_scale_trans)
    ax.text(0.0, 1.0, label, transform=ax.transAxes + trans,
            fontsize='medium', va='bottom', fontfamily='latin modern')

plt.tight_layout()
plt.show()
```

```python
fig.savefig('comparison_methods_voltage_dens.pdf')
```

```python
# At first sight, voltage and charge seem to oscillate in phase, 
# while the ldos oscillates 90° out of phase with the voltage and charge

# Interesting to fit with a wavepacket (sinusoidal oscillations with gaussian envelope)?

fig, ax = plt.subplots()

diff_ldos = ldos_1D_scqe.flatten()*(1e-3/cell_area) -  2*ldos_tb.flatten()*(1e-3*conversion_unit/cell_area)
diff_volt = -1e3*(voltage_1D_nr_fsc - voltage_1D_nr_lat)[:,0,0]
diff_char = (charge_1D_nr_fsc - charge_1D_nr_lat)[:,0,0]/cell_area

ax.plot(coord_1D_scqe, diff_ldos/1e14)
ax.plot(coord_1D_nr_lat[:,0], 6*diff_volt)
ax.plot(coord_1D_nr_lat[:,0], diff_char/2e13)

plt.show()

```

```python

```
