---
jupyter:
  jupytext:
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.13.8
  kernelspec:
    display_name: Python 3 (ipykernel)
    language: python
    name: python3
---

```python
raise Exception("This file may not be run all at once!")
```

```python
import qpc_exp2sim.tools.data_handling as dh
import qpc_exp2sim.tools.plotting_sim as plotting
from qpc_exp2sim.simulation import builder, solver

import numpy as np
from scipy import constants
from types import SimpleNamespace
```

```python
# p, coord, voltage, charge, V3, conductance = dh.read_file('system_3D/b3_grid20')
# p_geom = p
# p_sim = p
data = dh.read_file('system_3D/a2')

# unpacking the data
# p_geom = data.p_geom
# p_sim = data.p_sim
# V_gates_list = p_sim.V_gates_list
# coord = data.coord
# voltage = data.voltage
# charge = data.charge
# conductance = data.conductance
# voltage_shift = data.voltage_shift
# V3 = data.V3
```

```python
data = dh.read_file('system_3D/a4')
p, coord, voltage, charge, V3 = data

p_geom = SimpleNamespace(grid_fine = [p.a_xy_fine, p.a_xy_fine, p.a_z_fine],                  # Mesh in xyz direction in device (Pescado mesh + Kwant lattice)
                         grid_coarse = [p.a_xy_coarse, p.a_xy_coarse, p.a_z_coarse],              # Mesh in xyz direction around device (Pescado mesh)
                         section = [None, None, None],              # None if you don't take a cross section for this ax
                         L = p.L,                                  # Lenth total device in x direction = transport direction
                         W = p.W,                                  # Width total device in y direction = gate direction
                         L_narrow_gate = p.L_narrow_gate,                        # Length of the narrow gate 
                         W_narrow_gate = p.W_narrow_gate,                       # Widthh of the narrow gate
                         qpc_name = 'a4',
                         d0 = p.d0,                                   # Thickness GaAs layer bottom
                         d_2DEG = p.d_2DEG,                               # Thickness 2DEG 
                         d1 = p.d1,                                   # Thickness AlGaAs above 2DEG
                         d2 = p.d2,                                   # Thickness AlGaAs, Si doped
                         d3 = p.d3,                                   # Thickness AlGaAs 
                         d4 = p.d4,                                   # Thickness GaAs
                         d_contact = p.d_contact,                            # Thickness contact layer
                         scaling = p.scaling,                               # Size of the region around the device to be taken into account in Pescado -> not implemented yet
                         eps_GaAs = p.eps_GaAs,                             # Dielectric constant GaAs
                         eps_AlGaAs = p.eps_AlGaAs,                           # Dielectric constant AlGaAs
                         eps_air = p.eps_air,                               # Dielectric constant air/vacuum (around gates)
                         eps_gates = p.eps_gates,                           # Dielectric constant gates (metal)
                         effective_mass = p.effective_mass,                    # Effective mass for 2DEG DOS calculation
                         setting_pescado_solver= p.setting_pescado_solver)               # Determines if the 2DEG is set to flexible or helmholtz

p_sim = SimpleNamespace(# Boundary conditions
                        V_gates_list = p.V_gates_list, 
                        DEG_dens = p.DEG_dens,                # Electron density in 2DEG [/m²]
                        energy_list = np.array([0]),
    
                        # Simulation parameters
                        calibration = True, 
                        calc_V3 = True, 
                        accuracy = 1e-2,                   #Accuracy with which V3 needs to be calculated
                        order = 2,                         #Order polynome extrapolation V3
                        calc_cond = True)

builder.extend_params(p_geom)
kwant_sys = builder.make_kwant_system(p_geom)
conductance = solver.conductance_solver(coord, kwant_sys, voltage, p_sim.V_gates_list)

data_tw = SimpleNamespace(p_geom = p_geom, 
                           p_sim = p_sim,
                           coord = coord,
                           voltage = voltage, 
                           charge = charge, 
                           conductance = conductance,
                           voltage_shift = p.calibration_voltage,
                           V3 = V3)

dh.write_file(data_tw, 'system_3D/' + p_geom.qpc_name + '_grid{}'.format(p_geom.grid_fine[0]))
```

## Extend the current data with a conductance calculation

```python
solver.conductance_extend_file('system_3D/data_files/data3')
```

## Extend the current data with V3 calculated from the conductance

```python
files = ['b2_grid10_fine', 'b3_grid10_fine']

for file in files:
    data = dh.read_file('system_3D/fine/' + file)
    if data.V3[1] is None:
        _, cross_points, _ =  solver.crossings(data.p_sim.V_gates_list, data.conductance)
        V3_cond = cross_points[0,0]
        data.V3[1] = V3_cond
        dh.write_file(data,'system_3D/fine/' + file)
    else:
        print('For file {}, V3[1] is not None'.format(file))
```

## Extend the current data with V3 calculated from the density

```python
files = ['data_files/full_gate_calibration_fine']

for file in files:
    data = dh.read_file('system_3D/' + file)
    if data.V3[0] == [-float('inf'), None]:
        [V3_dens, precision] = solver.V3_dens_solver(data.p_sim.V_gates_list, data.coord, data.charge)
        data.V3[0] = [V3_dens, precision]
        dh.write_file(data,'system_3D/' + file)
    else:
        print('For file {}, V3[0] is not empty'.format(file))
```

```python
file = 'system_3D/calibration/full_gate_calibration_fine_po'

data = dh.read_file(file)
data.p_sim.V_gates_list = data.p_sim.V_gates_list[::2]
data.charge = data.charge[::2]
data.voltage = data.voltage[::2]
dh.write_file(data, file)

```

## Extend the parameters in the input files for the cluster

```python
from os import listdir
from os.path import isfile, join
from types import SimpleNamespace
mypath = '/home/thellemans/Documents/qpc_exp2sim/data_simulation/input_files'
file_list = [f for f in listdir(mypath) if isfile(join(mypath, f)) and f[0] != '.']

for file in file_list:
    p_geom, p_sim = dh.read_file('../data_simulation/input_files/' + file)
    
    input_data = SimpleNamespace(p_geom = p_geom,
                                 p_sim = p_sim)
    
    dh.write_file(input_data, '../data_simulation/input_files/' + file)
```

```python
file = 'b3_grid10_fine'
p_geom, p_sim = dh.read_file('../data_simulation/input_files/' + file)
    

input_data = SimpleNamespace(p_geom = p_geom,
                             p_sim = p_sim)
    
dh.write_file(input_data, '../data_simulation/input_files/' + file)
```

```python
for file in file_list:
    print('fine' in file)
```

```python
data = dh.read_file('../data_simulation/input_files/' + file)
```

```python
data1 = dh.read_file('system_3D/fine/b3_grid10_fine')
data2 = dh.read_file('system_3D/data_files/b3_grid10_fine')

```

```python
data1.p_sim.charge_sv = None
```

```python
data1.p_sim.__dict__
```

```python
data2.p_sim.__dict__
```

```python
dh.join_data([data1, data2])
```

```python
data2.conductance.shape
```

```python
0 in np.linspace(-2.5, 7.5, 1001)
```

```python

```
