---
jupyter:
  jupytext:
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.13.8
  kernelspec:
    display_name: Python 3 (ipykernel)
    language: python
    name: python3
---

# Data analysis: KPM calculations and it's influence on the voltage and charge

**Table of contents**

0. Loading data
1. Input parameters
2. LDOS and ILDOS
    * Evaluation LDOS and ILDOS
    * Colormap LDOS and ILDOS
    * Cross sections LDOS and ILDOS
    * LDOS and ILDOS ifo energy
3. Voltage and charge
    * Colormap voltage and charge
    * Cross sections voltage and charge
4. Convergence curves
    * Relative error
    * Absolute error

```python
import scipy.constants as c
import numpy as np
import matplotlib.pyplot as plt
from  qpc_exp2sim.tools import plotting_sim
import qpc_exp2sim.tools.data_handling as dh
from qpc_exp2sim.pescado_kwant import plotting_pes, pescado_kwant
import matplotlib as mpl
%matplotlib notebook
```

## 0) Loading data

```python
# Load a file for which the iterations are stored away
geom_file = 'a2_grid10'
sim_file = 'V4_a2_fsc_nr_m100_it20'

#data = dh.read_file('files_data/'+ geom_file + '_' + sim_file)
data = dh.read_file('files_data/a2_grid10_500_2500_V4_a2_fsc_nr_m200_it5')

# unpacking the data for easy acces to everything
p_geom = data.p_geom
p_sim = data.p_sim
file_geom = data.file_geom
file_sim = data.file_sim
V_gates_list = p_sim['V_gates_list']
energy_list = p_sim['energy_list']
coord = data.coord
coord_kwant = data.coord_kwant
coord_kpm = data.coord_kpm
voltage = data.voltage
charge = data.charge
conductance = data.conductance
voltage_shift = data.voltage_shift
dopant_dens = data.dopant_dens
V3 = data.V3
ldos = data.ldos
```

```python
p_sim
```

```python

```

## 1) Input parameters

```python
cell_area = p_geom['grid'][0] * p_geom['grid'][1] * 1e-18
index_v = 1
index_e = 0
iteration = -1
print('The gate voltage is {} V'.format(V_gates_list[index_v]))
print('The energy is {} t'.format(energy_list[index_e]))

if not isinstance(voltage[index_v], (list, np.ndarray)):
    voltage = np.array([[elem] for elem in voltage])
    charge = np.array([[elem] for elem in charge])
    ldos = np.array([[elem] for elem in ldos])

potential = voltage * (-1) # Potential in eV
# params = {'lead_left': min(coord_kwant[:,0]), 
#           'lead_right': max(coord_kwant[:,0])}
# params.update({'potential' : potential[index_v][0], 'conversion_indices': pk_problem.conversion_indices})
# flexible_index = pk_problem.rpp_problem.linear_problem_inst.regions_index[3]

voltage_vec = voltage[index_v]
charge_vec = charge[index_v]

print('\n', data)
```

## 2) LDOS and ILDOS
### 2.1) Evaluation ldos and ildos ifo space
This is done for a single gate voltage but for all the iterations stored for that gate voltage.

```python
# Running this cell might take some time!
ldos_val = []
ildos_val = []
coord_val = []

for iteration in range(0, len(ldos[index_v])):
    ldos_loc = ldos[index_v][iteration]
    coord_loc = coord[ldos_loc.indices][:,0:2]
    voltage_loc = voltage[index_v][iteration][ldos_loc.indices]
    origin_loc, slope_loc = ldos_loc.quantum_functional_kwant(voltage_loc)
    
    coord_val.append(coord_loc)
    ldos_val.append(slope_loc)
    ildos_val.append(origin_loc + slope_loc * voltage_loc)
```

### 2.2) Colormap ldos and ildos 

```python
iteration0 = 5
iteration1 = 20

# If only a single (mostly the last) iteration is stored, plot this iteration
if len(ldos_val) == 1:
    iteration0 = 0
    fig, (ax1, ax2) = plt.subplots(1,2, figsize=[10,5])
    plotting_pes.colormap(coord_val[iteration0], ldos_val[iteration0], aspect_equal=True, 
                          ax=ax1, title='ldos kpm iteration {}'.format(p_sim['iterations']), 
                          xlabel='x [nm]', ylabel = 'y [nm]', interpolation='hamming') 
    plotting_pes.colormap(coord_val[iteration0], ildos_val[iteration0], cell_area=cell_area, aspect_equal=True, 
                          ax=ax2, title='ildos kpm iteration {}'.format(p_sim['iterations']), 
                          xlabel='x [nm]', ylabel = 'y [nm]', cbar_label='n [/m²]')
    plt.tight_layout()
    plt.show()
# Otherwise the possibility exists to compare multiple iterations (iteration0 and iteration1 specified above)
else:
    assert (0 not in [iteration0, iteration1]), (
        "Iteration 0 contains the bulk ildos, it's only interesting to plot it ifo energy.")

    fig, ((ax1, ax2), (ax3, ax4)) = plt.subplots(2,2, figsize=[10,10])

    cbar_ldos = [min([np.min(ldos_val[iteration0]), np.min(ldos_val[iteration1])]), 
                 max([np.max(ldos_val[iteration0]), np.max(ldos_val[iteration1])])]
    cbar_ildos = [min([np.min(ildos_val[iteration0]), np.min(ldos_val[iteration1])])/cell_area, 
                  max([np.max(ildos_val[iteration0]), np.max(ildos_val[iteration1])])/cell_area]

    cbar_ldos = [None, None]
    cbar_ildos = [None, None]

    plotting_pes.colormap(coord_val[iteration0], ldos_val[iteration0], aspect_equal=True, 
                          ax=ax1, title='ldos kpm iteration {}'.format(iteration0), 
                          cbar_range=cbar_ldos, xlabel='x (nm)', ylabel = 'y (nm)')    

    ax1.axvline(0, color='tab:red')
    ax1.axhline(0, color='tab:red')
    ax1.axhline(200, color='tab:red')

    plotting_pes.colormap(coord_val[iteration0], ildos_val[iteration0], cell_area=cell_area, aspect_equal=True, 
                          ax=ax2, title='ildos kpm iteration {}'.format(iteration0), 
                          cbar_range= cbar_ildos, xlabel='x [nm]', ylabel = 'y [nm]', cbar_label='n [/m²]')

    plotting_pes.colormap(coord_val[iteration1], ldos_val[iteration1], aspect_equal=True, 
                          ax=ax3, title='ldos kpm iteration {}'.format(iteration1), 
                          cbar_range=cbar_ldos, xlabel='x [nm]', ylabel = 'y [nm]')    
    plotting_pes.colormap(coord_val[iteration1], ildos_val[iteration1], cell_area=cell_area, aspect_equal=True, 
                          ax=ax4, title='ildos kpm iteration {}'.format(iteration1), 
                          cbar_range=cbar_ildos, xlabel='x [nm]', ylabel = 'y [nm]', cbar_label='n [/m²]')

    plt.tight_layout()
    plt.show()
```

```python
fig.savefig('kpm_ldos_ildos.pdf')
```

### 2.3) Cross section ldos and ildos

```python
fig, ((ax1, ax2), (ax3, ax4), (ax5, ax6)) = plt.subplots(3, 2, figsize=[10,15])

def ildos_ldos_section(coord_val, ldos_val, ildos_val, iteration, section, ax1, ax2):
    coord_1D, ldos_1D = plotting_pes.data_cross_section(coord_val[iteration], [ldos_val[iteration]], section=section)
    coord_1D, ildos_1D = plotting_pes.data_cross_section(coord_val[iteration], [ildos_val[iteration]], section=section)
        
    ax1.plot(coord_1D[:,0], ldos_1D[:,0,0])
    ax2.plot(coord_1D[:,0], ildos_1D[:,0,0]/cell_area)
    
    label = ['x', 'y']
    n = section.index(None)
    m = (n + 1) % 2
    ax1.set_title('ldos kpm, {} = {} nm'.format(label[m], section[m]))
    ax2.set_title('ildos kpm, {} = {} nm'.format(label[m], section[m]))
    
    ax1.set_xlabel('{} [nm]'.format(label[n]))
    ax2.set_xlabel('{} [nm]'.format(label[n]))
    
    ax1.set_ylabel('ldos')
    ax2.set_ylabel('ildos [/m²]')

if len(ldos_val) == 1:
    iterations = [0]
else:
    iterations = range(1, len(ldos_val))

for iteration in iterations: 
    section = [None, 0]
    ildos_ldos_section(coord_val, ldos_val, ildos_val, iteration, section, ax1, ax2)
    
    section = [None, 200]
    ildos_ldos_section(coord_val, ldos_val, ildos_val, iteration, section, ax3, ax4)
    
    section = [0, None]
    ildos_ldos_section(coord_val, ldos_val, ildos_val, iteration, section, ax5, ax6)

ax6.legend(['Iteration {}'.format(iteration+1) for iteration in iterations])
plt.tight_layout()
plt.show()
```

```python

```

```python
fig.savefig('ldos_ildos_cross_section.pdf')
```

### 2.3) ldos and ildos ifo energy


#### 2.3.1) For different sites

```python
iteration = -1
#sites = np.array([[0,0,0], [130,0,0], [350, 0, 0], [700, 0, 0]])
sites = np.array([[0,0,0], [50,0,0], [100, 0, 0], [0, 50, 0]])

indices = pescado_kwant.indices_from_coordinates(coord, sites)
bottom_band =  -ldos[index_v][iteration].a + ldos[index_v][iteration].b
top_band =  ldos[index_v][iteration].a + ldos[index_v][iteration].b
energy = np.linspace(bottom_band, top_band, 200)
ldos_energy = ldos[index_v][iteration](energy=energy, index=indices) * 2 * c.elementary_charge/p_geom['t']

f, (ax1, ax2) = plt.subplots(1, 2, figsize = (10,5), gridspec_kw={'width_ratios': [2, 1]})

ax1.plot(energy, ldos_energy)
ax1.axvline(0, color = 'k', linestyle = '--', linewidth = 0.5)

ax1.set_xlabel('energy [t]')
ax1.set_ylabel('ldos (pescado units)')
asp = np.diff(ax1.get_xlim())[0] / np.diff(ax1.get_ylim())[0]
ax1.set_aspect(asp/2)

colors = [line.get_color() for line in ax1.lines][:-1]
plotting_sim.plot_overview_points(p_geom, sites[:,0:2], ax2, color=colors)

plt.tight_layout()
plt.show()
```

#### 2.3.2) For different iterations
Doesn't work if there is only a single iteration stored away!

```python
iterations = range(1, len(ldos[index_v]))
site = np.array([[0, 0,0]])

index = pescado_kwant.indices_from_coordinates(coord, site)

f, (ax1, ax2) = plt.subplots(1, 2, figsize = (10,5), gridspec_kw={'width_ratios': [2, 1]})

for iteration in iterations:

    bottom_band =  0.999 * -ldos[index_v][iteration].a + ldos[index_v][iteration].b
    top_band =  0.999 * ldos[index_v][iteration].a + ldos[index_v][iteration].b
    energy = np.linspace(bottom_band, top_band, 300)
    ldos_energy = ldos[index_v][iteration](energy=energy, index=index) * 2 * c.elementary_charge/p_geom['t']

    ax1.plot(energy, ldos_energy)

ax1.axvline(0, color = 'k', linestyle = '--', linewidth = 0.5)

ax1.set_xlabel('energy [t]')
ax1.set_ylabel('ldos (pescado units)')
asp = np.diff(ax1.get_xlim())[0] / np.diff(ax1.get_ylim())[0]
ax1.set_aspect(asp/2)

plotting_sim.plot_overview_points(p_geom, site[0,0:2], ax2, color='k')

plt.tight_layout()
plt.show()
```

## 3) Voltage and charge
### 3.1) Colormap voltage and charge

```python
iteration0 = 0
iteration1 = 10

if len(charge_vec) == 1 or iteration0 == iteration1:
    if len(charge_vec) == 1:
        iteration0 = 0
        title = ' last iteration ({})'.format(p_sim['iterations'])
    else:
        title = ' iteration {}'.format(iteration0)
        
    section = [None, None, 0]

    fig, (ax1, ax2) = plt.subplots(1,2, figsize=[10,5])
    coord_2D, voltage_2D_it0 = plotting_pes.data_cross_section(coord, voltage_vec[iteration0], section=section)
    coord_2D, charge_2D_it0 = plotting_pes.data_cross_section(coord, charge_vec[iteration0], section=section)
    
    plotting_pes.colormap(coord_2D, voltage_2D_it0, aspect_equal=True, 
                          imshow = True, ax=ax1, title='Voltage'+title, 
                          xlabel='x (nm)', ylabel = 'y (nm)', cbar_label='Voltage [V]')    

    plotting_pes.colormap(coord_2D, charge_2D_it0, cell_area=cell_area, aspect_equal=True, 
                          imshow=True, ax=ax2, title='Density'+title, cmap = plt.cm.plasma,
                          xlabel='x (nm)', ylabel = 'y (nm)', cbar_label='n [/m²]')

    
    ax1.axvline(0, color='tab:red')
    ax1.axhline(0, color='tab:red')
    ax1.axhline(200, color='tab:red')
    
    plt.tight_layout()
    plt.show()
else: 
    # Making a cross section of the data
    fig, ((ax1, ax2), (ax3, ax4)) = plt.subplots(2,2, figsize=[10,10])

    section = [None, None, 0]
    coord_2D, voltage_2D_it0 = plotting_pes.data_cross_section(coord, voltage_vec[iteration0], section=section)
    coord_2D, charge_2D_it0 = plotting_pes.data_cross_section(coord, charge_vec[iteration0], section=section)
    coord_2D, voltage_2D_it1 = plotting_pes.data_cross_section(coord, voltage_vec[iteration1], section=section)
    coord_2D, charge_2D_it1 = plotting_pes.data_cross_section(coord, charge_vec[iteration1], section=section)

    cbar_voltage = [np.min([voltage_2D_it0, voltage_2D_it1]), np.max([voltage_2D_it0, voltage_2D_it1])]
    cbar_charge = [np.min([charge_2D_it0, charge_2D_it1])/cell_area, np.max([charge_2D_it0, charge_2D_it1])/cell_area]

    print('{:e}'.format(cbar_charge[0]), '{:e}'.format(cbar_charge[1]))

    plotting_pes.colormap(coord_2D, voltage_2D_it0, aspect_equal=True, imshow = True, ax=ax1, 
                          title='Voltage iteration {}'.format(iteration0), cbar_range=cbar_voltage, 
                          xlabel='x (nm)', ylabel = 'y (nm)', cbar_label='Voltage [V]')    

    ax1.axvline(0, color='tab:red')
    ax1.axhline(0, color='tab:red')
    ax1.axhline(200, color='tab:red')

    plotting_pes.colormap(coord_2D, charge_2D_it0, cell_area=cell_area, aspect_equal=True, imshow = True, ax=ax2, 
                          title='Density iteration {}'.format(iteration0), cbar_range= cbar_charge, 
                          xlabel='x (nm)', ylabel = 'y (nm)', cbar_label='n [/m²]', cmap = plt.cm.plasma) #cbar_range= [0,2.8e15])

    plotting_pes.colormap(coord_2D, voltage_2D_it1, aspect_equal=True, imshow = True, ax=ax3, 
                          title='Voltage iteration {}'.format(iteration1), cbar_range=cbar_voltage, 
                          xlabel='x (nm)', ylabel = 'y (nm)', cbar_label='Voltage [V]')
    
    plotting_pes.colormap(coord_2D, charge_2D_it1, cell_area=cell_area, aspect_equal=True, imshow = True, ax=ax4, 
                          title='Density iteration {}'.format(iteration1), cbar_range=cbar_charge, 
                          xlabel='x (nm)', ylabel = 'y (nm)', cbar_label='n [/m²]', cmap = plt.cm.plasma) #cbar_range= [0,2.8e15])

    plt.tight_layout()
    plt.show()
```

### 3.2) Cross section voltage and charge

```python
fig, ((ax1, ax2), (ax3, ax4), (ax5, ax6)) = plt.subplots(3, 2, figsize=[10,15])

for iteration in range(len(voltage_vec)):
    section = [None, 0, 0]
    
    coord_1D, voltage_1D = plotting_pes.data_cross_section(coord, voltage_vec[iteration], section=section)
    coord_1D, charge_1D = plotting_pes.data_cross_section(coord, charge_vec[iteration], section=section)
    
    ax1.plot(coord_1D[:,0], voltage_1D[:,0,0])
    ax2.plot(coord_1D[:,0], charge_1D[:,0,0]/cell_area)
    
    ax1.set_title('Voltage, y = 0 nm')
    ax2.set_title('Density, y = 0 nm')
    
    ax1.set_xlabel('x (nm)')
    ax2.set_xlabel('x (nm)')
    
    ax1.set_ylabel('voltage [V]')
    ax2.set_ylabel('n [/m²]')
    

    section = [None, 200, 0]
    
    coord_1D, voltage_1D = plotting_pes.data_cross_section(coord, voltage_vec[iteration], section=section)
    coord_1D, charge_1D = plotting_pes.data_cross_section(coord, charge_vec[iteration], section=section)
    
    ax3.plot(coord_1D[:,0], voltage_1D[:,0,0])
    ax4.plot(coord_1D[:,0], charge_1D[:,0,0]/cell_area)
    
    ax3.set_title('Voltage, y = {} nm'.format(section[1]))
    ax4.set_title('Density, y = {} nm'.format(section[1]))
    
    ax3.set_xlabel('x (nm)')
    ax4.set_xlabel('x (nm)')
    
    ax3.set_ylabel('voltage [V]')
    ax4.set_ylabel('n [/m²]')
    
    
    section = [0, None, 0]
    
    coord_1D, voltage_1D = plotting_pes.data_cross_section(coord, voltage_vec[iteration], section=section)
    coord_1D, charge_1D = plotting_pes.data_cross_section(coord, charge_vec[iteration], section=section)
    
    ax5.plot(coord_1D[:,0], voltage_1D[:,0,0])
    ax6.plot(coord_1D[:,0], charge_1D[:,0,0]/cell_area, label = 'Iteration {}'.format(iteration))
    
    ax5.set_title('Voltage, x = 0 nm')
    ax6.set_title('Density, x = 0 nm')
    
    ax5.set_xlabel('y (nm)')
    ax6.set_xlabel('y (nm)')
    
    ax5.set_ylabel('voltage [V]')
    ax6.set_ylabel('n [/m²]')
    
ax6.legend(loc = 'lower right')
plt.tight_layout()
plt.show()
```

## 4) Convergence curve


### 4.1) Relative error |(v[i] - v[-1]) / v[-1]|

```python
sites = np.array([[0,0,0], [0, 200, 0], [500, 0, 0]])
indices = pescado_kwant.indices_from_coordinates(coord, sites)

f, (ax1, ax2) = plt.subplots(1, 2, figsize = (10,5), gridspec_kw={'width_ratios': [2, 1]})

for index in indices:

    voltage_convergence = np.array([voltage_sv[index] for voltage_sv in voltage[index_v]])
    rel_error_voltage = np.abs((voltage_convergence[:-1] - voltage_convergence[-1])/voltage_convergence[-1])

    charge_convergence = np.array([charge_sv[index] for charge_sv in charge[index_v]])
    rel_error_charge = np.abs((charge_convergence[:-1] - charge_convergence[-1])/charge_convergence[-1])

    iterations = np.arange(len(voltage[index_v])-1)

    ax1.plot(iterations, rel_error_voltage, 'x-')
    ax1.plot(iterations, rel_error_charge, 'o--', color = ax1.lines[-1].get_color())

legend_elements = [mpl.lines.Line2D([0], [0], marker='x', linestyle = '-', color='k', label='Potential'), 
                   mpl.lines.Line2D([0], [0], marker='o', linestyle = '--', color='k', label='Charge density')]
    

ax1.set_title('Convergence curve')
ax1.set_xlabel('Number of iterations')
ax1.set_ylabel(r'$Relative~error~[-]$')
ax1.set_yscale('log')
ax1.legend(handles=legend_elements)

colors = [line.get_color() for line in ax1.lines][::2]
plotting_sim.plot_overview_points(p_geom, sites[:,0:2], ax2, color=colors)

plt.tight_layout()
plt.show()
```

### 4.2) Absolute error v[i] - v[-1]

```python
sites = np.array([[0,0,0], [0, 200,0], [500, 0, 0]])
indices = pescado_kwant.indices_from_coordinates(coord, sites)

f, (ax1, ax2) = plt.subplots(1, 2, figsize = (10,5), gridspec_kw={'width_ratios': [2, 1]})

charge_conv_list=[]
abs_error_list = []
for index in indices:

    voltage_convergence = np.array([voltage_sv[index] for voltage_sv in voltage[index_v]])
    abs_error_voltage = (voltage_convergence[:-1] - voltage_convergence[-1])
    charge_convergence = np.array([charge_sv[index] for charge_sv in charge[index_v]])
    abs_error_charge = (charge_convergence[:-1] - charge_convergence[-1])

    iterations = np.arange(len(voltage[index_v])-1)
    charge_conv_list.append(charge_convergence)
    abs_error_list.append(abs_error_charge)
    ax1.plot(iterations, -abs_error_voltage, 'x-') # The minus sign sets it to the absolute error on potential
    ax1.plot(iterations, abs_error_charge, 'o--', color = ax1.lines[-1].get_color())

legend_elements = [mpl.lines.Line2D([0], [0], marker='x', linestyle = '-', color='k', label='Potential [eV]'), 
                   mpl.lines.Line2D([0], [0], marker='o', linestyle = '--', color='k', label='Charge density')]
    

ax1.set_title('Convergence curve')
ax1.set_xlabel('Number of iterations')
ax1.set_ylabel(r'$Absolute~error$')
ax1.set_yscale('symlog', linthresh = 1e-8)
ax1.legend(handles=legend_elements)

colors = [line.get_color() for line in ax1.lines][::2]
plotting_sim.plot_overview_points(p_geom, sites[:,0:2], ax2, color=colors)

plt.tight_layout()
plt.show()
```

```python
cell_area
```

```python
sites = np.array([[0,0,0], [0, 200, 0], [500, 0, 0]])
indices = pescado_kwant.indices_from_coordinates(coord, sites)

fig, axs = plt.subplots(1, 3, figsize = (10,4), sharey=True)


for ax, index_v in zip(axs, (0,1,2)):
    for index in indices:

        voltage_convergence = np.array([voltage_sv[index] for voltage_sv in voltage[index_v]])
        rel_error_voltage = ((voltage_convergence[:-1] - voltage_convergence[-1])/voltage_convergence[-1])

        charge_convergence = np.array([charge_sv[index] for charge_sv in charge[index_v]])
        rel_error_charge = ((charge_convergence[:-1] - charge_convergence[-1])/charge_convergence[-1])

        iterations = np.arange(len(voltage[index_v])-1)

        ax.plot(iterations, rel_error_voltage, 'x-')
        ax.plot(iterations, rel_error_charge, 'o-', color = ax.lines[-1].get_color())

    ax.set_title(r'$V_g = {}~V$'.format(V_gates_list[index_v]))
        
    ax.set_xlabel('Number of iterations')
    ax.set_yscale('log')
    ax.set_yscale('symlog', linthresh = 1e-8)

    ax.axhline(1e-5, linestyle='--', color = 'k', linewidth = 0.5)
    ax.axhline(-1e-5, linestyle='--', color = 'k', linewidth = 0.5)
    
    ax.set_xticks(list(range(0,21, 5)))

axs[0].set_ylabel(r'$Relative~error~[-]$')

legend_elements = [mpl.lines.Line2D([0], [0], marker='x', linestyle = '-', color='k', label='$Potential$'), 
                   mpl.lines.Line2D([0], [0], marker='o', linestyle = '-', color='k', label='$Charge~density$')]

axs[-1].legend(handles=legend_elements)

ax_legend = axs[-1].twinx()
ax_legend.set_yticks([])

legend_elements = [mpl.lines.Line2D([0], [0], linestyle = '-', color='tab:blue', label=r'$center$'), 
                   mpl.lines.Line2D([0], [0], linestyle = '-', color='tab:orange', label=r'$gated$'),
                   mpl.lines.Line2D([0], [0], linestyle = '-', color='tab:green', label=r'$ungated$')]

ax_legend.legend(handles=legend_elements, loc='lower right')

labels_fig = 'a)', 'b)', 'c)'

for ax, label in zip(axs, labels_fig):
    # label physical distance to the left and up:
    trans = mpl.transforms.ScaledTranslation(-20/72, 7/72, fig.dpi_scale_trans)
    ax.text(0.0, 1.0, label, transform=ax.transAxes + trans,
            fontsize='medium', va='bottom', fontfamily='latin modern')
    
plt.tight_layout()
plt.show()
```

```python
fig.savefig('convergence.pdf')
```

```python

```
