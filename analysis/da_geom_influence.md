---
jupyter:
  jupytext:
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.13.8
  kernelspec:
    display_name: Python 3 (ipykernel)
    language: python
    name: python3
---

```python
import qpc_exp2sim.tools.data_handling as dh
import qpc_exp2sim.tools.plotting_sim as plotting_sim
import numpy as np
from __future__ import print_function
from ipywidgets import interact, interactive, fixed, interact_manual
import ipywidgets as widgets
import matplotlib.pyplot as plt
#%matplotlib notebook
```

## Influence L

```python
data = dh.read_file('system_3D/L/b3_L800')
V_gates_list = data.p_sim.V_gates_list
coord =data.coord

widget_vL = widgets.Dropdown(options=V_gates_list, description ='V_gates:')
widget_vecL = widgets.Dropdown(options=[('x (along transport)',0),('y (along gates)',1),('z (thickness)',2)], description='vec:')
widget_coordxL = widgets.Dropdown(options=np.unique(coord[:,0]), description='coord x:', value=0)
widget_coordyL = widgets.Dropdown(options=np.unique(coord[:,1]), description='coord y:', value=0)
widget_figsizeL = widgets.IntSlider(value=5, min=0, max=15, step=1, continuous_update=False, description='Figure size:')

widget_vW = widgets.Dropdown(options=V_gates_list, description ='V_gates:')
widget_vecW = widgets.Dropdown(options=[('x (along transport)',0),('y (along gates)',1),('z (thickness)',2)], description='vec:')
widget_coordxW = widgets.Dropdown(options=np.unique(coord[:,0]), description='coord x:', value=0)
widget_coordyW = widgets.Dropdown(options=np.unique(coord[:,1]), description='coord y:', value=0)
widget_figsizeW = widgets.IntSlider(value=5, min=0, max=15, step=1, continuous_update=False, description='Figure size:')

widget_vLW = widgets.Dropdown(options=V_gates_list, description ='V_gates:')
widget_vecLW = widgets.Dropdown(options=[('x (along transport)',0),('y (along gates)',1),('z (thickness)',2)], description='vec:')
widget_coordxLW = widgets.Dropdown(options=np.unique(coord[:,0]), description='coord x:', value=0)
widget_coordyLW = widgets.Dropdown(options=np.unique(coord[:,1]), description='coord y:', value=0)
widget_figsizeLW = widgets.IntSlider(value=5, min=0, max=15, step=1, continuous_update=False, description='Figure size:')

widget_vmap = widgets.Dropdown(options=V_gates_list, description ='V_gates:')
```

```python
files = ['system_3D/L/' + file for file in ['b3_L800', 'b3_L1000', 'b3_L1250', 'b3_L1500', 'b3_L2000']]
legend_label = ['L = ' + file[16:] for file in files]
    
interact(plotting_sim.compare_geom_abs, 
         V_gates=widget_vL, 
         vec=widget_vecL,
         coordx = widget_coordxL,
         coordy = widget_coordyL, 
         figsize = widget_figsizeL, 
         files = fixed(files), 
         legend_label = fixed(legend_label))
```

```python
files = ['system_3D/L/' + file for file in ['b3_L800', 'b3_L1000', 'b3_L1250', 'b3_L1500', 'b3_L2000']]
legend_label = ['L = ' + file[16:] for file in files]

interact(plotting_sim.compare_geom_rel, 
         V_gates=widget_vL, 
         vec=widget_vecL,
         coordx = widget_coordxL,
         coordy = widget_coordyL, 
         figsize = widget_figsizeL, 
         files = fixed(files), 
         legend_label = fixed(legend_label),
         ylimits = fixed([0.92,1.08]))
```

## Influence W

```python
files = ['system_3D/W/' + file for file in ['b3_W800', 'b3_W1000', 'b3_W1250', 'b3_W1500']]
legend_label = ['W = ' + file[16:] for file in files]

interact(plotting_sim.compare_geom_abs, 
         V_gates=widget_vW, 
         vec=widget_vecW,
         coordx = widget_coordxW,
         coordy = widget_coordyW, 
         figsize = widget_figsizeW, 
         files = fixed(files), 
         legend_label = fixed(legend_label))
```

```python
files = ['system_3D/W/' + file for file in ['b3_W800', 'b3_W1000', 'b3_W1250', 'b3_W1500']]
legend_label = ['W = ' + file[16:] for file in files]

interact(plotting_sim.compare_geom_rel, 
         V_gates=widget_vW, 
         vec=widget_vecW,
         coordx = widget_coordxW,
         coordy = widget_coordyW, 
         figsize = widget_figsizeW, 
         files = fixed(files), 
         legend_label = fixed(legend_label), 
         ylimits = fixed([0.99, 1.55]))
```

## Influence LW

```python
files = ['system_3D/LW/' + file for file in ['b3_LW800', 'b3_LW1000', 'b3_LW1250']]
legend_label = ['LW = ' + file[18:] for file in files]

interact(plotting_sim.compare_geom_abs, 
         V_gates=widget_vLW, 
         vec=widget_vecLW,
         coordx = widget_coordxLW,
         coordy = widget_coordyLW, 
         figsize = widget_figsizeLW, 
         files = fixed(files), 
         legend_label = fixed(legend_label))
```

```python
files = ['system_3D/LW/' + file for file in ['b3_LW800', 'b3_LW1000', 'b3_LW1250']]
legend_label = ['LW = ' + file[18:] for file in files]

interact(plotting_sim.compare_geom_rel, 
         V_gates=widget_vLW, 
         vec=widget_vecLW,
         coordx = widget_coordxLW,
         coordy = widget_coordyLW, 
         figsize = widget_figsizeLW, 
         files = fixed(files), 
         legend_label = fixed(legend_label),
         ylimits = fixed(None))
```

```python
# For L
from qpc_exp2sim.simulation import builder

#files = ['system_3D/L/' + file for file in ['b3_L800', 'b3_L1000', 'b3_L1250', 'b3_L1500', 'b3_L2000']]
#widget_fileL = widgets.Dropdown(options=[('L = ' + file[16:], file) for file in files[:-1]], description ='file:')

files = ['system_3D/data_files/b3_grid10', 'system_3D/grid/b3_grid10']
widget_fileL = widgets.Dropdown(options=files, description ='file:')
widget_vmap = widgets.Dropdown(options=dh.read_file('system_3D/grid/b3_grid10').p_sim.V_gates_list, description ='V_gates:')

def f(V_gates, file, file_ref):
    # Reading the data of the reference file
    data_ref = dh.read_file(file_ref)
    coord_ref = data_ref.coord
    V_gates_list_ref = data_ref.p_sim.V_gates_list
    index_ref = np.where(V_gates_list_ref == V_gates)[0][0]
    voltage_ref = data_ref.voltage[index_ref]
    charge_ref = data_ref.charge[index_ref]
    
    # Reading the data of the other file
    data = dh.read_file(file)
    p_geom = data.p_geom
    coord = data.coord
    V_gates_list = data.p_sim.V_gates_list
    index = np.where(V_gates_list == V_gates)[0][0]
    voltage = data.voltage[index]
    charge = data.charge[index]
    
    # Making a cross section of the data
    section = [None, None, 0]
    coord_2D_ref, voltage_2D_ref = plotting_sim.data_cross_section(coord_ref, voltage_ref, section=section)
    _, charge_2D_ref = plotting_sim.data_cross_section(coord_ref, charge_ref, section=section)
    coord_2D, voltage_2D = plotting_sim.data_cross_section(coord, voltage, section=section)
    _, charge_2D = plotting_sim.data_cross_section(coord, charge, section=section)
    
    # Finding which coordinates of the reference file fall within the shape of the other file
    # Create the shape of the 2DEG again
    shape = builder.DEG_shape(data.p_geom, section = section)
    
    sort_ind_2D = np.lexsort((coord_2D[:,0], coord_2D[:,1]))
    coord_2D = coord_2D[sort_ind_2D]
    voltage_2D = voltage_2D[sort_ind_2D]
    charge_2D = charge_2D[sort_ind_2D]
    
    sort_ind_2D_ref = np.lexsort((coord_2D_ref[:,0], coord_2D_ref[:,1]))
    coord_2D_ref = coord_2D_ref[sort_ind_2D_ref]
    voltage_2D_ref = voltage_2D_ref[sort_ind_2D_ref]
    charge_2D_ref = charge_2D_ref[sort_ind_2D_ref]
        
    voltage_2D_ref = voltage_2D_ref[shape(coord_2D_ref)]
    charge_2D_ref = charge_2D_ref[shape(coord_2D_ref)]
        
    # Getting the correct labels
    labels = ['x(nm)', 'y(nm)']
        
    # Plotting the data
    voltage = voltage_2D/voltage_2D_ref-1
    
    index_depleted = np.all([charge_2D == 0, charge_2D_ref == 0], axis=0)
    
    # Where the reference is depleted but the other not
    index_d_ref = np.all([charge_2D != 0, charge_2D_ref == 0], axis=0)
    
    # Where the other is depleted but the reference not
    index_d_file = np.all([charge_2D == 0, charge_2D_ref != 0], axis =0)
    
    index_singularities = np.any([index_depleted, index_d_ref, index_d_file], axis=0)
    
    # Where we have singularities or a perfect match we set the value to zero and we handle the singularities later
    charge = np.where(index_singularities, 0, charge_2D/charge_2D_ref-1)
    
    print('There are {} singularities detected in the density colormap.'\
          .format(int(sum(index_d_file) + sum(index_d_ref))))
    
    print('Voltage std: ', np.average(voltage))
    print('Voltage min, max: ', np.min(voltage), np.max(voltage))

    print('Charge std: ', np.average(charge[np.logical_not(index_singularities)]))
    print('Charge min, max: ', np.min(charge), np.max(charge))


    plotting_sim.plot_colormap(coord_2D, voltage, p_geom, setting=3, aspect_equal=True, labels=labels)    
    plotting_sim.plot_colormap(coord_2D, charge, p_geom, setting=4, aspect_equal=True, labels=labels, show = False)
    plt.scatter(coord_2D[index_d_file[:,0,0],0], coord_2D[index_d_file[:,0,0],1], color='k', marker='x', s=30)
    plt.scatter(coord_2D[index_d_ref[:,0,0],0], coord_2D[index_d_ref[:,0,0],1], color='k', marker='o', s=30)
    plt.axvline(100, color = 'k', linestyle = '--', linewidth = 1)
    plt.show()
    
interact(f, V_gates=widget_vmap, file=widget_fileL, file_ref=fixed(files[-1]))

# Calculate average std error
```

```python
# For W
from qpc_exp2sim.simulation import builder
files = ['system_3D/W/' + file for file in ['b3_W800', 'b3_W1000', 'b3_W1250', 'b3_W1500']]

widget_fileW = widgets.Dropdown(options=[('W = ' + file[16:], file) for file in files[:-1]], description ='file:')
interact(f, V_gates=widget_vmap, file=widget_fileW, file_ref=fixed(files[-1]))

```

```python
def compare_conductances(files, legend_label, ylimits = None):
    data = dh.read_file(files[-1])
    V_gates_list_ref = data.p_sim.V_gates_list
    conductance_ref = data.conductance

    V_gates_list = []
    conductance_abs = []
    conductance_rel = []
    for file in files[:-1]: 
        data = dh.read_file(file)
        V_gates_list.append(data.p_sim.V_gates_list)
        conductance_rel.append(np.array(data.conductance)/np.array(conductance_ref))
        conductance_abs.append(np.array(data.conductance) - np.array(conductance_ref))

    plt.figure()
    plt.plot(np.transpose(V_gates_list), np.transpose(conductance_rel), '.--')
    plt.legend(legend_label)
    plt.xlabel(r"$V_g [V]$")
    plt.ylabel("conductance / conductance_reference")
    plt.ylim(ylimits)
    plt.show()

    plt.figure()
    plt.plot(np.transpose(V_gates_list), np.transpose(conductance_abs), '.--')
    plt.legend(legend_label)
    plt.xlabel(r"$V_g [V]$")
    plt.ylabel("conductance - conductance_reference")
    plt.ylim()
    plt.show()
```

```python
files = ['system_3D/L/' + file for file in ['b3_L800', 'b3_L1000', 'b3_L1250', 'b3_L1500', 'b3_L2000']]
#files.append('system_3D/fine/b3_grid10_fine')
legend_label = ['L = ' + file[16:] for file in files]

compare_conductances(files, legend_label, [0.95,1.02])
```

```python
dh.read_file('system_3D/W/b3_W2000').conductance
```

```python
files = ['system_3D/W/' + file for file in ['b3_W800', 'b3_W1000', 'b3_W1250', 'b3_W1500']]
legend_label = ['W = ' + file[16:] for file in files]
    
compare_conductances(files, legend_label,[0.85,1.01])
```

```python
files = ['system_3D/LW/' + file for file in ['b3_LW800', 'b3_LW1000', 'b3_LW1250']]
legend_label = ['LW = ' + file[18:] for file in files]

compare_conductances(files, legend_label, [0.84, 1.01] )
```

```python

```
