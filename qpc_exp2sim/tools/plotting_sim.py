#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar  1 16:07:29 2022

@author: thellemans
"""
import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
try:
    import mpl_axes_aligner
except:
    pass    
from ..simulation import builder
from ..pescado_kwant import plotting_pes
from . import data_handling
import warnings 
from types import SimpleNamespace
from scipy.signal import argrelextrema
from matplotlib.patches import Arc
from matplotlib.transforms import IdentityTransform, TransformedBbox, Bbox
        

def plot_overview_points(p_geom, points, ax=None, margin=30, color=None):
    """
    check if the shapes can be pickled now that the definition of General.shape is adapted, 
    if so:
        1) adapt the definition of the rpp_problem to store the shapes
        2) adapt the definition make_pescado_system
        3) load the gate shape here from the data_file
    """
    if isinstance(p_geom, dict):    
        gates = builder.gates_shape(**p_geom)
        p_geom = SimpleNamespace(**p_geom)
    else:
        gates = builder.gates_shape(**p_geom.__dict__)


    plotting_pes.shape(shapes_list=[gates], 
                       colors=['lightgray'],  
                       grid_cell = [5, 5, 40], 
                       section = [None, None, 120], 
                       ax=ax, 
                       imshow=True)
    
    ax.scatter(*points.transpose(), color=color)
    ax.set_xlim((-p_geom.L_kwant/2-margin, p_geom.L_kwant/2+margin))
    ax.set_ylim((-p_geom.W_kwant/2-margin, p_geom.W_kwant/2+margin))
    ax.plot([-p_geom.L_kwant/2, -p_geom.L_kwant/2, p_geom.L_kwant/2, p_geom.L_kwant/2, -p_geom.L_kwant/2], 
             [-p_geom.W_kwant/2, p_geom.W_kwant/2, p_geom.W_kwant/2, -p_geom.W_kwant/2, -p_geom.W_kwant/2],
            color = 'k', linestyle = '--', linewidth = 0.5)
    ax.set_xticks([-p_geom.L_kwant/2, -p_geom.L_kwant/4, 0 , p_geom.L_kwant/4, p_geom.L_kwant/2])
    ax.set_yticks([-p_geom.W_kwant/2, -p_geom.W_kwant/4, 0 , p_geom.W_kwant/4, p_geom.W_kwant/2])
    ax.set_xlabel('x [nm]')
    ax.set_ylabel('y [nm]', labelpad=-13)


def crossings(V_gates_list, conductance):
    '''
    Calculate the linear fit with the conductance:
        horizontal plateaus
        slope of step equal to max slope of the conductance for the specific step
        
        
    Returns
    -------
    
    slope : np.ndarray
        derivative of the conductance
    
    cross_points : 
        cross points are the points at the edges of the conductance plateau
        when doing a linear fit
        column 1 : cross point voltages
        column 2 : cross_point conductances
    
    infliction points : np.ndarray
        infliction points are the points with maximal derivative in the 
        conductance
        column 1 : infliction point voltages
        column 2 : infliction point conductances
        column 3 : local slope at the infliction point
    '''
    cross_points = []
    
    # Calculation of the derivative of the conductance
    slope = np.gradient(conductance, V_gates_list[1] - V_gates_list[0])
    
    # Calculation infliction points (max derivative)
    index_vlines = argrelextrema(slope, np.greater)
    infliction_points = [[V_gates_list[index_vline], conductance[index_vline], 
                          slope[index_vline]] for index_vline in index_vlines[0]]
    
    # Calculation plateau steps (min derivative)
    index_hlines = argrelextrema(slope, np.less)
    hlines = np.array(conductance)[index_hlines]
    hlines = np.insert(hlines, 0, 0)
    hlines = np.append(hlines, np.max(hlines)+1)
    
    for nb_step, index_v in enumerate(index_vlines[0]):
        x1 = V_gates_list[index_v]
        y1 = conductance[index_v]
        m = slope[index_v]
        
        cross_points.append([(hlines[nb_step] - y1)/m + x1, hlines[nb_step]])
        cross_points.append([(hlines[nb_step+1] - y1)/m + x1, hlines[nb_step+1]])
    
    return slope, np.array(cross_points), np.array(infliction_points)

    
def linear_fit_conductance(V_gates_list, conductance, show_slope = False):
    slope, cross_points, infliction_points = crossings(V_gates_list, conductance)

    fig,ax1 = plt.subplots()
    # Create two separate axes, one for the derivative and one for the conductance steps
    if show_slope:
        ax2=ax1.twinx()
        ax2.plot(V_gates_list, slope, '.-', color='tab:orange')

    ax1.plot(V_gates_list, conductance, '.-', color='tab:blue')

    for x in infliction_points[:,0]:
        plt.axvline(x, color = 'k', linestyle = '--', linewidth =0.5)

    for x in np.unique(cross_points[:,1]):
        ax1.axhline(x, color = 'k', linestyle = '--', linewidth =0.5)  

    ax1.plot(cross_points[:,0], cross_points[:,1], color = 'tab:red', linestyle = '-', linewidth =2)

    if show_slope:
        mpl_axes_aligner.align.yaxes(ax1, 0, ax2, 0)
        ax2.set_ylabel('slope [Vh/2e²]')
        
    ax1.set_xlabel("V_g [V]")
    ax1.set_ylabel("conductance [2e²/h]")
    plt.show()   
    
    
def plot_conductance(V_gates_list, conductance, V3=0, energy_list=[0], label = None, 
                     linestyle = '.--', limits_v=None, hlines=None, ax = None):

    # Plotting the conductance in function of V_g
    #for index in range(len(energy_list)):
    if ax is None:
        fig, ax = plt.subplots()
    
    plt.xlabel(r"$V_g$ [V]")
    plt.ylabel(r"conductance [$2e^2/h$]")
    
    y_min, y_max = 0, -float('inf')
    

    
    if isinstance(V_gates_list[0], (int, float)):
        V_gates_list = [V_gates_list]
    if isinstance(conductance[0], (int, float)):
        conductance = [conductance]
    
    for i, V_gates in enumerate(V_gates_list):
        V_gates -= V3
        plt.plot(V_gates, conductance[i], linestyle, label=label[i] \
                 if label is not None else None)
        
        if limits_v is not None:
            # For setting the limits in the y-direction
            indices = np.where(np.logical_and(limits_v[0] <= V_gates, V_gates <= limits_v[1]))
            conductance_values = np.array(conductance[i])[indices]
            y_min = min(np.min(conductance_values), y_min)
            y_max = max(np.max(conductance_values), y_max)
    
    if limits_v is not None:
        plt.xlim(limits_v)
        plt.ylim((y_min, y_max))
    
    if label is not None:
        if len(label) <= 10:
            loc = 'best'
            bbox = None
        else:
            loc = 'center right'
            bbox = (1.35, 0.5)

        plt.gca().legend(loc=loc, bbox_to_anchor=bbox)

    if isinstance(hlines, (list, np.ndarray)):
        for line in hlines:
            plt.axhline(line, color = 'k', linestyle = '--', linewidth =0.5)
    
    # Plotting the conductance in function of energy
#    if len(energy_list) > 1:
#        plt.figure()
#        plt.plot(energy_list, np.transpose(conductance))
#        plt.xlabel("energy [t]")
#        plt.ylabel("conductance [e^2/h]")
#        plt.gca().legend(["V_gates = {}".format(round(v, 2)) for v in V_gates_list], 
#                loc='center right', bbox_to_anchor=(1.35, 0.5))
#        if show:
#            plt.show()


###############################################################################
    
#           Until here the functions are usefull                              #
    
###############################################################################
    
    
def _label_from_section(dimension, coord_section):
    if coord_section is None:
        coord_section = [[None]*dimension]
    elif isinstance(coord_section[0], (int, float)) or coord_section[0] is None:
        coord_section = [coord_section]
    
    labels = ['x(nm)', 'y(nm)', 'z(nm)']
    label = [labels[i] for i, val in enumerate(coord_section[0]) if val is not None]
    
    return label


def _label_data(grid, setting_data):
    """
    setting_y: 
        input value to choose the right y axis label
            0 for voltage
            1 for density (charge/cell)
            2 for density (/m²)
    setting_x: 
        input value to choose the right x axis label
            0,1,2 for x,y,z
            3 for Vg -> not implemented at this point
    legend: 
        list containing the legend 
    """ 
    assert setting_data in range(5), \
     r"Setting has to be an int: \n\
     * 0 (voltage V) \n\
     * 1 (charge per cell) \n\
     * 2 (density [/$m^2$]) \n\
     * 3 (relative voltage [-]) \n\
     * 4 (relative density [-])"  + \
     "Option 3 and 4 not implemented anymore (at the moment)"
     
    if setting_data == 2:
        # np.product(np.array(p.grid_fine[:-1])) for conversion from 
        # charge/cell to charge/nm²
        # 1e18 for charge/nm² to charge/m²
        cell_area = np.product(np.array(grid[:-1])) * 1e18
    else:
        cell_area = None
    
    data_label = ['Voltage [V]', 'Charge per cell', r'$n [/m^2]$', 
                  r'$V/V_{ref}$ [-]', r'$n/n_{ref}$ [-]'][setting_data]

    # Settings for the colorbar
    cmap = [plt.cm.viridis_r, plt.cm.plasma, plt.cm.plasma, 
            plt.cm.bwr, plt.cm.bwr][setting_data]
    
    return cell_area, data_label, cmap
    

def _vline(p_geom, coord_section):
    """
    For a complete and correct implementation this should be calculated
    from the qpc_geometry
    """
    if coord_section is None:
        coord_section = [[None]*len(p_geom['grid_fine'])]
    elif isinstance(coord_section[0], (int, float)) or coord_section[0] is None:
        coord_section = [coord_section]
    
    setting_x = [i for i, val in enumerate(coord_section[0]) if val is not None][0]
    
    if setting_x == 0:
        vertical_lines = []#[- p.L_narrow_gate/2, p.L_narrow_gate/2]
    elif setting_x == 1:
        vertical_lines = []# [-(p.W - p.W_narrow_gate)/2, (p.W - p.W_narrow_gate)/2]
    else:
        p = SimpleNamespace(**p_geom)
        vertical_lines = [-p.d_2DEG/2, 
                          p.d_2DEG/2, 
                          p.d_2DEG/2 + p.d1, 
                          p.d_2DEG/2 + p.d1 + p.d2, 
                          p.d_2DEG/2 + p.d1 + p.d2 + p.d3,
                          p.d_2DEG/2 + p.d1 + p.d2 + p.d3 + p.d4]
    return vertical_lines
        

def _setting_colormap(setting_cbar):
    
     
    # Settings for the colorbar
    cmap = [plt.cm.viridis_r, plt.cm.plasma, plt.cm.plasma, 
            plt.cm.bwr, plt.cm.bwr][setting_cbar]
    
    cbar_label = ['Voltage (V)', 'Charge per cell', r'$n [/m^2]$', \
                  r'$V/V_{ref}$ [-]', r'$n/n_{ref}$ [-]'][setting_cbar]
    
    return cbar_label, cmap


def plot_pescado(data, setting_data, coord_section=None, 
                 value_section=None, **kwargs):
    """
    This function plots the voltage ifo a coordinate, i.e. a colormap or line depending on
    the section
    
    value_section: int, float or list of int/float
    
    coord_section: list, same shape as coord[0] or list of lists
        None values should be in the same location!!
    
    """
    # Obtaining the data
    p_sim = data.p_sim
    value = [data.voltage, data.charge, data.charge][setting_data]
    coord = data.coord
    
    if value_section is None:
        index_v = 0
    else:
        index_v = np.where(p_sim['V_gates_list'] == value_section)[0][0]
    
    coord_fil, value_fil = plotting_pes.data_cross_section(coord, value[index_v],
                                                           coord_section)
    
    label = _label_from_section(len(data.p_geom['grid_fine']), coord_section)
    cell_area, data_label, cmap = _label_data(data.p_geom['grid_fine'], setting_data)
    
    # Make lineplot
    if coord_fil.shape[1] == 1:
        vline = _vline(data.p_geom, coord_section)
        plotting_pes.line(coord_fil, value_fil, cell_area=cell_area, 
                          xlabel=label[0], ylabel=data_label, vline=vline, **kwargs)
    
    # Make colormap
    elif coord_fil.shape[1] == 2:
        # determine x and y axis label based on the coord_section
        plotting_pes.colormap(coord_fil, value_fil, cell_area=cell_area, 
                              xlabel=label[0], ylabel=label[1],
                              cmap=cmap, cbar_label=data_label, **kwargs)
    else:
        raise Exception("Please enter a ''coord_section' that reduces the number of " +\
                         "dependent variables to 1 or 2.")
    
    
def conductance(data, V3=None, label = None, linestyle = '.--', limits_v=None, hlines=None, 
                ax=None, energy_section = None, voltage_section = None, lineplot='', 
                figsize=[6.4, 4.8]):
    
    """
    Data should be able to be a list of data objects
    
    energy section and voltage section should be values out of V_gates_list or energy_list
    If both are None or a list with lenth longer than 1, by default a colormap is plotted
    Plotting a line plot with multiple conductance lines is possible by setting lineplot
    to 'energy' to have lines ifo energy or 'voltage' to have lines ifo voltage.
    
    make a function:
        _conductance_line()
        _conductance_map()
        
    They should each accept a single data object
    """
    
    # Obtaining the data
    p_sim, result = data.results[data.selected_result]
    V_gates_list = p_sim['V_gates_list']
    energy_list = p_sim['energy_list']
    conductance = result.conductance
    if V3 == 'density':
        V3 = result.V3[0][0]
    elif V3 == 'conductance':
        V3 = result.V3[1]
    else:
        V3 = 0
    
    # Plotting the conductance in function of V_g
    # for index in range(len(energy_list)):
    if ax is None:
        fig, ax = plt.subplots(figsize=figsize)
    
    plt.xlabel("V_g [V]")
    plt.ylabel("conductance [2e^2/h]")
    
    y_min, y_max = 0, -float('inf')
    
    if isinstance(V_gates_list[0], (int, float)):
        V_gates_list = [V_gates_list]
    if isinstance(conductance[0], (int, float)):
        conductance = [conductance]
    
    for i, V_gates in enumerate(V_gates_list):
        V_gates -= V3
        plt.plot(V_gates, conductance[i], linestyle, label=label[i] \
                 if label is not None else None)
        
        if limits_v is not None:
            # For setting the limits in the y-direction
            indices = np.where(np.logical_and(limits_v[0] <= V_gates, V_gates <= limits_v[1]))
            conductance_values = np.array(conductance[i])[indices]
            y_min = min(np.min(conductance_values), y_min)
            y_max = max(np.max(conductance_values), y_max)
    
    if limits_v is not None:
        plt.xlim(limits_v)
        plt.ylim((y_min, y_max))
    
    if label is not None:
        if len(label) <= 10:
            loc = 'best'
            bbox = None
        else:
            loc = 'center right'
            bbox = (1.35, 0.5)

        plt.gca().legend(loc=loc, bbox_to_anchor=bbox)

    if isinstance(hlines, (list, np.ndarray)):
        for line in hlines:
            plt.axhline(line, color = 'k', linestyle = '--', linewidth =0.5)
    

    
    # Plotting the conductance in function of energy
#    if len(energy_list) > 1:
#        plt.figure()
#        plt.plot(energy_list, np.transpose(conductance))
#        plt.xlabel("energy [t]")
#        plt.ylabel("conductance [e^2/h]")
#        plt.gca().legend(["V_gates = {}".format(round(v, 2)) for v in V_gates_list], 
#                loc='center right', bbox_to_anchor=(1.35, 0.5))
#        if show:
#            plt.show()





def _conductance_map(conductance, V_gates_list, energy_list, nb_steps=7, filtered = False, derivative=False, ylabel=None):
    fig, ax = plt.subplots()
    ax.set_xlabel(r'$V_g [V]$')
    if ylabel is None:
        ax.set_ylabel('energy [t]')
    else:
        ax.set_ylabel(ylabel)
    label = r'Conductance $2e^2/h$'

    cmap = plt.get_cmap('hot')
    X,Y = plotting_pes.cmap_mesh(V_gates_list, energy_list)

    if filtered:
        if derivative:
            warnings.warn('Plotting derivative only possible for "filtered=False", normal conductance plotted')
        levels = np.insert(np.arange(0.5, nb_steps+0.5, 1), 0, 0)
        norm = mpl.colors.BoundaryNorm(levels, ncolors=cmap.N, clip=True)
        ax.pcolormesh(X,Y, conductance.transpose(), cmap=cmap, norm=norm)
        cf = ax.contourf(V_gates_list, energy_list, conductance.transpose(), levels=levels, cmap=cmap)
        fig.colorbar(cf, ax=ax, ticks=np.arange(nb_steps), label = label)
    else:
        if derivative:
            conductance = np.gradient(conductance, V_gates_list[1] - V_gates_list[0], axis=0)
            label = r'$dG/dV_g$'
        norm = mpl.colors.Normalize(vmin=0, vmax=np.nanmax(conductance))
        scalarmappable = plt.cm.ScalarMappable(norm=norm, cmap=cmap)
        fig.colorbar(scalarmappable, label = label)
        ax.pcolormesh(X,Y, conductance.transpose(), cmap=cmap, norm=norm)


def plot_V3(files, figsize=(10,7)):
    from ..simulation.QPC_geometry import qpc_data
    
    if not isinstance(files[0], list):
        files = [files]
    
    fig, axs = plt.subplots(len(files), figsize=figsize)
    
    for i, files_elem in enumerate(files):
        # Read the data
        
        if len(files) == 1:
            axs = [axs]
        
        V3_exp = []
        V3_exp_std = []
        V3_sim_dens = []
        V3_sim_cond = []
        V3_sim_el = []
        x = []
    
        for file in files_elem:
            data = data_handling.read_file(file)
            V3_sim_dens.append(data.V3[0][0])
            V3_sim_cond.append(data.V3[1])
            qpc = qpc_data[data.p_geom.qpc_name]
            x_param = {'a':'L', 'b':'R', 'c':'L'}[data.p_geom.qpc_name[0]]
            x.append(qpc[x_param])
            V3_sim_el.append(np.average(qpc['V3_sim_el']))
            V3_exp.append(np.average(qpc['V3_exp']))
            V3_exp_std.append(np.std(qpc['V3_exp']))
    
        # Plot the data
            
        hlines = [-2.5, -2.25, -2,-1.75, -1.5, -1.25, -1]
        for line in hlines:
            axs[i].axhline(line, color = 'k', linestyle = '--', linewidth =0.3)
    
    
        axs[i].errorbar(x[0], V3_exp[0], yerr=V3_exp_std[0], color='tab:blue', label='exp: W = 250 nm')
        axs[i].errorbar(x[1:], V3_exp[1:], yerr=V3_exp_std[1:],  color='tab:orange', label='exp: W = 300 nm')
    
        axs[i].plot(x[0], V3_sim_dens[0], marker='o', markerfacecolor='none', color='k', label="sim: dens")
        axs[i].plot(x[1:], V3_sim_dens[1:], marker='o', markerfacecolor='none', color='k', linestyle='--')
    
        axs[i].plot(x[0], V3_sim_cond[0], marker='d', markerfacecolor='none', color='k', label="sim: cond")
        axs[i].plot(x[1:], V3_sim_cond[1:], marker='d', markerfacecolor='none', color='k', linestyle='--')
        
        axs[i].plot(x[0], V3_sim_el[0], marker='x', markerfacecolor='none', color='r', label="sim: eleni")
        axs[i].plot(x[1:], V3_sim_el[1:], marker='x', markerfacecolor='none', color='r', linestyle='--')
    
        axs[i].legend(loc='lower right')
        xlabel = {'L':'Length L [nm]', 'R': 'Radius R [nm]'}[x_param]
        axs[i].set_xlabel(xlabel)
        axs[i].set_ylabel(r'$V_3 [V]$')
        axs[i].set_title("Pinch off voltage V3 for QPC shape '{}'".format(data.p_geom.qpc_name[0]))
        axs[i].set_xscale('log')
    plt.subplots_adjust(hspace=0.5)
    plt.show
    
    
def compare_geom_abs(V_gates, vec, coordx, coordy, figsize, files, legend_label):
    """
    The plotting_pes.line() input arguments are not yet correct
    """
    # Getting the sparse vectors containing the data for a certain gate voltage
    
    section = [coordx,coordy,0]
    section[vec] = None
    
    fig, ax = plt.subplots(figsize = [figsize]*2)
    for file in files:
        data = data_handling.read_file(file)
        p_geom = data.p_geom
        coord = data.coord
        V_gates_list = data.p_sim.V_gates_list
        index = np.where(V_gates_list == V_gates)[0][0]
        voltage_sparse_vec = data.voltage[index]
        
        # Making a cross section of the data
        coord_1D, voltage_1D = plotting_pes.data_cross_section(coord, voltage_sparse_vec, 
                                                               section)

        # Sort the data such that we don't get crossing lines through the figure
        sort_indices = np.argsort(coord_1D[:,0])
        coord_1D = coord_1D[:,0][sort_indices]
        voltage_1D = voltage_1D[:,:,0][sort_indices]

        # Plotting the data
        plotting_pes.line(coord_1D, voltage_1D, p_geom, setting_x=vec, setting_y=0, figsize=figsize, ax=ax)
    ax.legend(legend_label)
    plt.show()
  
    fig, ax = plt.subplots(figsize = [figsize]*2)
    for file in files:
        data = data_handling.read_file(file)
        p_geom = data.p_geom
        coord = data.coord
        V_gates_list = data.p_sim.V_gates_list
        index = np.where(V_gates_list == V_gates)[0][0]
        charge_sparse_vec = data.charge[index] 
        
        # Making a cross section of the data
        coord_1D, charge_1D = plotting_pes.data_cross_section(coord, charge_sparse_vec, 
                                                              section)

        # Sort the data such that we don't get crossing lines through the figure
        sort_indices = np.argsort(coord_1D[:,0])
        coord_1D = coord_1D[:,0][sort_indices]
        charge_1D = charge_1D[:,:,0][sort_indices]

        # Plotting the data
        plotting_pes.line(coord_1D, charge_1D, p_geom, setting_x=vec, setting_y=2, figsize=figsize, ax=ax)
    ax.legend(legend_label)
    plt.show()


def compare_geom_rel(V_gates, vec, coordx, coordy, figsize, files, legend_label, ylimits = None):
    """
    The plotting_pes.line() input arguments are not yet correct
    """
    # Getting the sparse vectors containing the data for a certain gate voltage
       
    section = [[coordx,coordy,0]]
    section[0][vec] = None
    
    data_ref = data_handling.read_file(files[-1])
    coord = data_ref.coord
    V_gates_list_ref = data_ref.p_sim.V_gates_list
    index_ref = np.where(V_gates_list_ref == V_gates)[0][0]
    voltage_ref = data_ref.voltage[index_ref]
    charge_ref = data_ref.charge[index_ref]

    # Making a cross section of the data
    coord_ref, voltage_ref = plotting_pes.data_cross_section(coord, voltage_ref, section)
    coord_ref, charge_ref = plotting_pes.data_cross_section(coord, charge_ref, section)

    # Sort the data such that we don't get crossing lines through the figure
    sort_indices = np.argsort(coord_ref[:,0])
    coord_ref = coord_ref[:,0][sort_indices]
    voltage_ref = voltage_ref[:,0,0][sort_indices] 
    charge_ref = charge_ref[:,0,0][sort_indices]    
    
    fig, ax = plt.subplots(figsize = [figsize]*2)
    for file in files[:-1]:
        data = data_handling.read_file(file)
        p_geom = data.p_geom
        coord = data.coord
        V_gates_list = data.p_sim.V_gates_list
        index = np.where(V_gates_list == V_gates)[0][0]
        voltage_sparse_vec = data.voltage[index]
        
        # Making a cross section of the data
        coord_1D, voltage_1D = plotting_pes.data_cross_section(coord, voltage_sparse_vec, section)

        # Sort the data such that we don't get crossing lines through the figure
        sort_indices = np.argsort(coord_1D[:,0])
        coord_1D = coord_1D[:,0][sort_indices]
        voltage_1D = voltage_1D[:,0,0][sort_indices]
        
        slice_index = int((len(coord_ref) - len(coord_1D))/2)
        if slice_index > 0:
            voltage_1D = voltage_1D / voltage_ref[slice_index : -slice_index]
        else:
            voltage_1D = voltage_1D / voltage_ref

        # Plotting the data
        plotting_pes.line(coord_1D, voltage_1D, p_geom, setting_x=vec, setting_y=3, figsize=figsize, ax=ax)

    ax.legend(legend_label)
    ax.set_ylim(ylimits)
    plt.show()
  
    fig, ax = plt.subplots(figsize = [figsize]*2)
    for file in files[:-1]:
        data = data_handling.read_file(file)
        p_geom = data.p_geom
        coord = data.coord
        V_gates_list = data.p_sim.V_gates_list
        index = np.where(V_gates_list == V_gates)[0][0]
        charge_sparse_vec = data.charge[index] 
        
        # Making a cross section of the data
        coord_1D, charge_1D = plotting_pes.data_cross_section(coord, charge_sparse_vec, 
                                                              section)

        # Sort the data such that we don't get crossing lines through the figure
        sort_indices = np.argsort(coord_1D[:,0])
        coord_1D = coord_1D[:,0][sort_indices]
        charge_1D = charge_1D[:,0,0][sort_indices]

        slice_index = int((len(coord_ref) - len(coord_1D))/2)
        if slice_index > 0:
            charge_1D = charge_1D / charge_ref[slice_index : -slice_index]
        else:
            charge_1D = charge_1D / charge_ref
                
        # Plotting the data
        plotting_pes.line(coord_1D, charge_1D, p_geom, setting_x=vec, setting_y=4, figsize=figsize, ax=ax)
    
    ax.legend(legend_label)
    ax.set_ylim(ylimits)
    plt.show()
    
class Data(data_handling.Data):
    """
    This is a copy of the data class to be compatible with some old pickles
    """


class AngleAnnotation(Arc):
    """
    Draws an arc between two vectors which appears circular in display space.
    Copied from: 
        https://matplotlib.org/stable/gallery/text_labels_and_annotations/
        angle_annotation.html
    """
    def __init__(self, xy, p1, p2, size=75, unit="points", ax=None,
                 text="", textposition="inside", text_kw=None, **kwargs):
        """
        Parameters
        ----------
        xy, p1, p2 : tuple or array of two floats
            Center position and two points. Angle annotation is drawn between
            the two vectors connecting *p1* and *p2* with *xy*, respectively.
            Units are data coordinates.

        size : float
            Diameter of the angle annotation in units specified by *unit*.

        unit : str
            One of the following strings to specify the unit of *size*:

            * "pixels": pixels
            * "points": points, use points instead of pixels to not have a
              dependence on the DPI
            * "axes width", "axes height": relative units of Axes width, height
            * "axes min", "axes max": minimum or maximum of relative Axes
              width, height

        ax : `matplotlib.axes.Axes`
            The Axes to add the angle annotation to.

        text : str
            The text to mark the angle with.

        textposition : {"inside", "outside", "edge"}
            Whether to show the text in- or outside the arc. "edge" can be used
            for custom positions anchored at the arc's edge.

        text_kw : dict
            Dictionary of arguments passed to the Annotation.

        **kwargs
            Further parameters are passed to `matplotlib.patches.Arc`. Use this
            to specify, color, linewidth etc. of the arc.

        """
        self.ax = ax or plt.gca()
        self._xydata = xy  # in data coordinates
        self.vec1 = p1
        self.vec2 = p2
        self.size = size
        self.unit = unit
        self.textposition = textposition

        super().__init__(self._xydata, size, size, angle=0.0,
                         theta1=self.theta1, theta2=self.theta2, **kwargs)

        self.set_transform(IdentityTransform())
        self.ax.add_patch(self)

        self.kw = dict(ha="center", va="center",
                       xycoords=IdentityTransform(),
                       xytext=(0, 0), textcoords="offset points",
                       annotation_clip=True)
        self.kw.update(text_kw or {})
        self.text = ax.annotate(text, xy=self._center, **self.kw)

    def get_size(self):
        factor = 1.
        if self.unit == "points":
            factor = self.ax.figure.dpi / 72.
        elif self.unit[:4] == "axes":
            b = TransformedBbox(Bbox.from_bounds(0, 0, 1, 1),
                                self.ax.transAxes)
            dic = {"max": max(b.width, b.height),
                   "min": min(b.width, b.height),
                   "width": b.width, "height": b.height}
            factor = dic[self.unit[5:]]
        return self.size * factor

    def set_size(self, size):
        self.size = size

    def get_center_in_pixels(self):
        """return center in pixels"""
        return self.ax.transData.transform(self._xydata)

    def set_center(self, xy):
        """set center in data coordinates"""
        self._xydata = xy

    def get_theta(self, vec):
        vec_in_pixels = self.ax.transData.transform(vec) - self._center
        return np.rad2deg(np.arctan2(vec_in_pixels[1], vec_in_pixels[0]))

    def get_theta1(self):
        return self.get_theta(self.vec1)

    def get_theta2(self):
        return self.get_theta(self.vec2)

    def set_theta(self, angle):
        pass

    # Redefine attributes of the Arc to always give values in pixel space
    _center = property(get_center_in_pixels, set_center)
    theta1 = property(get_theta1, set_theta)
    theta2 = property(get_theta2, set_theta)
    width = property(get_size, set_size)
    height = property(get_size, set_size)

    # The following two methods are needed to update the text position.
    def draw(self, renderer):
        self.update_text()
        super().draw(renderer)

    def update_text(self):
        c = self._center
        s = self.get_size()
        angle_span = (self.theta2 - self.theta1) % 360
        angle = np.deg2rad(self.theta1 + angle_span / 2)
        r = s / 2
        if self.textposition == "inside":
            r = s / np.interp(angle_span, [60, 90, 135, 180],
                                          [3.3, 3.5, 3.8, 4])
        self.text.xy = c + r * np.array([np.cos(angle), np.sin(angle)])
        if self.textposition == "outside":
            def R90(a, r, w, h):
                if a < np.arctan(h/2/(r+w/2)):
                    return np.sqrt((r+w/2)**2 + (np.tan(a)*(r+w/2))**2)
                else:
                    c = np.sqrt((w/2)**2+(h/2)**2)
                    T = np.arcsin(c * np.cos(np.pi/2 - a + np.arcsin(h/2/c))/r)
                    xy = r * np.array([np.cos(a + T), np.sin(a + T)])
                    xy += np.array([w/2, h/2])
                    return np.sqrt(np.sum(xy**2))

            def R(a, r, w, h):
                aa = (a % (np.pi/4))*((a % (np.pi/2)) <= np.pi/4) + \
                     (np.pi/4 - (a % (np.pi/4)))*((a % (np.pi/2)) >= np.pi/4)
                return R90(aa, r, *[w, h][::int(np.sign(np.cos(2*a)))])

            bbox = self.text.get_window_extent()
            X = R(angle, r, bbox.width, bbox.height)
            trans = self.ax.figure.dpi_scale_trans.inverted()
            offs = trans.transform(((X-s/2), 0))[0] * 72
            self.text.set_position([offs*np.cos(angle), offs*np.sin(angle)])