#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar  8 15:35:32 2022

@author: thellemans
Code obtained from Eleni paper
"""

import numpy as np
import matplotlib.pyplot as plt
import scipy.constants as c

from .data_handling import read_file
from ..simulation.QPC_geometry import qpc_data


def plot_1D_map(filename, xt, yt, qpc_n = None, conductance=True, V3_zero=False, hlines=None, 
                linestyle='-', ylimits=None, label=None, ax=None, savefig=False, color=None, resistance_factor=1, legend=True):
    '''
    Plot 1D map of the sample
    filename: filanme (+location if not in folder) of 2D data
    xt : X coordinate of the sample (str) X1 - X6
    yt : Y coordinate of the sample (str) Y1 - Y3
    
    resistance_factor: float
        Factor to scale the series resistance, this means:
        0: the series resistance is not subtracted
        1: the series resistance is being subtracted
        Other numbers are used to scaled the series resistance R_series = V_b / I_0
        
    '''  
    dictionary= read_file(filename, file_type='experiment')
    Sample=xt +'_'+ yt
    
    ### plot 1D data
    
    if ax is None:
        fig,ax = plt.subplots(nrows=1, ncols=1)
        
    if qpc_n is None:
        qpc_n = np.arange(1,len(dictionary[Sample]))
    if isinstance(qpc_n, int):
        qpc_n = [qpc_n]
    assert isinstance(qpc_n, (int, list, np.ndarray)), 'Enter a valid qpc number (int, list or np.ndarray)'
    
    x_min, x_max = float('inf'), -float('inf')
    dev_shape = {'X1':'a', 'X2':'b', 'X5':'a', 'X6':'c'}
    dev_index = {'X1':0, 'X2':0, 'X5':1, 'X6':0}
    
    for index, qpc in enumerate(qpc_n):

        V3 = qpc_data[dev_shape[xt] + str(qpc)]['V3_exp'][dev_index[xt]]\
                if V3_zero else 0
        
        voltage = dictionary[Sample][qpc]['voltage']['value'] - V3
        current = dictionary[Sample][qpc]['current']['value']

        if conductance:
            # Conversion current to conductance with series resistance into account:
            values = current/((500e-6) * (1 - resistance_factor*current/(current[0])))

            # Setting units:
            values *= c.Planck / (2* c.elementary_charge**2)
            ax.set_ylabel(r'G [$2e^2/h$]')
        else:
            values = current
            ax.set_ylabel('Current [{}]'.format(dictionary[Sample][index]['current']['unit']))
        
        ax.plot(voltage, values, linestyle, color=color, label= qpc if label is None else label[index])
        
        if ylimits is not None:
            # For setting the limits in the y-direction
            voltage_limited = voltage[np.where(np.logical_and(ylimits[0] <= values, values <= ylimits[1]))]
            x_min = min(np.min(voltage_limited), x_min)
            x_max = max(np.max(voltage_limited), x_max)

    if ylimits is not None:
        ax.set_xlim((x_min, x_max))
        ax.set_ylim(ylimits)
    
    if isinstance(hlines, (list, np.ndarray)):
        for line in hlines:
            ax.axhline(line, color = 'k', linestyle = '--', linewidth =0.5)
    
    ax.set_xlabel(r'$V_g$ [{}]'.format(
                    dictionary[Sample][qpc]['voltage']['unit']))
    if legend:
        ax.legend()
    ax.set_title(xt+'_'+yt)
    
    if savefig:
        plt.savefig(xt+'_'+yt+'_1D.pdf')
    return


def plot_2D_map(filename, xt, yt, qpc_n=1, ax=None, savefig=False):
    '''
    Plot 2D map of the sample
    filename: filanme (+location if not in folder) of 2D data
    xt : X coordinate of the sample (str) X1 - X6
    yt : Y coordinate of the sample (str) Y1 - Y3
    qpc_n: QPC sequential number (int) 1-16
    '''   
    dictionary= read_file(filename, file_type='experiment')
    Sample=xt +'_'+ yt
    
    botv=dictionary[Sample][qpc_n]['BotVoltage']['value']
    topv=dictionary[Sample][qpc_n]['TopVoltage']['value']
    current=dictionary[Sample][qpc_n]['current']['value']
    
    X, Y = np.meshgrid(botv, topv)
    z = np.reshape(current, (-1, np.size(botv)))
    if ax is None:
        fig, ax = plt.subplots(figsize = (8,6))
    cf = ax.contourf(X, Y, z, 30)
    
    cbar = plt.colorbar(cf, ax=ax)
    cbar.set_label(r'Current (A)', fontsize=14)
    ax.set_xlabel(r'$V_{bot}$ (V)', fontsize=14)
    ax.set_ylabel(r'$V_{top}$ (V)', fontsize=14)
    ax.set_title(Sample+'_'+str(qpc_n))
    
    if savefig:
        plt.savefig(Sample+'_'+str(qpc_n)+'_2D.pdf')
    return 