#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar 23 13:03:19 2022

@author: thellemans
"""
from mpi4py import MPI
import numpy as np
import scipy.constants as c
#from tqdm import tqdm
import warnings
import copy
import pickle

from pescado.tools import DiscreteIldos, SparseVector

from . import builder
from ..pescado_kwant import extension_pes, plotting_pes
from ..tools.plotting_sim import crossings
from ..tools.data_handling import Data

comm = MPI.COMM_WORLD


def calibrate_voltage(p_geom, p_sim):
    # Geometric capacitance GaAs (2DEG) layer
    C_2DEG = c.epsilon_0 * p_geom['eps_GaAs'] / (p_geom['d_2DEG']/2 *1e-9)
    
    # Geometric capacitance AlGaAs layer
    D_bottom = (p_geom['d1'] + p_geom['d2']/2)*1e-9
    C_bottom = c.epsilon_0 * p_geom['eps_AlGaAs'] / D1
        
    # Total geometric capacitance
    C_2 = 1/(1/C_2DEG + 1/C_bottom)
    
    # Quantum capacitance
    rho = p_geom['effective_mass'] * c.electron_mass /(c.pi * c.hbar **2)
    C_quan = c.elementary_charge**2 * rho
    
    voltage_shift = (1/C_2 + 1/C_quan)*c.elementary_charge*p_sim['DEG_dens']
    return voltage_shift


def calibrate_dopant(p_geom, p_sim, comm=None):
    """
    This still needs to be parallelized properly using mpi
    
    This needs to be redone, set dopant to flexible (?) -> how does this
    interact with the other flexible sites
    
    """
    # Calculate the theoretical voltage to obtain the correct charge density in 
    # the 2DEG
    D1 = (p_geom['d1'] + p_geom['grid_fine'][-1])*1e-9
    rho = p_geom['effective_mass'] * c.electron_mass /(c.pi * c.hbar **2)

    C_geom = (c.epsilon_0 * p_geom['eps_AlGaAs'])/D1
    C_quan = rho*c.elementary_charge**2
    V0 = (1/C_geom + 1/C_quan) * p_sim['DEG_dens'] * c.elementary_charge
    
    # Use this voltage to calculate the charge distribution in the dopant layer
    p_geom_cal = copy.deepcopy(p_geom)
    p_sim_cal = copy.deepcopy(p_sim)
    p_geom_cal['calibration_device'] = True
    p_sim_cal['setting_solver'] = 'lhh'
    
    # Calculate a knew pp_problem
    pp_builder = builder.make_pescado_system(**p_geom_cal)
    
    # Get the poisson problem from the poisson builder (only returned correctly
    # on rank 0)
    pp_problem = extension_pes.finalized(pp_builder, comm=comm)
    
    # Set the initial values for the calibration device
    voltage_sv = pp_problem.sparse_vector(val=0, name='gates')
    voltage_sv.extend(pp_problem.sparse_vector(val=V0, name='dopant'))
    helmholtz_sv = pp_problem.sparse_vector(val=-p_sim['helmholtz_coef'], 
                                            name='2DEG')
    
    # Solve the pp_problem
    voltage_res, charge_res = pp_problem.solve(voltage = voltage_sv,
                                               helmholtz_density= helmholtz_sv)
    
    # Normally we need to do some post processing on the charge for the
    # helmholtz sites, but we are only interested in the charge in the dopant 
    # layer
    
    # The problem here is that we assume that the indices in both pp_problems 
    # correspond to the same coordinates in real space, is this correct?
    
    # Read the charge results in the dopant layer
    values, indices = pp_problem.read_sparse_vector(charge_res, 
                                                    name='dopant', 
                                                    return_indices=True)
    
    dopant_dens = pp_problem.sparse_vector(val=values, indices=indices)
        
    return dopant_dens


def calibrate(pk_problem, p_geom, p_sim):
    """
    Parameters
    ----------
    pk_problem : PescadoKwantProblem
        The PescadoKwantProblem that has to be calibrated.
    p_geom : dict
        Geometry parameters of the QPC.
    p_sim : dict
        Simulation parameters.

    Returns
    -------
    voltage_shift : float
        Read the report corresponding to this project for more details. 
        Whenever the user specifies that some physical quantity has to be 
        calculated at voltage V_g_user, V_g_user is a calibrated voltage. 
        This makes sure that whenever the user asks for the density/potential 
        in the 2DEG at V_g_user = 0V, a uniform density/potential is obtained. 
        This means that the voltage that is internally used satisfies: 
            V_g_internal - voltage_shift = V_g_user (cfr. eq. 3.9 report)
        
    dopant_dens : SparseVector
        DESCRIPTION.

    """
    
    # Setting the default values
    voltage_shift = None
    dopant_dens = p_sim['DEG_dens'] * 1e-18 / p_geom['d2']
    
    # Calibration
    if isinstance(p_sim['calibration'], bool) and p_sim['calibration']:
        if p_sim['calibration_method'] == 'd':
            dopant_dens = calibrate_dopant(p_geom, p_sim)
        # If none of the above calibration methods is chosen but we need to
        # calibrate, we use 'v2', the fastest option
        else:
            voltage_shift = calibrate_voltage(p_geom, p_sim)
    
    elif isinstance(p_sim['calibration'], (float, int)):
        voltage_shift = p_sim['calibration']
    else:
        voltage_shift = None
    
    return voltage_shift, dopant_dens
    

def get_branch(disc_ildos, voltage):
    """
    returns a numpy array with branch numbers 
    """
    assert voltage.default is None
    
    ildos_branch_edges = disc_ildos[:,0][1:-1]
        
    branch_values = np.sum(np.array([voltage.values > edge 
                         for edge in ildos_branch_edges]), axis=0).astype(int)
    
    return branch_values


def _solve(pk_problem, energy_list, V_gates_list, dopant_dens, 
          helmholtz_coef, t, voltage_shift=None, 
          setting_solver='fsc', calc_V3=True, calc_cond=True, 
          save_poisson=True, iterations=1, num_moments=100, temp=0, 
          params={}, save_iteration=True, **kwargs):
    """
    Parameters
    -----------
    pp_problem: instance of pescado.poisson.PoissonProblem
    
    V_gates_list: np.ndarray
        contains gate voltages for which the poisson problem has to be solved
    
    dopant_dens: float, int or sparse_vector
        Charge density in the dopant layer [nm^-3]
    
    helmholtz_coef: float or int
        DOS in the 2DEG expressed in ...
        
    voltage_shift: float or int
        +
        
    ASSUMING V_gates_list in rising order for calculation V3_dens
    
    Returns
    --------
    voltage: list of sparse_vector instances, len(voltage) = len(p.V_gates_list)
    
    charge: list of sparse_vector instances, len(charge) = len(p.V_gates_list)
    
    """
    voltage = [] if save_poisson else None
    charge = [] if save_poisson else None
    ldos = [] if ('fsc' in setting_solver) or ('nr' in setting_solver) else None
    conductance = np.empty((len(V_gates_list), len(energy_list))) if calc_cond else None
    V3_dens = -float('inf')
    precision = None
    initial_config = 1
    initial_pot = 0
    rpp_problem = pk_problem.rpp_problem
    
    if voltage_shift is not None:
        V_gates_list = V_gates_list + voltage_shift
        

    if setting_solver in ['nr_lat']:
        ldos = pk_problem.exact_ldos(num_moments=num_moments, 
                                     where = lambda s: s.pos == np.array([0,0]), 
                                     params=params)
        
        flexible_index = pk_problem.rpp_problem.linear_problem_inst.regions_index[3]
        sites_ildos = SparseVector(indices = flexible_index, 
                                   values = np.zeros(len(flexible_index)))
    
    elif setting_solver in ['pi_lat']:
        disc_ildos = pk_problem._lat_ildos_piecewise(
            num_moments=num_moments, temp=temp, center=[0,0], 
            params=params)
        
    elif setting_solver == 'pi_bulk':
        disc_ildos = np.array([[-1, 0], [0,0], [1, helmholtz_coef]])
    
    elif setting_solver in ['dis_lat', 'fsc_nr']:
        ildos_dos, mu_limits = pk_problem._lat_ildos_dos_continuous(
            num_moments=num_moments, temp=temp, center=[0,0], 
            params=params, return_info=True)
        if setting_solver == 'dis_lat':
            functional_points = np.array([-1, 1], dtype=float)
            functional_points = np.insert(functional_points, 1, 
                                          np.linspace(*mu_limits, 5))
            disc_ildos = DiscreteIldos(quantum_functional=ildos_dos, 
                                       functional_points=functional_points, 
                                       potential_precision=1e-8)
            
            sites_ildos = SparseVector(
                    indices = rpp_problem.sub_region_index['2DEG'], 
                    values = np.arange(len(rpp_problem.sub_region_index['2DEG'])))
            
            disc_ildos = [disc_ildos]*len(sites_ildos.values)
        
    
    for index_v, V_gates in enumerate(V_gates_list): 
         
        if setting_solver == 'fsc_nr':
            
            voltage_vec, charge_vec, ldos_vec = \
                pk_problem.solve_fsc_nr(voltage = {'gates': V_gates},
                                    charge_density = {'dopant': dopant_dens},
                                    initial_potential = {'2DEG': initial_pot},
                                    num_moments=num_moments,
                                    iterations=iterations, 
                                    params=params, 
                                    center = np.array([0,0]), 
                                    save_iteration = save_iteration)
            
            # Built a list containing the voltage/charge values for every V_gates
            if save_poisson:
                voltage.append(voltage_vec)
                charge.append(charge_vec)
                ldos.append(ldos_vec)
                
            if save_iteration:
                voltage_res = voltage_vec[-1]
                charge_res = charge_vec[-1]
            else:
                voltage_res = voltage_vec
                charge_res = charge_vec
            
            # We can use the obtained voltage results to get a better 
            # initialisation for the next iteration
            initial_pot = rpp_problem.read_sparse_vector(voltage_res, 
                                                         name='2DEG')
            
        elif setting_solver == 'fsc_nrn':            
            
            voltage_vec, charge_vec, ldos_vec = \
                pk_problem.solve_fsc_nr(voltage = {'gates': V_gates},
                                    charge_density = {'dopant': dopant_dens},
                                    initial_potential = {'2DEG': initial_pot},
                                    num_moments=num_moments,
                                    iterations=iterations, 
                                    params=params, 
                                    threshold_ldos=helmholtz_coef/3, 
                                    center = np.array([0,0]), 
                                    save_iteration = save_iteration)
            
            # Built a list containing the voltage/charge values for every V_gates
            if save_poisson:
                voltage.append(voltage_vec)
                charge.append(charge_vec)
                ldos.append(ldos_vec)
            
            if save_iteration:
                voltage_res = voltage_vec[-1]
                charge_res = charge_vec[-1]
            else:
                voltage_res = voltage_vec
                charge_res = charge_vec
            
            # We can use the obtained voltage results to get a better 
            # initialisation for the next iteration
            initial_pot = rpp_problem.read_sparse_vector(voltage_res, 
                                                         name='2DEG')
        
        else:
            if setting_solver in ['pi_bulk', 'pi_lat']:
                voltage_res, charge_res = rpp_problem.solve_sc_piecewise(
                    voltage = {'gates': V_gates},
                    charge_density = {'dopant': dopant_dens},
                    configuration = {'2DEG': initial_config},
                    ildos = disc_ildos)                
                # We can use the obtained voltage results to get a better 
                # initialisation for the next iteration
                initial_config = get_branch(disc_ildos, voltage_res)
                
            elif setting_solver in ['dis_lat']:
                            
                voltage_res, charge_res = rpp_problem.solve_sc_piecewise(
                    voltage = {'gates': V_gates},
                    charge_density = {'dopant': dopant_dens},
                    configuration = {'2DEG': initial_config},
                    ildos = disc_ildos, 
                    sites_ildos = sites_ildos)
                
            elif setting_solver == 'nr_lat':
                voltage_res, charge_res = rpp_problem.solve_sc_continuous(
                    voltage = {'gates': V_gates},
                    charge_density= {'dopant': dopant_dens},
                    initial_potential = {'2DEG': initial_pot},
                    quantum_functional = ldos.quantum_functional_lat, 
                    sites_ildos = sites_ildos)
                
                # We can use the obtained voltage results to get a better 
                # initialisation for the next iteration
                initial_pot = rpp_problem.read_sparse_vector(voltage_res, 
                                                             name='2DEG')
                
            else:            
                rpp_problem.assign_flexible(
                    helmholtz_index = rpp_problem.sub_region_index['2DEG'])
                
                voltage_res, charge_res = rpp_problem.solve_lhh(
                    voltage = {'gates': V_gates},
                    charge_density = {'dopant': dopant_dens},
                    helmholtz_coef = {'2DEG': -helmholtz_coef})
                
                charge_res = charge_res * (-1)
        
            # Built a list containing the voltage/charge values for every V_gates
            if save_poisson:
                voltage.append(voltage_res)
                charge.append(charge_res)
        
        if calc_V3:
            # Check for the pinch of voltage V3
            charge_center = charge_res[np.where((pk_problem.coord == (0,0,0))\
                                                 .all(axis=1))[0][0]]
            
            if float(charge_center) == 0:
                V3_dens = V_gates - voltage_shift if voltage_shift is not None \
                                                  else V_gates
                if index_v+1 < len(V_gates_list):
                    precision = abs(V_gates_list[index_v+1] - V_gates)
                else: 
                    precision = None
            
        del charge_res
        
        # Solve the conductance
        if calc_cond:
            conductance[index_v] = pk_problem.conductance(
                potential = voltage_res * (c.elementary_charge/t), 
                energy = energy_list, params=params)[0]
        
    return voltage, charge, ldos, conductance, [V3_dens, precision]



def V3_cond_solver(V_gates_list, conductance):
    """
    Returns a list with V3_cond and length equal to the energy_list
    """
    
    # If we receive one list/array we assume that it is in function of V_gates
    # Otherwise it should have the shape (V_gates_list, energy_list)
    if not isinstance(conductance[0], (np.ndarray, list)):
        conductance = np.array(conductance)[:,None]
    
    V3_cond = []
    for elem in conductance.transpose():
        _, cross_points, _ = crossings(V_gates_list, elem)
        V3_cond.append(cross_points[0,0] if cross_points.size != 0 else None)
    return V3_cond


def V3_dens_solver(V_gates_list, coord, charge):
    '''
    Based on the simulation of the charge density in the 2DEG this function
    returns the last gate voltage for which the density in the center of the 
    2DEG is equal to zero (V3) together with the precision of this value, i.e. the 
    interval between the last voltage value for which the center of the 2DEG is 
    depleted and the first voltage value for which the center of the 2DEG is 
    not depleted.
    '''
    # Getting the charge values in the center of the 2DEG
    _, charge_point = plotting_pes.data_cross_section(coord, charge, section=[[0,0,0]])
    index_first_nonzero = np.argwhere(charge_point[0,:,0])[0][0]
    
    V3_dens = V_gates_list[index_first_nonzero-1]
    precision = V_gates_list[index_first_nonzero] - V3_dens
    
    return [V3_dens, precision]


def extend_params_solver(p_geom, p_sim):
    # Extend the pescado problem parameters    
    
    # Standard density of states in a 2D system
    rho = p_geom['effective_mass'] * c.electron_mass /(c.pi * c.hbar **2)
    
    # Conversion to the right units
    # 1e-18 necessary for the conversion between /Jm² and /Jnm² (pescado works in nm), 
    # elementary charge to have it in 1/(V nm²) instead of 1/Jnm²
    # np.product(p_geom['grid']) to have it in /Vcell instead of /Vnm² (for 3D)
    helmholtz_coef = rho * 1e-18 * c.elementary_charge * np.product(p_geom['grid'])
    
    p_sim['helmholtz_coef'] = helmholtz_coef
    

    if isinstance(p_sim['calibration'], bool) and p_sim['calibration']:
        assert p_sim['calibration_method'] in {'v1', 'v2', 'd'}, \
        'Please choose a valid calibration method "v1" (voltage based on extra\
        simulation) "v2" (voltage from equation) or "d" (dopant)'
        
    assert p_sim['setting_solver'] in ['fsc_nrn', 'fsc_nr', 'fsc_lhh', 'pi_bulk', 
                'pi_lat', 'nr_lat', 'lhh', 'dis_lat'], (
        "Select a valid pescado solver: 'fsc_nr', 'fsc_lhh', 'pi_bulk', 'pi_lat', 'nr_lat', 'lhh' or 'dis_lat', "
        "currently: {}".format(p_sim['setting_solver']))


def read_input(pk_problem=None, p_geom=None, p_sim=None, file_geom=None, 
               file_sim=None):
    """
    If p_sim or p_geom are provided they overrule the files! Therefore, if you 
    provide p_geom, also pk_problem has to be provided.
    The assert statements below express in a logical way what the input 
    combinations are.
    
    Recommended operation regimes:
        1) provide pk_problem, p_geom, p_sim
        2) provide file_geom (that includes p_geom AND pk_problem), file_sim
    """
    assert ((p_geom is None) and (pk_problem is None))^(file_geom is None)
    assert (p_sim is None) ^ (file_sim is None)
    
    if p_sim is None:
        with open(file_sim, 'rb') as inp:
            p_sim = pickle.load(inp)
    
    if p_geom is None:
        with open(file_geom, 'rb') as inp:
            inp = pickle.load(inp)
        if isinstance(inp, (tuple, list)):
            p_geom = inp[0]
            pk_problem = inp[1]
        else:
            p_geom = inp
            
    return pk_problem, p_geom, p_sim  
    
    
def split_job(V_gates, energy, size):
    """
    It should be possible to generalise this function and make
    it recursive to be able to handle with multiple job lists
    Not only 2 jobs lists
    Introduce a parameter: grouped = [True, False, False] to 
    specify wheter the jobs should remain grouped as in V_gates
    or not as in energy. The grouped parameters should be
    mentioned first in the jobs list. 
    """
    
    if isinstance(energy, (float, int)):
        energy = np.array([energy])
        
    if isinstance(V_gates, (float, int)):
        V_gates = np.array([V_gates])
    
    # If there are more gate voltages than the size
    # of the communicator, the gate voltages are just devided
    # and every system has to do the calculation for 
    # all the energies (kwant uses a single rank anyway)
    if len(V_gates) >= size:
        V_gates_job = np.array_split(V_gates, size)
        energy_job = list(np.tile(energy, (size, 1)))

    # If there are less gate voltages than the size
    # of the communicator we divide the voltages and 
    # energies over the communicator. This means that 
    # some ranks will initially do the same calculation
    # for the potential and afterwards focus on 
    # different energies
    else:
        size_used = min(size, len(V_gates) * len(energy))
        rank_division = np.array_split(np.arange(size_used), len(V_gates))
        V_gates_indices = np.concatenate([[index]*(len(ranks)) 
                        for index, ranks in enumerate(rank_division)])

        V_gates_job = list(V_gates[V_gates_indices][:, None])

        energy_job = []
        for ranks in rank_division:
            if len(ranks) > 1:
                local_energy_job = np.array_split(energy, len(ranks))
                for elem in local_energy_job:
                    energy_job.append(elem)
            else:
                energy_job.append(energy)
                
        if size_used < size:
            V_gates_job += [np.array([])]*(size-size_used)
            energy_job += [np.array([])]*(size-size_used)

    return V_gates_job, energy_job


def combine_job(data, V_gates, energy, size, dependence_energy=True):
    if isinstance(energy, (float, int)):
        energy = np.array([energy])
        
    if isinstance(V_gates, (float, int)):
        V_gates = np.array([V_gates])
    
    if len(V_gates) <= size:
        # The dimension of the data, only voltage dependent or also energy dependent
        size_used = min(size, len(V_gates) * len(energy))
        rank_division = np.array_split(np.arange(size_used), len(V_gates))

        if dependence_energy:        
            # Concatenate along energy
            data = [np.concatenate([data[rank] for rank in ranks], axis=1) 
                    for ranks in rank_division]
        else:
            # Filter along energy, since the data is independent from energy, 
            # but might be calculated on several ranks
            data = [data[ranks[0]] for ranks in rank_division]

    # Concatenate along voltage
    data = np.concatenate(data, axis=0)
    
    # maybe this would work for ldos , not exactly the same as concatenate
    # data = np.array([elem for elem in data], dtype=dtype)

    return data


def solve(pk_problem=None, p_geom=None, p_sim=None, file_geom=None, 
          file_sim=None, file_data=None):
    
    """
    p_sim.calibration: float, int or boolean
        if float or int, this value is used for calibration of the gate voltage
        if boolean it decides if the calibration voltage should be calculated 
        and applied or not
        
    result only returned correctly on rank 0!
    """
    if comm.rank == 0:
        
        pk_problem, p_geom, p_sim = read_input(pk_problem, p_geom, p_sim, 
                                               file_geom, file_sim)
            
        # Extend the simulation parameters with some important values
        extend_params_solver(p_geom, p_sim)
        
        # Calibration
        voltage_shift, dopant_dens = calibrate(pk_problem, p_geom, p_sim)
        
        # Division of the jobs
        V_gates_job, energy_job = split_job(p_sim['V_gates_list'], 
                                            p_sim['energy_list'], comm.size)
        
        print('\n'.join(['JOB1:'] + ['Rank {} :'.format(i) + str(elem) 
                                     for i, elem in enumerate(V_gates_job)]))
        print('\n'.join(['JOB1:'] + ['Rank {} :'.format(i) + str(elem) 
                                    for i, elem in enumerate(energy_job)]))
    else:
        p_geom = None
        p_sim = None
        voltage_shift = None
        dopant_dens = None
        V_gates_job = None
        energy_job = None
    
    # Distribute the information and jobs over the ranks
    pk_problem, p_geom, p_sim, voltage_shift, dopant_dens = comm.bcast(
        (pk_problem, p_geom, p_sim, voltage_shift, dopant_dens), root=0)
    
    V_gates_job = comm.scatter(V_gates_job, root=0)
    energy_job = comm.scatter(energy_job, root=0)
    
    extra_input = {**p_sim, **p_geom}
    del extra_input['V_gates_list']
    del extra_input['energy_list']
    
    x_coord_kwant = [site.pos[0] for site in pk_problem.kwant_sys.sites]
    params = {'lead_left': min(x_coord_kwant), 
              'lead_right': max(x_coord_kwant)}
    
    # Solve the problem
    voltage_job, charge_job, ldos_job, conductance_job, V3_dens_job = \
        _solve(pk_problem=pk_problem, 
                V_gates_list=V_gates_job, 
                energy_list=energy_job,
                dopant_dens=dopant_dens, 
                voltage_shift=voltage_shift,
                params = params, 
                **extra_input)
    
    print('conductance rank {}: {}'.format(comm.rank, conductance_job))
    
    # Gather the information and jobs on rank 0
    voltage = comm.gather(voltage_job, root=0)
    charge = comm.gather(charge_job, root=0)
    ldos = comm.gather(ldos_job, root=0)
    conductance = comm.gather(conductance_job, root=0)
    V3_dens = comm.gather(V3_dens_job, root=0)

    # Postprocessing on rank 0
    if comm.rank == 0:        
        if p_sim['save_poisson']:
            voltage = combine_job(voltage, p_sim['V_gates_list'], 
                                  p_sim['energy_list'], comm.size, False)
            charge = combine_job(charge, p_sim['V_gates_list'], 
                                  p_sim['energy_list'], comm.size, False)
        else:
            voltage, charge = None, None
        
        if 'fsc' in p_sim['setting_solver']:
            # Make combine job work for this case, set dtype=object in concatenate
            ldos =  combine_job(ldos, p_sim['V_gates_list'], 
                                p_sim['energy_list'], comm.size, False)
            # ldos = None
        elif 'nr' in p_sim['setting_solver']:
            ldos = ldos[0]
        else:
            ldos = None
            
        if p_sim['calc_cond']:
            conductance = combine_job(conductance, p_sim['V_gates_list'], 
                                      p_sim['energy_list'], comm.size, True)
        else:
            conductance = None
        
        # Calculation V3_dens from the results of the different ranks
        V3_dens = np.array(V3_dens)
        V3 = V3_dens[0][0] if V3_dens.shape[0] == 1 else np.max(V3_dens[:,0])
        if V3 != -float('inf'):
            index = np.where(np.round(p_sim['V_gates_list'],6) == 
                                 np.round(V3,6))[0][0]
            precision = np.append(np.diff(p_sim['V_gates_list']), None)[index]
            V3_dens = [V3, precision]
        else:
            V3_dens = [None, None]
        
        # Calculation pinch of voltage based on conductance
        if p_sim['calc_V3'] and conductance is not None and \
            conductance.shape[0] > 20:
            V3_cond = V3_cond_solver(p_sim['V_gates_list'], conductance)
        else:
            V3_cond = None
            
        # Calculation of the kwant coordinates, this is usefull for later 
        # plotting etc.
        coord_kwant = np.array([site.pos for site in 
                                pk_problem.kwant_builder.sites()])
    
        coord_kpm = np.array([site.pos for site in 
                              pk_problem.kpm_builder.sites()])
        
        # Setup of resulting dictionary
        result = {'p_geom' : p_geom, 
                  'p_sim' : p_sim,
                  'file_geom' : file_geom, 
                  'file_sim' : file_sim,
                  'coord' : pk_problem.coord,
                  'coord_kwant' : coord_kwant,
                  'coord_kpm' : coord_kpm, 
                  'voltage' : voltage, 
                  'charge' : charge, 
                  'ldos' : ldos,
                  'conductance' : conductance,
                  'voltage_shift' : voltage_shift,
                  'dopant_dens' : dopant_dens,
                  'V3' : [V3_dens, V3_cond]}

        data = Data(file=file_data, **result)

    else:
        data = None
    
    return data
