# -*- coding: utf-8 -*-
"""
Created on Fri Aug 12 15:09:41 2022

@author: helle
"""

import numpy as np
import qpc_exp2sim.tools.data_handling as dh
import scipy.constants as c
import sys
from mpi4py import MPI
from qpc_exp2sim.simulation import solver


# Use default communicator. No need to complicate things.
comm = MPI.COMM_WORLD

if comm.rank == 0:
    data = dh.read_file(sys.argv[1])
    p_geom, pk_problem = dh.read_file(data.file_geom)
    
    conversion_unit = c.elementary_charge/p_geom['t']
    voltage = data.voltage 
    
    if len(data.voltage.shape) > 1:
        voltage = voltage[:,-1]
    
    potential = voltage * conversion_unit
    energy = np.linspace(-2.5, 7.5, 8)

    potential_job, energy_job = solver.split_job(potential, 
                                  energy, comm.size)
    
    x_coord_kwant = [site.pos[0] for site in pk_problem.kwant_sys.sites]
    params = {'lead_left': min(x_coord_kwant), 
              'lead_right': max(x_coord_kwant)}
else:
    params, pk_problem = None, None
    potential_job, energy_job = None, None

params, pk_problem = comm.bcast((params, pk_problem), root=0)

# scatter the jobs over the different ranks
potential_job = comm.scatter(potential_job, root=0)
energy_job = comm.scatter(energy_job, root=0)
print('Rank {}'.format(comm.rank), potential_job, energy_job)

# perform the job
conductance_job = pk_problem.conductance(potential_job, energy_job, params)
print('Rank {}'.format(comm.rank), conductance_job)

# combine the jobs on rank 0
conductance = comm.gather(conductance_job, root=0)


if comm.rank == 0:
    
    conductance = solver.combine_job(conductance, potential, 
                              energy, comm.size, True)
    
    data.p_sim['energy_list'] = energy
    data.conductance = conductance
    
    print(conductance)
    print(conductance.shape)
    
    dh.write_file(data, sys.argv[1])