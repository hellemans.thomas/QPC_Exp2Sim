#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May  4 11:13:12 2022

@author: thellemans
"""
import collections
import numpy as np
import warnings
import tinyarray as ta
from functools import partial, reduce
import copy 
import os
import multiprocessing

from pescado import mesher, tools
from pescado.mesher import shapes
from pescado.poisson.builder import sparse_capa_matrix
from pescado.poisson import PoissonProblem
from pescado.tools._meshing import where as c_where
from .timing import timing

"""
The following adaptions/extension are made in comparison to the original 
pescado

1)  Rewritten shapes.reflect such that it doesn't use nested functions
    anymore. Nested functions can't be pickled, therefore they avoid that 
    a PoissonProblem object can be pickled. Pickling a PoissonProblem object
    is interesting for paralleliszation with MPI.
    Note: The same should be done for shapes.rotates, shapes.dilatate, ...
    more fundamental in shapes.Shape the __sub__ method should also be 
    adapted in this way.
2)  Extension to mesher.shapes such that extrusions and cross_sections of 
    shapes can easily be computed.
3)  Adaption of the pescado.builder functions convert_kwant() and 
    add_kwant_system() such that kwant_systems with dimension d can be 
    integrated into pescado.Builder objects with dimension d+n
4)  Parallelization of the finalisation of a poisson.Builder object using MPI
    instead of fork
5)  Parallelization of the calculation of the volume using multiprocessing 
    (fork) and MPI, the volume needs to be calculated when intitialising an 
    PoissonProblem object.
6)  Introduction of the ReducedPoissonProblem, we only keep the highly 
    necessary parts of the poisson problem for further calculations. 
    Especially the deletion of the mesh_inst saves a lot of memory.
7)  Note: the parameter 'parallelize' for the calculation of the linear problem
    in PoissonProblem._initialize_LP, is never used. Parallelize is passed to
    arrange_matrix_elements() and afterwards group_matrix() where it dies 
    quietly.
"""



def indices_from_coordinates(all_coord, coordinates, tol=1e-12):
    '''Copied from pescado
    Return the indices associated with a 'coordinates'

    Parameters
    -----------

        coordinates: sequence of floats or np.array (optional- default None)
            Mesh coordinates

        tol: float

    '''

    coordinates = np.asarray(coordinates)
    if len(coordinates.shape) == 1:
        coordinates = coordinates[None, :]

    idx = c_where(
        coordinates, all_coord,
        tol=tol)

    not_in_mesh = np.arange(0, len(coordinates))[idx == -1]

    if len(not_in_mesh) > 0:
        warnings.warn(
            ('Coordinates : \n {0} were not found in the mesh.'.format(
                 coordinates[not_in_mesh]))
             + 'They are tagged as -1')

    return idx


def _func_reflected_coord(x, project_mat, origin):
        dim = x.shape[1]

        assert np.all(np.array(project_mat.shape) - dim == 0), (
            'Projection matrix dimensions larger than coordinate dimensions')

        return (x @ (np.identity(dim) - 2 * project_mat)
                + 2 * origin @ project_mat)


def _func_inverse_reflected_coord(x, project_mat, origin):

    dim = x.shape[1]

    assert np.all(np.array(project_mat.shape) - dim == 0), (
        'Projection matrix dimensions larger than coordinate dimensions')

    inv_proj = np.linalg.inv(np.identity(dim) - 2 * project_mat)
    orig_mat = project_mat @ inv_proj

    return (x @ inv_proj  - 2 * origin @ orig_mat)


def _reflection(origin, normal):
    """Reflection across a given line (2D) or plane (3D)

    Parameters
    ----------
    origin: np.array, shape(1, d), floats
        Origin of the plane / line

    normal: np.array, shape(1, d), floats
        Vector normal to the plane / line \n

    Returns
    -------
        fun(r) -> r'
            r: np.array, shape(m, d), floats \n
                r the initial coordinates
            r': np.array, shape(m, d), floats \n
                r' reflected coordinates
            Function returning the reflected coordinates \n
            m number of coordinates \n
            d their dimension \n

        fun^{-1}(y) -> x
            x: np.array, shape(m, d), floats \n
            y: np.array, shape(m, d), floats \n
            Inverse function returning the "unreflected" coordinates \n
            m number of coordinates \n
            d their dimension \n

    Notes
    ------

    The two equations are defined as follows

        fun(r) = r(id - 2 * P) + 2 * O * P = r' \n

        fun^{-1}(r') =
        r'(id - 2 * P)^{-1} - 2 * O * P * (id - 2 * P)^{-1} = r \n

    with id the identity matrix, O the origin and P the projection matrix, i.e.

        P = (n / sqrt(n @ n)).T @ (n / sqrt(n @ n))

    Finally (id - 2 * P)^{-1} is calculated with np.linalg.inv
    """
    normalized_normal = normal / (normal @ normal.T) ** (1 / 2)

    project_mat = normalized_normal.T @ normalized_normal

    return partial(_func_reflected_coord, project_mat=project_mat, 
                             origin=origin), \
           partial(_func_inverse_reflected_coord, 
                             project_mat=project_mat, origin=origin)


def _func_is_inside(coord, old_is_inside, inverse_reflected_coord):

        new_coord = inverse_reflected_coord(coord)

        return old_is_inside(new_coord)


def reflect(shape, origin, normal):
    """Reflection of a Shape instance across a given line (2D) or plane (3D)

    Parameters
    ----------

    shape: Shape instance

    origin: np.array, shape(1, d), floats
        Origin of the plane / line

    normal: np.array, shape(1, d), floats
        Vector normal to the plane / line \n

    Returns
    --------
        Shape instance
            An instance of the reflected shape
    """

    old_is_inside = copy.deepcopy(shape._is_inside)
    bbox = shape.bbox

    origin = np.asanyarray(origin, dtype=np.floating)[None, :]
    normal = np.asanyarray(normal, dtype=np.floating)[None, :]

    tools.assert_coord_convention(origin, name='Reflection origin')
    tools.assert_coord_convention(normal, name='Reflection plane normal')

    reflected_coord, inverse_reflected_coord = _reflection(
        origin=origin, normal=normal)

    transformed_box = reflected_coord(shapes.bbox2box(bbox=bbox))

    transformed_bbox = np.zeros(bbox.shape)
    transformed_bbox[0, :] = np.min(transformed_box, axis=0)
    transformed_bbox[1, :] = np.max(transformed_box, axis=0)

    return shapes.General(func=partial(_func_is_inside, 
        old_is_inside=old_is_inside, 
        inverse_reflected_coord=inverse_reflected_coord), 
        bbox=transformed_bbox)


def _func_cross_section(pos_list, shape, section):
    assert len(pos_list[0]) == section.count(None), \
    'dimension of the submitted coordinate is {}, but should be \
     equal to {} (the dimension of simulation)'\
     .format(len(pos_list[0]), section.count(None))
    
    section_list = np.repeat(section, len(pos_list)).reshape((3,len(pos_list))).transpose()
    np.place(section_list, section_list == None, pos_list)
    return shape(section_list.astype(float))


def cross_section(shape, section):
    """
    This function allows to make 1D or 2D cross sections from the shapes
    defined above.
    """
    dim_simulation = section.count(None)
        
    if dim_simulation == len(section):
        return shape
    else:
        bbox = shape.bbox
        removed_axes = [i for i, val in enumerate(section) if val is not None]
        bbox = np.delete(bbox, removed_axes, axis=1)
        
        return shapes.General(partial(_func_cross_section, shape=shape, 
                                                section=section), bbox)
    

def _func_extrude(pos_list, shape, ax, interval):
    check =  []
    for pos in pos_list:
        pos_reduced = np.hstack([pos[:ax], pos[ax+1:]])[None, :]
        check.append(bool((interval[0] <= pos[ax] <= interval[1]) and shape(pos_reduced)))
    return check


def extrude(shape, ax, interval):
    """
    shape: n dimensional shape to extrude to an n+1 dimensional shape    
    interval = list : [min_val, max_val]
    ax = index of the axis along which to extrude the shape to an extra dimension
        0,1,2 for respectively x,y,z
    """

    bbox = np.insert(shape.bbox, ax, interval, axis=1)
        
    return shapes.General(partial(_func_extrude, shape=shape, 
                                            ax=ax, interval=interval), bbox)



def convert_kwant(kwant_builder, kwant_lattice=None, extra_dim=[], 
                  coord_extra_dim=[], pattern_extra_dim=[]):
    ''' Convert kwant object into Pattern + kwant_tags

    Parameters
    -----------

    kwant_builder: kwant.builder.Builder

    kwant_lattice: instance of kwant.lattice() (optional)

        If given, the function will see if there is a compatible
        mesher.patterns.Pattern() instance that can be generated
        instead of the default mesher.patterns.Finite()

        Optmization possible for kwant.lattice.Monoatomic
        with prime vectors defined along a single direction,
        e.g.

            prime_vectors = [[1, 0, 0], [0, 0.5, 0], [0, 0, 5]]
      
    The following parameters are only necessary if a kwant system needs to be 
    converted into a higher dimension:         
    
    extra_dim: list of int
        
        Integers representing the dimensions that need to be added in
        the kwant_builder: 0=x, 1=y, ...
        
    coord_extra_dim: list of int/float
    
        Value(s) that the kwant_builder coordinates must get in the 
        extra dimension(s). 
        NOTE: This could be expanded to a list of functions which
        take the initial coordinates as variable. e.g. to create
        the possibility of inserting a diagonal plane in a 3D device
        
    pattern_extra_dim: list
    
        Size of the unit vector(s) of the pattern in the extra dimension(s)
        Only necessary if a monatomic lattice is used. 

    Returns
    --------

    kwant_pat: instance of mesher.patterns.Pattern()

    kwant_coordinates: dictionary
        Keys : kwant.Sites instance

        values: np.array of floats
            sites coordinates obtained with kwant.Sites.pos

    '''
    print('convert kwant is used')
    extra_dim = np.array(extra_dim, dtype=int) - np.arange(len(extra_dim))
    
    kwant_pat = None
    if (str(type(kwant_lattice))
        == "<class 'kwant.lattice.Monatomic'>"):
        # Rectangular works if the
        # X, Y and Z axis are independant
        mask_pv = kwant_lattice.prim_vecs != 0.
        
        # If there's only one nonzero element in every prim_vec
        # This means that we have a rectangular pattern aligned along the axes
        # (Kwant already ensures linearly independent prim_vecs)
        if np.all(np.sum(mask_pv, axis=1) == 1):
                        
            kwant_pat = mesher.patterns.Rectangular.constant(
                    element_size=np.insert(kwant_lattice.prim_vecs[mask_pv],
                                           extra_dim, pattern_extra_dim))
    
    # Compatibility with python 3.6
    kwant_coordinates = collections.OrderedDict([
            (site, ta.array(np.insert(site.pos, extra_dim, coord_extra_dim))) 
            for site in kwant_builder.sites()])

    if kwant_pat is None:
        kwant_pat = mesher.patterns.Finite(
            elements=kwant_coordinates)

    return kwant_pat, kwant_coordinates


def add_kwant_system(
        pescado_builder, shape_inst, kwant_builder,
        sub_mesh_name=None, ext_bd=None, kwant_lattice=None,
        _refine=False, extra_dim=[], coord_extra_dim=[], pattern_extra_dim=[]):
        ''' 
        To put this function back in pescado -> change 'pescado_builder' by 
        'self' and remove return statement
        
        Add the points inside a kwant system to the mesh in
        pescado.poisson.PoissonProblemBuilder.

        Parameters
        -----------

        shape_inst: instance of pescado.mesher.Shape

        kwant.builder: kwant.builder.Builder

        kwant.lattice: instance of kwant.lattice (optional)
            If given, the mesher will look for the most
            efficient pattern to fill the 'shape_inst'
            instead of using the default Finite pattern.

        sub_mesh_name: string (optional parameter)
            Tag of the sub_region

            Defaults to 'quantum_region_{0}'. with
            {0} the number of the quantum sub_region.

        ext_bd: interger (optional, default None)

            Adds points forming the external boundary up to 'ext_bd'
            of the 'shape' instance in question.

            Uses pescado.mesher.external_boundary_shape(). Hence
            only convex shapes can be defined correctly.

        _refine: boolean (optional, default False)

            If True removes the shape_inst from the builder_inst
            using self.drill(shape_inst) before adding the sub_mesh
            to the builder_inst.
            
        The following parameters are only necessary if a kwant system needs to 
        be inserted into a pescado builder of higher dimension:         
        
        extra_dim: list of int
            
            Integers representing the dimensions that need to be added in
            the kwant_builder: 0=x, 1=y, ...
            
        coord_extra_dim: list
        
            Value(s) that the kwant_builder coordinates must get in the 
            extra dimension(s). 
            NOTE: This could be expanded to a list of functions which
            take the initial coordinates as variable. e.g. to create
            the possibility of inserting a diagonal plane in a 3D device
            
        pattern_extra_dim: list
        
            Size of the unit vector(s) of the pattern in the extra dimension(s)
            Only necessary if a monatomic lattice is used. 
        '''
        print('add kwant system is used')
        pattern_inst, coordinates = convert_kwant(
            kwant_builder=kwant_builder, 
            kwant_lattice=kwant_lattice, 
            extra_dim=extra_dim, 
            coord_extra_dim=coord_extra_dim,  
            pattern_extra_dim=pattern_extra_dim)

        pescado_builder.add_sub_mesh(
            shape_inst=shape_inst, pattern_inst=pattern_inst, 
            sub_mesh_name=sub_mesh_name, ext_bd=ext_bd, _refine=_refine)

        # Selects only the sites inside 'shape_inst'
        # This is important in case ext_bd is not None.
        # In such cases some kwant points used to make the pattern
        # might not be in the 'shape_inst'.
        sites = list(coordinates.keys())
        inside_sites_idx = np.arange(0, len(sites), dtype=int)[
            shape_inst(np.array([coordinates[site] for site in sites]))]

        pescado_builder.kwant_tags += [sites[idx] for idx in inside_sites_idx]
        pescado_builder.kwant_coordinates += [
            coordinates[sites[idx]] for idx in inside_sites_idx]
        
        return pescado_builder
    


def _dielectric_constant(pp_builder, mesh_inst):
    # Find the non default dielectric constant values
    dielectric_constant = np.ones(mesh_inst.npoints, dtype=float)
    
    for (val, shape_inst) in pp_builder.dielectric_functions:
        idx = np.arange(mesh_inst.npoints)[
            shape_inst(mesh_inst.coordinates())]
        dielectric_constant[idx] = val(
            mesh_inst.coordinates(indices=idx))
    
    dielectric_constant = dielectric_constant * pp_builder.vaccum_perm
    
    return dielectric_constant


def sparse_capa_matrix_parallel_mpi(mesh_inst, dielectric_cst, comm):
    """
    Only returned correctly on rank 0
    """
    
    from mpi4py import MPI
    
    # Split the mesh points in the amount of ranks we have and obtain the job
    # for a specific rank by indicing
    points_idx = np.array_split(np.arange(0, mesh_inst.npoints), 
                                comm.size)[comm.rank]

    # Do the jobs
    local_sparse_capa_matrix = sparse_capa_matrix(points_idx, 
                                                  mesh_inst, 
                                                  dielectric_cst)
        
    total_sparse_capa_matrix = comm.reduce(local_sparse_capa_matrix, 
                                           op=MPI.SUM, root=0)
    
    return total_sparse_capa_matrix


def finalized_mpi(pp_builder, capa_matrix=None, parallelize=False, comm=None):
    if comm.rank == 0:
        # Do not use _update_regions since default can change each time
        # finalized is used.
        regions = copy.deepcopy(pp_builder.regions)
        regions_shapes = copy.deepcopy(pp_builder.regions_shapes)
        
        # Set up default region to Neumann
        default_shape = pp_builder.builder_inst.shape
        if len(regions_shapes) > 0:
            default_shape -= reduce(
                lambda x, y: x | y, [g for g in regions_shapes.values()])
        
        default_name = pp_builder._generate_region_name(
            (default_shape, 'default'), 'neumann')
        
        regions['neumann'].append(default_name)
        regions_shapes.update({default_name: default_shape})
        
        # Finalize the mesh
        # Mesh to be assumed in nm
        print('Finalizing mesh builder')
        mesh_inst = pp_builder.builder_inst.finalized()
        
        # OrderedDict is used
        kwant_indices = None
        if pp_builder.kwant_coordinates:
            kwant_indices_list = mesh_inst.indices(
                coordinates=np.array(pp_builder.kwant_coordinates))
            kwant_indices = {
                site:index
                for site, index in zip(
                    pp_builder.kwant_tags, kwant_indices_list)}
        
        print('Done')
        
        dielectric_cst = _dielectric_constant(pp_builder, mesh_inst)
    else:
        regions = None
        regions_shapes = None 
        kwant_indices = None
        mesh_inst = None
        dielectric_cst = None
    
    # bcast the necessary variables to the other ranks
    mesh_inst, dielectric_cst = comm.bcast((mesh_inst, dielectric_cst), root=0)
    
    ###########################################################################
    
    # Center of the parallelization
    if capa_matrix is None:

        # MPI implementation of the calculation of the capacitance matrix
        # Only returned correctly on rank 0, else a local matrix that is wrong
        capa_matrix = sparse_capa_matrix_parallel_mpi(mesh_inst, 
                                                      dielectric_cst, comm)
    
    return PoissonProblem_copy(capacitance_matrix=capa_matrix,
                                regions=regions,
                                regions_shapes=regions_shapes,
                                kwant_indices=kwant_indices,
                                mesh_inst=mesh_inst,
                                parallelize=parallelize, 
                                comm=comm)


def finalized_fork(pp_builder, capa_matrix=None, parallelize=False):
    ''' Returns an instance of pescado.poisson.Solver

    Parameters
    -----------

    capa_matrix: instance of scipy.sparse.csr_matrix - default None
        Contains the elements of the capacitance matrix

        If None then a capacitance matrix is constructed.

    parallelize: bool - optional (default is False)
        If True, parallelize the capacitance matrix construction
        using the multiprocessing standart library.
    '''

    # Do not use _update_regions since default can change each time
    # finalized is used.
    regions = copy.deepcopy(pp_builder.regions)
    regions_shapes = copy.deepcopy(pp_builder.regions_shapes)

    # Set up default region to Neumann
    default_shape = pp_builder.builder_inst.shape
    if len(regions_shapes) > 0:
        default_shape -= reduce(
            lambda x, y: x | y, [g for g in regions_shapes.values()])

    default_name = pp_builder._generate_region_name(
        (default_shape, 'default'), 'neumann')

    regions['neumann'].append(default_name)
    regions_shapes.update({default_name: default_shape})

    # Finalize the mesh
    # Mesh to be assumed in nm
    print('Finalizing mesh builder')
    mesh_inst = pp_builder.builder_inst.finalized()

    # OrderedDict is used
    kwant_indices = None
    if pp_builder.kwant_coordinates:
        kwant_indices_list = mesh_inst.indices(
            coordinates=np.array(pp_builder.kwant_coordinates))
        kwant_indices = {
            site:index
            for site, index in zip(
                pp_builder.kwant_tags, kwant_indices_list)}

    print('Done')

    if capa_matrix is None:
        capa_matrix = pp_builder._make_capacitance_matrix(
            mesh_inst=mesh_inst, parallelize=True)

        print('Done')

    return PoissonProblem_copy(
                capacitance_matrix=capa_matrix,
                regions=regions,
                regions_shapes=regions_shapes,
                kwant_indices=kwant_indices,
                mesh_inst=mesh_inst,
                parallelize=parallelize)


@timing
def finalized(pp_builder, capa_matrix=None, parallelize='fork', comm=None):
    ''' 

    Parameters
    -----------

    capa_matrix: instance of scipy.sparse.csr_matrix - default None
        Contains the elements of the capacitance matrix
        If None then a capacitance matrix is constructed.

    parallelize: str
        'mpi': The pescado builder will be finalized using mpi 
        parallelisation, e.g. on the cluster
         
        'fork': The pescado builder will be finalized using fork 
        parallelisation, e.g. on a computer, jupyter notebook
        
        'original': The pescado builder will be finalized using the original
        parallelisation implemented in pescado, i.e. parallelize=True
        
        Any other value will be interpreted as False, no parallelization
        
    Notes
    -----
    ONLY RETURNS THE CORRECT PP_PROBLEM ON RANK 0 IF MPI IS USED
    
    '''
    # Split between standard pescado finalization or mpi finalization
    if parallelize == 'fork':
        print('Fork parallelization is used')
        pp_problem = finalized_fork(pp_builder, capa_matrix, 
                                    parallelize='fork')
    elif parallelize == 'mpi':
        if comm.rank == 0:
            print('MPI parallelization is used, rank {}'.format(comm.rank))
        pp_problem = finalized_mpi(pp_builder, capa_matrix, 
                                   parallelize='mpi', comm=comm)
        
    elif parallelize == 'original':
        print('Original pescado parallelization is used (no volume parallelization)')
        pp_problem = pp_builder.finalized(capa_matrix, parallelize=True)
    else:
        print('No parallelization is used')
        pp_problem = pp_builder.finalized(capa_matrix, parallelize=False)
    return pp_problem

def volume_mpi(mesh_inst, indices, comm):
    """
    Returns correctly on all ranks
    """
    
    # Split the mesh points in the amount of ranks we have and obtain the job
    # for a specific rank by indicing
    points_idx = np.array_split(np.arange(0, mesh_inst.npoints), 
                                comm.size)[comm.rank]
    
    # Do the job
    volume = mesh_inst._voronoi_property(indices=points_idx, _property='volume')
    
    volume = comm.allgather(volume)
    volume = np.concatenate(volume)
        
    return volume


def voronoi_property(indices, mesh_inst, _property, neigs=None):
    mesh_inst = Mesh_copy(mesh_inst.sub_meshes, mesh_inst.neighbours)
    return mesh_inst._voronoi_property(indices, _property, neigs)


def volume_fork(mesh_inst, indices):
    # Identical behaviour in linux or windows
    npros = os.cpu_count()
        
    with multiprocessing.get_context().Pool(processes=npros) as pool:

        volume = np.concatenate(pool.map(
            partial(
                voronoi_property,
                mesh_inst=mesh_inst,
                _property='volume', 
                neigs=None),
            np.split(
                np.arange(0, mesh_inst.npoints),
                np.arange(
                    mesh_inst.npoints // npros,
                    mesh_inst.npoints,
                    mesh_inst.npoints // npros)))) 
    
    return volume


@timing
def volume(mesh_inst, indices, parallelize=False, comm=None):
    ''' Returns the volume betwen 'point' and 'neig'

    Parameters
    -----------

    indices: interger or sequence n of interger

        Index of the mesh point from which to find the volume

    neigs: list of interger or list of n lists of interger - default None

        Index of the fneig of 'indices' from which to find
        the separating volume.

        If indices is a int -> neig is a list of integers
        If indices is a list -> neig is a list of list of integers

        Default behavior is to find all separating surfaces. The volume
        indexing uses the same order as the one in 'self.neighbours'

    Returns
    -------

    list or list of n floats
    '''
    if parallelize == 'fork':
        return volume_fork(mesh_inst, indices)
    elif parallelize == 'mpi':
        return volume_mpi(mesh_inst, indices, comm)
    else:
        return np.array(mesh_inst._voronoi_property(
            indices=indices, _property='volume'))


class PoissonProblem_copy(PoissonProblem):
    
    def __init__(self, 
        capacitance_matrix, regions, regions_shapes,
        kwant_indices, mesh_inst, parallelize=False, comm=None):
        
        ''' Returns an instance of pescado.poisson.PoissonProblem

        Parameters
        -----------

        capacitance_matrix: scipy.sparse matrix

        regions: dictionary
            key: region type ('dirichlet', 'neumann', 'helmholtz', 'flexible')
            val: list of strings with the sub_region names

        regions_shapes: dictionary
            key: sub_region name (from regions.values())
            val: sub_region shape

        kwant_indices: dicionary
            key: instance of kwant.Sites
            val: mesh index for the kwant site

        self.mesh_inst: instance of pescado.mesher.mesh.Mesh

        parallelize: bool
            If True parallelizes the linear problem construction

        Returns
        -------

        instance of pescado.problem.PoissonProblem

        TODO: Discuss with Xavier wether precalculating the volume
        is a good idea.
        
        Only returned correctly on rank 0!
        '''
        if comm is None or comm.rank == 0: 
    
            self.capacitance_matrix = capacitance_matrix
            self.regions = regions
            
            # This object can't be pickled, can only be done on rank 0
            self.regions_shapes = regions_shapes
    
            self._kwant_indices = kwant_indices
    
            self.mesh_inst = mesh_inst
    
            regions_index = self._arrange_region_index()
        
            for num, idx in enumerate(regions_index):
                not_num = np.setdiff1d(np.arange(3), (num,))
                for ot_num, ot_idx in enumerate(regions_index[not_num]):
                    rep = np.intersect1d(idx, ot_idx)
                    if len(rep) > 0:
                        raise RuntimeError(
                            'Sites {0} were set to both {1} and {2} types'.format(
                                rep, num, not_num[ot_num]))
    
            (self.dirichlet_indices, self.neumann_indices, self.helmholtz_indices,
             self.flexible_indices) = regions_index

        #######################################################################

        # Center of the parallelization

        # Pre calculate to save time
        self.volume = volume(mesh_inst, indices=np.arange(mesh_inst.npoints), 
                             parallelize=parallelize, comm=comm)
        
        #######################################################################
        
        if comm is None or comm.rank == 0:
            print('Prepare LP')

            self.linear_problem_inst = self._initialize_LP(
                regions_index, parallelize='bullshit_parameter')
            
            ('Done')
            


class Mesh_copy(mesher.mesh.Mesh):
    """
    Copy of the mesh class with the adaption that the decorator @int_or_sequence
    is removed, this gave problems with pickling since the decorator contains
    and extra definition wrapper within its definition.
    """
    
    def sub_mesh_number(self, indices):
        ''' Return the sub meshes number to which the index in 'indices' belong to

        Parameters
        -----------
        indices: sequence of integers
            The index of the meshes points

        Returns
        --------
        numpy.array of integers with shape(m, )
            with m = len(indices)

        '''

        indices = np.asarray(indices)
        sorting_map = np.argsort(indices)

        meshes_num = np.searchsorted(
            self.sub_meshes_cumul_size - 1, indices[sorting_map]) - 1
        return meshes_num[np.argsort(sorting_map)]


    def tag(self, indices, return_pattern=True):
        ''' Return a the (tag, pattern) tuple associated with a given index

        Parameters
        -----------
        indices: sequence n of integers
            The index of the meshes points

        return_pattern: boolean, default True
            If True also returns which pattern the index belongs to

        Returns
        --------
        list of of n tuples with 2 elements:
            1st: tag in the sub mesh
            2nd: sub mesh pattern
        '''

        indices = np.asarray(indices)
        meshes_num = self.sub_mesh_number(indices)

        tag_list = np.empty((len(indices),), dtype=object)
        # TODO: Implement this more efficiently
        for i, ele in enumerate(meshes_num):
            pts = (indices[i] - (self.sub_meshes_cumul_size[ele]))

            info = self.sub_meshes[ele][0][pts]

            if return_pattern:
                info = (info, self.sub_meshes[ele][1])

            tag_list[i] = info

        return tag_list


    def coordinates(self, indices=None):
        ''' Return a the real space coordinates for a given index

        Parameters
        -----------
            indices: sequence of integers
            The index of the meshes points
        '''
        indices = np.asarray(indices)
        meshes_num = self.sub_mesh_number(indices)
        coordinates = np.empty((len(indices), self.dim), dtype=float)

        # TODO: Implement this more efficiently
        unique_ele = np.unique(meshes_num)

        for ele in unique_ele:
            pts = indices[meshes_num == ele] - (self.sub_meshes_cumul_size[ele])
            coordinates[meshes_num == ele, :] = self.sub_meshes[ele][1](
                self.sub_meshes[ele][0][pts])

        return coordinates


    def _voronoi_property(self, indices, _property, neigs=None):
        ''' Internal function to recover a information about
        the local voronoi cell of 'indices'

        Parameters
        -----------

        indices: interger or sequence n of interger

            Index of the mesh point from which to find the surface

        _property: str
            'surface', 'distance', 'riridgedges', 'volume'

        neigs: list of interger or list of n lists of interger - default None

            Index of the fneig of 'indices' from which to find
            the separating surface.

            If indices is a int -> neig is a list of integers
            If indices is a list -> neig is a list of list of integers

            Default behavior is to find all separating surfaces. The surface
            indexing uses the same order as the one in 'self.neighbours'

        Returns
        -------

        list or list of n floats

        '''
        
        vor_cell, neigs_list = self.voronoi(indices=indices)
        values = [[] for i in range(len(vor_cell))]

        for i, (cell, neig_idx) in enumerate(zip(vor_cell, neigs_list)):

            # Always in the same order as self.neighbours[index]
            if neigs is None:
                order_neig = np.arange(len(neig_idx))
            else:
                order_neig = neig_idx.searchsorted(np.sort(neigs[i]))

                if len(order_neig) != len(neigs[i]):
                    wrong_neig = np.setdiff1d(
                        neigs[i], neig_idx)
                    raise ValueError(
                        '{0} do not share a surface with {1}'.format(
                            wrong_neig, indices[i]))

            if _property == 'volume':
                values[i] = cell.volume
            elif _property == 'surface':
                values[i] = cell.surface(neig_idx=order_neig)
            elif _property == 'distance':
                values[i] = cell.distance(neig_idx=order_neig)
            elif _property == 'ridge':
                values[i] = [
                    cell.vertices[cell.ridges[neig]]
                    for neig in order_neig]
            else:
                raise ValueError('Can"t find property {0}'.format(
                    _property))

        return values


    def _pattern_voronoi(self, indices):
        ''' Return the local voronoi cell given by the pattern
        for each index in 'indices'.

        Parameters
        -----------

        indices: interger or sequence n of interger

        Returns
        --------
        instace of mesher.voronoi.VoronoiCell or a sequence of
        such instances
        '''

        vor_list = list()

        for index in indices:
            sub_mesh_number = self.sub_mesh_number([index, ])[0]
            pat = self.sub_meshes[sub_mesh_number][1]
            vor_list.append(
                pat.voronoi(self.sub_meshes[sub_mesh_number][0][
                    index - (self.sub_meshes_cumul_size[sub_mesh_number])]))

        return vor_list

    
    def _voronoi(self, indices):
        ''' Return the local voronoi cell with the neighbour indices
        updated to match the global mesh indexing

        Paramters
        ----------

        indices: interger or sequence n of interger

            Index of the mesh point from which to find the voronoi cell

        Returns
        -------

        list or list of n instances of pescado.mesher.voronoi.VoronoiCell
        '''

        vor_cell_list = [[] for i in range(len(indices))]
        pat_vor_cell_list = self._pattern_voronoi(indices=indices)

        for i, index in enumerate(indices):

            vor_cell = pat_vor_cell_list[i]

            # neig ordering and index updating
            (vor_cell.neighbours_tags, vor_cell.neighbours,
             vor_cell.ridges) = self._update_voronoi_neighbour(
                 index=index, vor_cell=vor_cell)

            # Update index
            vor_cell.point_tag = index
            vor_cell_list[i] = copy.deepcopy(vor_cell)

        del pat_vor_cell_list
        return vor_cell_list


    def voronoi(self, indices, return_neig=True):
        ''' Return a the real space coordinates for a given index

        Paramters
        ----------

        indices: interger or sequence n of interger

            Index of the mesh point from which to find the voronoi cell

        return_neig = boolean (optional) - default is False
            If True returns the neighbours index for the voronoi cell

        Returns
        -------

        vor_cell_list: list or list of n instances of pescado.mesher.voronoi.VoronoiCell

        -- if return_neig is True, then also returns :

        neig_list : np.ndarray or list of np.ndarray of floats with shape(n, )
                with n the number of first neighbours for the corresponding
                voronoi cell in the list.
        '''

        vor_cell_list = [[] for i in range(len(indices))]
        neig_list = [[] for i in range(len(indices))]

        for i, vor_cell in enumerate(self._voronoi(indices=indices)):
            neig_list[i] = vor_cell.neighbours_tags

            del vor_cell.neighbours_tags
            del vor_cell.point_tag

            vor_cell_list[i] = vor_cell

        if return_neig:
            return vor_cell_list, neig_list
        else:
            return vor_cell_list