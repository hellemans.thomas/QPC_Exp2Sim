#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun 16 13:39:23 2022

@author: thellemans
"""

import itertools
import numpy as np
import time
import copy 

from pescado import self_consistent
from pescado.tools import SparseVector, DiscreteIldos

from .timing import timing
from .newton_raphson import NewtonRaphson


class ReducedPoissonProblem():
    """
    Lowers the memory usage when the object has to be broadcasted to
    different ranks. 
    
    On top of that it also allows to use pickle instead of dill to communicate
    between ranks. (you can't pickle a pp_problem)
    """
    
    def __init__(self, pp_problem): 
        # self.linear_problem_inst.regions_index is an array with on index:
        # 0: all the dirichlet indices
        # 1: all the neumann indices
        # 2: all the helmholtz indices
        # 3: all the flexible indices
        
        names = set(itertools.chain.from_iterable(pp_problem.regions.values())) - {'default'}
        self.sub_region_index = {name: pp_problem.sub_region_index(name=name).astype(int) for name in names}

        self.linear_problem_inst = pp_problem.linear_problem_inst
        self.volume = pp_problem.volume
        
        self.regions = copy.deepcopy(pp_problem.regions)
        
        # Remove the default region, not of interest to us
        self.regions['neumann'] = self.regions['neumann'][:-1]
                
        # Add for flexible regions another sub region to put depleted sites in
        for region in pp_problem.regions['flexible']:
            self.regions['flexible'].append(region + '_depleted')
            self.sub_region_index[region + '_depleted'] = np.array([], dtype=int)
            

    def sparse_vector(self, val, name, dtype=None, density_to_charge=False):
        """
        Copied from pescado with small adaptions, see documentation pescado
        e.g. no possibility to use a function as argument for 'val'
        """
        indices = self.sub_region_index[name]
                
        if isinstance(val, (float, int, np.float, np.integer)):
            values = np.ones((len(indices), ), dtype=dtype) * val
        elif isinstance(val, np.ndarray):
            values = val
        else:
            raise
        
        if density_to_charge:
            values = values * self.volume[indices]
        
        return SparseVector(indices=indices, values=values, dtype=dtype)
    

    def read_sparse_vector(
        self, sparse_vector_inst, name=None, return_indices=False):
        """
        Copied from pescado with small adaptions, see documentation pescado
        """

        indices = self.sub_region_index[name]
        values = sparse_vector_inst[indices]
        
        if return_indices:
            return values, indices
        else:
            return values
        
        
    def assign_flexible(
        self, neumann_index=None, dirichlet_index=None, helmholtz_index=None,
        contain_all=False):
        ''' 
        Copied from pescado, see documentation pescado
        '''
    
        self.linear_problem_inst.assign_flexible(
            neumann_index=neumann_index,
            dirichlet_index=dirichlet_index,
            helmholtz_index=helmholtz_index,
            contain_all=contain_all)
        
        
    def freeze(self):
        '''
        Copied from pescado, see documentation pescado
        '''
        self.linear_problem_inst.build()
        
    
    def solve(
        self, voltage, charge_electrons=None, charge_density=None,
        helmholtz_density=None, method='mumps', **kwargs):
        ''' 
        Copied from pescado, see documentation pescado
        '''
        
        return self.linear_problem_inst.solve(
            voltage=voltage, charge_electrons=charge_electrons,
            charge_density=charge_density, helmholtz_density=helmholtz_density,
            method=method, **kwargs)
        
        
    def reset(self):
        ''' 
        Copied from pescado with adaption that the flexible sites are grouped 
        again in the non depleted region. 
        '''
        self.linear_problem_inst = self._initialize_LP(
                self.linear_problem_inst.regions_index)
        
        for region in self.regions['flexible']:
            if '_depleted' not in region:
                
                self.sub_region_index[region] = np.concatenate([
                        self.sub_region_index[region], 
                        self.sub_region_index[region + '_depleted']])
                
                self.sub_region_index[region + '_depleted'] = np.array([])
        
    
    def format_input(func):
        """
        Decorator that allows to format the input of the solver functions, 
        In this case voltage, charge electrons, charge_density and 
        helmholtz_coeff are changed from dictionaries to sparse vectors
        
        To avoid this, we could also use it as a function instead of decorator
        in the beginning of each solver.
        """
        def wrapper(self, **kwargs):
            if ('voltage' in kwargs.keys() and
                isinstance(kwargs['voltage'], dict)):
                kwargs['voltage'] = self._starting_values(
                        kwargs['voltage'])
            
            if ('charge_electrons' in kwargs.keys() and
                isinstance(kwargs['charge_electrons'], dict)):
                kwargs['charge_electrons'] = self._starting_values(
                        kwargs['charge_electrons'])
            
            if ('charge_density' in kwargs.keys() and
                isinstance(kwargs['charge_density'], dict)):
                kwargs['charge_density'] = self._starting_values(
                        kwargs['charge_density'])
            
            if ('helmholtz_coef' in kwargs.keys() and
                isinstance(kwargs['helmholtz_coef'], dict)):
                kwargs['helmholtz_coef'] = self._starting_values(
                        kwargs['helmholtz_coef'])
            
            return func(self, **kwargs)
        return wrapper
    

    def _starting_values(self, initial_val={}, dtype=None):
        """
        Function that allows to give the input as dictionaries with as keys
        regions in the poisson problem. As values there should be given an 
        int/float or array with the same number of elements as the number of
        sites in the region. 
        
        Returns a sparse vector
        """
        initial_val_sv = SparseVector(
                indices=np.array([], dtype=int), values=np.array([]), 
                dtype=dtype)

        for index, (region, val) in enumerate(initial_val.items()):
            # We suppose that val is a float, int or sparse_vector
            if isinstance(val, (float, int, np.ndarray, np.int32, np.int64)):                
                sv =self.sparse_vector(val=val, name=region, dtype=dtype)
            else:
                sv = val
                
            initial_val_sv.extend(sv)
        
        return initial_val_sv
    
    
    def _charge_lhh(self, voltage_res, charge_res, helmholtz_coef):
        """
        helmholtz_dens: dict with as keys the names of regions
        voltage_res and charge_res are sparse vectors
        """
        charge_res = charge_res + SparseVector(
            values = (voltage_res[helmholtz_coef.indices] 
                      * helmholtz_coef.values), 
            indices = helmholtz_coef.indices, 
            default=0)

        return charge_res
    
    
    @format_input
    @timing
    def solve_lhh(self, voltage, charge_electrons=None, charge_density=None,
                  helmholtz_coef=None):
        """
        voltage charge_electrons, charge_density and helmholtz_sv should be
        dictionaries with for every region a voltage/charge/helmholtz value 
        or array.
        Default is Neumann, charge=0 like in pescado.
        
        If the given number is a cst, we create a sparse vector from it for the
        given region
        
        Include assertion that all regions have a necessary boundary condition
        
        HELMHOLTZ COEF should be given negative for corresponding potential 
        results with the solve_sc function. Afterwards also the resulting 
        charge has to be multiplied by -1 for corresponding results.
        """
        # Solve the lhh problem        
        voltage_res, charge_res = self.solve(voltage=voltage, 
                                        charge_electrons=charge_electrons,
                                        charge_density=charge_density,
                                        helmholtz_density=helmholtz_coef)
        
        # For the helmholz indices (2DEG) we still need to do a small adaption 
        # on the charge
        charge_res = self._charge_lhh(voltage_res, charge_res, helmholtz_coef)
                
        return voltage_res, charge_res
    

    @format_input    
    @timing
    def solve_sc_piecewise(self, voltage, charge_electrons=None, 
        charge_density=None, helmholtz_coef=None, configuration=None, 
        ildos=None, helmholtz_cutoff=None, sites_ildos=None, 
        tolerance=None):
        """
        Solves the poisson problem for a piecewise ildos
        
        helmholtz_coef is used for the helmholtz sites
        disc_ildos is used for the flexible sites 
        """        
        sp_problem = SchrodingerPoisson_copy(
                ildos=ildos, 
                poisson_problem_inst=self, 
                helmholtz_cutoff=helmholtz_cutoff, 
                sites_ildos=sites_ildos, 
                tolerance=tolerance)
    
        poisson_problem_input = {'voltage': voltage, 
                                 'charge_electrons': charge_electrons, 
                                 'charge_density': charge_density, 
                                 'helmholtz_density': helmholtz_coef}
        
        # Setting the default configuration to the first branch
        if not isinstance(configuration, SparseVector):
            if configuration == {}:
                for region in self.regions['flexible']:
                    configuration[region] = 1
            configuration_sv = self._starting_values(configuration, dtype=int)
        
        sp_problem.initialize(initial_guess=configuration_sv,
                              poisson_problem_input=poisson_problem_input, 
                              return_poisson_output=False)
        
        sp_problem.iterate(return_poisson_output=False)
        
        if not isinstance(sp_problem.ildos_list[0], DiscreteIldos):
            # check if there are still sites changing type (Neumann <-> Dirichlet),
            # if so keep on iterating
            while np.any(
                np.abs(sp_problem.iteration_data[-1]['interval']
                    - sp_problem.iteration_data[-2]['interval']) != 0):
        
                sp_problem.iterate(return_poisson_output=False)
        else:
            sp_problem._iterate_sc_piecewise()
    
        return (sp_problem.potential(iteration=-1), 
                sp_problem.charge(iteration=-1))
        
        
    @format_input        
    @timing
    def solve_sc_continuous(self, voltage, charge_electrons=None, 
        charge_density=None, helmholtz_coef=None, initial_potential=None, 
        ildos=None, ildos_dos=None, quantum_functional=None, sites_ildos=None, 
        potential_precision=1e-6):
        """
        Solves the poisson problem for a continuous ldos using Newton Raphson
        
        Question for antonio, do you store all the iteration data? Or just
        the last few iterations --> consumes a lot of memory for larger systems
        """    
        # Initialise a NaiveSchrodingerPoisson problem which can be solved 
        # using Newton Raphson
        nr_problem = NewtonRaphson(
            ildos = ildos,
            ildos_dos = ildos_dos,
            quantum_functional = quantum_functional,
            poisson_problem_inst=self, 
            sites_ildos=sites_ildos, 
            potential_precision=potential_precision)
    
        poisson_problem_input = {'voltage': voltage, 
                                 'charge_electrons': charge_electrons, 
                                 'charge_density': charge_density, 
                                 'helmholtz_density': helmholtz_coef}
        
        if not isinstance(initial_potential, SparseVector):
            if initial_potential == {}:
                for region in self.regions['flexible']:
                    initial_potential[region] = 0
            initial_potential = self._starting_values(initial_potential)
        
        # Doing the initialisation already does a first iteration
        nr_problem.initialize(poisson_problem_input = poisson_problem_input,
                              initial_potential = initial_potential, 
                              return_poisson_output=False)
        
        # Now we do a second iteration
        start = time.time()
        _, (voltage_res, charge_res), _ = nr_problem.iterate(
                                                    return_iteration_data=True)
        end = time.time()
        print('Iteration 2 took {} s'.format(end-start))
        
        # Based on the previous 2 iterations we can estimate the error
        # and check for convergence
        iteration = 3
        while not nr_problem.convergence(verbose=False):
            start = time.time()
            _ , (voltage_res, charge_res), _ = nr_problem.iterate(
                                                    return_iteration_data=True)
            end = time.time()
            print('Iteration {} took {} s'.format(iteration, end-start))
            iteration += 1
        
        # Filtering the result for the flexible indices out of the complete 
        # poisson results.¸
        flexible_index = self.linear_problem_inst.regions_index[3]
        voltage_res = SparseVector(indices = flexible_index, 
                                   values = voltage_res[flexible_index]) 
        
        # We get the charge in which is taken care of the helmholtz subtype 
        # flexible indices (in terms of conventions)
        charge_res_scaled = nr_problem.charge(iteration = -1)
        
        # We extend this charge with the possible values for flexible sites
        # that don't have the helmholtz subtype.
        charge_res_scaled.extend(SparseVector(indices = flexible_index, 
                                          values = charge_res[flexible_index]))
        return voltage_res, charge_res_scaled
    
    
    def filter_flexible(self, density, ldos, threshold_ldos):
        """
        Split the flexible regions into a depleted region which can be fixed 
        to neumann and a non depleted region which stays flexible.
        
        There are four options:
            
            When a site is set to neumann:
                    
                1) It stays neumann if the ldos < threshold_ldos
                2) It becomes flexible if the ldos > threshold_ldos
            
            When a site is set to flexible:
                
                3) It stays flexible if the dens > 0
                4) It becomes neumann if the dens <= 0
        
        density: sparse vector
            Defined for all the flexible sites
            
        ldos: sparse vector
            Defined for all the flexible sites
            
        threshold_ldos: sparse vector
            Defined for all the flexible sites
            Threshold for filtering Neumann sites (see explanation above)
            Should be in the same units as 'ldos'!
        
        """
        for region in self.regions['flexible']:
            if '_depleted' not in region:
                
                # Handling option 1 and 2
                index_neu = self.sub_region_index[region + '_depleted']
                boolean = ldos[index_neu] <= threshold_ldos[index_neu]
                
                index_neu_neu = index_neu[boolean]
                index_neu_flex = index_neu[np.logical_not(boolean)]
                
                # Handling option 3 and 4
                index_flex = self.sub_region_index[region]
                boolean = density[index_flex] > 0

                index_flex_flex = index_flex[boolean]
                index_flex_neu = index_flex[np.logical_not(boolean)]
                
                # Combining the results
                index_neu = np.sort(np.concatenate([index_neu_neu, 
                                                   index_flex_neu]))
                index_flex = np.sort(np.concatenate([index_neu_flex, 
                                                     index_flex_flex]))
    
                self.sub_region_index[region + '_depleted'] = index_neu
                self.sub_region_index[region] = index_flex
                
#class DiscreteIldos_copy(DiscreteIldos):
    
class SchrodingerPoisson_copy(self_consistent.SchrodingerPoisson):
    """
    This class is based on the SchrodingerPoisson class in Pescado.
    
    All functions from the SchrodingerPoisson object are still operable 
    for the SchrodingerPoisson_copy object. 
    
    The following extension is made:
    1)  _iterate_sc_piecewise()
        *   New function.
        *   Performs iterations with updates of the ildos as long as there is 
            no convergence of the results. Based on the tests written for the 
            SchrodingerPoisson object.
    """
                        
    def _iterate_sc_piecewise(sp_problem):
        """
        Helper function for solve_sc_piecewise.
        
        Checks the convergence of the solution and otherwise keeps on iterating.
        """        
        old_func_pts_list = list()
    
        for ildos in sp_problem.ildos_list:
            old_func_pts_list.append(ildos.functional_points)
    
        converged = False
    
        while (not converged):
            while np.any(
                np.abs(sp_problem.iteration_data[-1]['interval']
                       - sp_problem.iteration_data[-2]['interval']) != 0):
    
                sp_problem.iterate(return_poisson_output=False)
    
            print('update ildos')
            sp_problem.update_ildos()
            sp_problem.iterate(return_poisson_output=False)
    
            new_func_pts_list = list()
            converged = True
            for ildos, old_func_pts in zip(sp_problem.ildos_list, old_func_pts_list):
                new_func_pts_list.append(ildos.functional_points)
    
                if (not np.array_equal(old_func_pts, ildos.functional_points)):
                    converged = False
    
            old_func_pts_list = new_func_pts_list