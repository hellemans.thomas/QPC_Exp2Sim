# -*- coding: utf-8 -*-
"""
Created on Sun May 22 19:41:05 2022

@author: helle
"""

import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
from pescado import tools
from pescado.mesher import shapes
from mpl_toolkits.axes_grid1 import make_axes_locatable
from mpl_toolkits.mplot3d import Axes3D # This import is necessary!
import warnings 

def cmap_mesh(x,y):
    """
    Only for rectangular meshes
    """
    def cmap_points(data):
        data = np.unique(data)
        diff = np.diff(data)
        X = data - np.insert(diff, 0, diff[0])/2
        X = np.append(X, data[-1]+diff[-1]/2)
        return X
    return np.meshgrid(cmap_points(x), cmap_points(y))


def cmap_data(coord, data, coord_reconstruct=None):
    """
    Helper function for matplotlib.pyplot.imshow.
    
    From an array of coordinates and corresponding array of data we reconstruct
    a data grid that can be plotted by matplotlib.pyplot.imshow.
    
    coord: np.ndarray
        shape (m, n) with m the number of coordinates and n the dimension of 
        the coordinates
        
    data: np.ndarray
        shape (m,)
        
    coord_reconstruct: np.ndarray
        superset of coord, also including possible sites for which the data
        is not calculated. It's not always necessary to provide this but it 
        avoids mistakes in the plotting when 
    """        
    if coord_reconstruct is None:
        coord_reconstruct = coord
    
    X = np.unique(coord_reconstruct[:,0])
    Y = np.unique(coord_reconstruct[:,1])
    X_mesh, Y_mesh = np.meshgrid(X, Y)
    mesh_shape = X_mesh.shape

    lookup_dict = dict(zip([tuple(elem) for elem in coord], data.ravel()))
    data_map = np.array(list(map(lambda xx, yy : lookup_dict[(xx,yy)]
                                 if (xx, yy) in lookup_dict.keys() else np.NaN, 
                                 X_mesh.ravel(), Y_mesh.ravel())))
    data_map = data_map.reshape(mesh_shape)
    
    extent = ((3*X[0]-X[1])/2, (3*X[-1] - X[-2])/2, 
              (3*Y[0]-Y[1])/2, (3*Y[-1] - Y[-2])/2)
    
    return data_map, extent

def shape(shapes_list, colors, grid_cell=10, coord=None, section=None, 
          aspect_equal=True, figsize=[6.4, 4.8], legend_label=None, ax=None,
          ax_labels=None, imshow=True, interpolation=None):
    
    """
    shapes_list: list
        Objects within the list should be of type pescado.mesher.Shapes
        
    colors: list
        Same length as shapes_list, contains matplotlib colors in which the 
        corresponding shapes should be plotted
        
    grid_cell: list or float
        Grid which is used to plot the shapes. 
        When an int is provided, this is the size of a square grid cell
        When a list is provided the list should have the same length as the 
        dimension of the plot or the dimension of the shapes.
        
    coord: np.ndarray
        Overrules grid_cell. This is an array of the coordinates that will 
        be used for the plot. e.g. obtained by 
        pp_problem.mesh_inst.coordinates()
        
    section: list
        Same length as the number of dimensions of the shapes. 
        For each dimension it should be specified whether a cross section has
        to be made (float specified) or whether this dimension should be kept 
        for the final plot (None specified). 
        
        e.g. For a 3D shape: when section = [None, None, 10], one will obtain 
        a 2D plot ifo x and y (first 2 dimensions), while the z coordinate 
        (3rd dimension) is fixed to 10.
        
    aspect_equal: Boolean
        Whether the aspect ratio should be equal or not, only possible for 
        2D plots
        
    figsize: list
        size of the figure, see matplotlib
    
    legend_label: list
        Same length as shapes_list, contains strings with the names of the 
        shapes
    
    ax: matplotlib.axes
        Existing ax on which the plot has to be made
        
    ax_labels: list
    
    imshow: Boolean
        Whether imshow should be used for the plot or plt.scatter
    """
    
    from .extension_pes import cross_section
    
    dim_shape = len(shapes_list[0].bbox[0])
    
    assert np.all([len(shape.bbox[0]) == dim_shape for shape in shapes_list]), \
     'All shapes must have the same dimension.'
    
    assert len(shapes_list) == len(colors),\
     'Not all shapes have a specified color'

    assert section is None or len(section) == dim_shape,\
     'The section needs to be None (for no cross section) or needs to have' +\
         ' as many elements as the dimension of the shapes'


    def create_grid(shapes_list, grid_cell, section, dim_plot, dim_shape):
        # Setting the grid cell
        assert isinstance(grid_cell, (int, float)) or \
         len(grid_cell) == dim_plot or len(grid_cell)==dim_shape, \
            'The length of the grid_cell must be equal to the dimension of '+\
            'the plot, the dimension of the shapes or be a float/int'
        
        if isinstance(grid_cell, (int, float)):
            grid_cell = [grid_cell] * dim_plot
        elif len(grid_cell) == dim_shape and dim_plot != dim_shape:
            grid_cell = np.delete(grid_cell, [i for i, val in enumerate(section) if val is not None])             

        # Creating the grid
        grid_bbox = shapes.add_bbox([shape.bbox for shape in shapes_list])
        coord = tools.meshing.grid(bbox=grid_bbox, step=grid_cell, center=[0]*dim_plot)
        return coord
    
    # Check if the provided section matches with the dimension and make a 
    # cross section of the coordinates if necessary
    if section is None or section == [None]*dim_shape:
        # No cross section has to be made, coord remains coord
        section = [None] * dim_shape
        dim_plot = dim_shape
        if coord is None:
            coord = create_grid(shapes_list, grid_cell, section, dim_plot, 
                                dim_shape)
    else:
        # A cross section has to be made
        dim_plot = section.count(None)
        
        shapes_list = [cross_section(shape, section) for shape in shapes_list]
        
        # If the coordinates are provided we use these points, otherwise 
        # we create a grid ourselves
        if coord is None:
            coord = create_grid(shapes_list, grid_cell, section, dim_plot, 
                                dim_shape)
        else:
            boolean = np.array([coord[:,i] == val for i, val in 
                                enumerate(section) if val is not None])
            index = np.where(np.all(boolean, axis=0))[0]
            coord = np.delete(coord, [i for i, val in enumerate(section) 
                                      if val is not None], 1) 
            coord = coord[index]
    
    # Check if the provided ax_labels match with the dimension
    if ax_labels is True:
        ax_labels = ['x (nm)', 'y (nm)', 'z (nm)'][:len(section)]
        ax_labels = [ax_label for i, ax_label in enumerate(ax_labels) 
                     if section[i] is None]
    elif ax_labels is None:
        ax_labels = [None]*dim_plot
    else:
        assert len(ax_labels) == dim_plot, \
        'If the "ax_labels" are provided manually, they should have the same'+\
            ' length as the dimension of the plot'
    
    # pcolormesh only supported in 2 dimensions for now
    if imshow and dim_plot == 2:
        shape_imshow(ax, figsize, ax_labels, shapes_list, colors, 
                         coord, aspect_equal, legend_label, interpolation)
    else:
        shape_scatter(dim_plot, ax, figsize, ax_labels, shapes_list, 
                      colors, coord, aspect_equal, legend_label)


def shape_scatter(dim_plot, ax, figsize, ax_labels, shapes_list, colors, 
                  coord, aspect_equal, legend_label):
    # Initialise the plot
    if dim_plot == 1:
        if ax is None:
            figsize = [figsize,1] if isinstance(figsize, (int,float)) else [figsize[0], 1]
            plt.figure(figsize=figsize)
            ax = plt.axes()
        ax.set_xlabel(ax_labels[0])
        ax.set_yticklabels([])
        for shape, color in zip(shapes_list, colors):
            index = shape(coord)
            ax.scatter(coord[index, 0],
                       np.zeros(len(coord[index, 0])), marker='.', c=color)
    elif dim_plot == 2:
        if ax is None:
            figsize = [figsize]*2 if isinstance(figsize, (int,float)) else figsize
            plt.figure(figsize=figsize)
            ax = plt.axes()
        ax.set_xlabel(ax_labels[0])
        ax.set_ylabel(ax_labels[1])
        # Setting the aspect ratio of the plot equal or not
        if aspect_equal:
            ax.set_aspect('equal', adjustable='box')
        for shape, color in zip(shapes_list, colors):
            index = shape(coord)
            ax.scatter(*coord[index].transpose(), marker='.', c=color)
    elif dim_plot == 3:
        if ax is None:
            figsize = [figsize]*2 if isinstance(figsize,(int,float)) else figsize
            plt.figure(figsize=figsize)
            ax = plt.axes(projection="3d")
        ax.set_xlabel(ax_labels[0])
        ax.set_ylabel(ax_labels[1])
        ax.set_zlabel(ax_labels[2])
        for shape, color in zip(shapes_list, colors):
            index = shape(coord)
            ax.scatter(*coord[index].transpose(), marker='.', c=color) 
        
    if legend_label is not None:
        ax.legend(legend_label)
        
            
def shape_imshow(ax, figsize, ax_labels, shapes_list, colors, 
                     coord, aspect_equal, legend_label, interpolation):
    """
    
    """
    # The expression below maps the coordinates on an index value corresponding
    # to the shape to which the coordinate belongs to. This doesn't work for
    # overlapping shapes!!
    
    if ax is None:
        figsize = [figsize]*2 if isinstance(figsize, (int,float)) else figsize
        fig, ax = plt.subplots(figsize=figsize)
    
    if 'k' in colors or 'black' in colors:
        warnings.warn('Black (k) is also the color used for overlapping shapes')
    
    data = np.sum([shape(coord)*(index+1) for index, shape in 
                   enumerate(shapes_list)], axis=0)
    
    # Creating the correct colormap, white for the background, black for overlap
    cmap = mpl.colors.ListedColormap(['w'] + list(colors) + ['k'])
    boundaries = np.arange(len(shapes_list)+3) - 0.5
    norm = mpl.colors.BoundaryNorm(boundaries, cmap.N, clip=False)

    data, extent = cmap_data(coord, data)
    ax.imshow(data, origin='lower', interpolation=interpolation, cmap=cmap, 
              norm=norm, extent=extent)

    ax.set_xlabel(ax_labels[0])
    ax.set_ylabel(ax_labels[1])
    
    if aspect_equal:
        ax.set_aspect('equal', adjustable='box')
    
    if legend_label is not None:
        # we create some custom lines that we don't plot to be able to add
        # the legend to these lines
        legend_elements = [mpl.patches.Patch(facecolor=color, edgecolor=color, 
                                             label=legend_label[i])
                           for i, color in enumerate(colors)]
    
        ax.legend(handles=legend_elements)
        
        
def data_cross_section(coord, data, section=None):
    """
    Parameters
    ----------
    coord: np.ndarray
    
    data: SparseVector, list of SparseVector elements
          or list of np.ndarray elements
    
    section: list, same shape as coord[0] or list of lists
    
    Returns
    -------
    coord_filtered: np.ndarray
        shape (m, n, p) with
            m = len(indices) with indices the indices of the coordinates within the section
            n = dimension cross section
            p = len(section_list)
    
    data_filtered: np.ndarray
        shape (m,p) with
            m = len(indices) with indices the indices of the coordinates within the section
            o = len(data_list)
            p = len(section_list)
    
    Note: dimension p at the end since this allows for easy plotting
    
    Check: do a thorough check of the returned dimensions
    
    Mistake: The dimension of coord_filtered is not the same as mentioned, 
             We need to implement the last dimension correctly and correct
             the plotting functions line() and colormap() accordingly.
    """
        
    # If only a single section or single sparse vectored is entered, we put them in a list
    if section is None:
        section = [[None]*len(coord[0])]
    elif isinstance(section[0], (int, float)) or section[0] is None:
        section = [section]
        assert len(coord[0]) == len(section[0])
    if isinstance(data, tools.sparse_vector.SparseVector):
        data = [data]
    
    data_filtered = []
    coord_filtered = []
    
    # Check if the asked sections are valid sections for the provided coordinates
    if not np.all(np.array(section) == None):
        boolean = [np.all(np.any([coord[:,i] == val \
            for i, val in enumerate(section_elem) if val is not None], axis=1)) \
            for section_elem in section]
        assert np.all(boolean), \
        'The following sections can not be found in the coordinate array: \n\
        {}'.format(section[np.logical_not(boolean)])
    
    # Making the asked sections
    for section_elem in section:
        # If all the data is asked (section is completely None)
        if np.all(section_elem == [None]*len(section_elem)):
            index = np.arange(len(coord))
        # If we need to make a real cross section:
        else:
            boolean = np.array([coord[:,i] == val for i, val in enumerate(section_elem) if val is not None])
            index = np.where(np.all(boolean, axis=0))[0]
        # Find the data corresponding to these indices
        data_filtered.append([data_elem[index].astype(float) for data_elem in data])
    
    data_filtered = np.array(data_filtered).transpose()
    
    # Find the coordinates corresponding to these indices
    coord_dim_reduced = np.array([coord[:,i] for i, val in enumerate(section_elem) if val is None]).transpose()
    #np.delete(coord, [i for i, val in enumerate(section_elem) if val is not None], 1) 
    # If we evaluate to a single point in the data set
    if len(coord_dim_reduced) == 0:
        coord_filtered = np.empty((0,0))
    # If there would still be some coordinate depency of the data
    else:
        coord_filtered.append(coord_dim_reduced[index])
        coord_filtered = np.array(coord_filtered)[0]
    
    return coord_filtered, data_filtered


def line(coord, values, cell_area=None, xlabel='', ylabel='', title='',
         ax=None, figsize=[6.4, 4.8], legend_label=None, vline=[]):
    """
    cell_area:
        # np.product(np.array(p.grid_fine[:-1])) for conversion from 
        # charge/cell to charge/nm²
        # 1e18 for charge/nm² to charge/m²
        data = data / np.product(np.array(self.p_geom['grid_fine'][:-1])) * 1e18
    
    legend: 
        list containing the legend 
        
    values:
        np.array with the same shape as would be returned by data_cross_section
        while making multiple cross_sections
    """ 
    # Check the dimensions of the input values    
    values = values.reshape([size for size in values.shape if size != 1])
    
    # Processing input values
    if cell_area is not None:
        if isinstance(cell_area, list):
            cell_area = np.array(cell_area)    
        values = values/cell_area
    
    # Make the plot
    if ax is None:
        figsize = [figsize]*2 if isinstance(figsize, (int,float)) else figsize
        fig, ax = plt.subplots(figsize=figsize)
        
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    ax.set_title(title)
    
    ax.plot(coord, values)
    
    if legend_label is not None:
        ax.legend(legend_label)

    for x in vline:
        ax.axvline(x, color = 'k', linestyle = '--', linewidth =0.5)
    

def colormap(coord, values, cell_area=None, xlabel='', ylabel ='', title='', 
             ax=None, figsize=[6.4, 4.8], aspect_equal=False, imshow=True, 
             cmap = plt.cm.viridis_r, cbar_label='', cbar_range=[None,None], 
             cbar_log=False, interpolation='none', cbar=True):
    """
    cell_area: int, np.ndarray or list
        The area of the voronoi cells perpendicular to the plane of the plot
        expressed in m^2 
        For a 2D simulation this was originally: values = values / (p.grid_fine[0] *  1e-18)  
    
        p.grid_fine[0] * p.grid_fine[1] * 1e-18
    """
    # Check the dimensions of the input values    
    #assert values.shape[0] == coord.shape[0], \
    #    'The data array and coord array should have the same length'
    assert len(values.shape) == 1 or len(values.shape) == 3, \
        'The data should be given as a flat array or in the same format as ' +\
        'returned by plotting_pes.data_cross_section()'
    
    if len(values.shape) == 3:
        assert values.shape[1] == 1, 'Only the data from one sparse_vec can be plotted'
        assert values.shape[2] == 1, 'Only the data from one cross_section can be plotted'
        values = values[:,0,0]

    # Processing input values    
    if cell_area is not None:
        if isinstance(cell_area, list):
            cell_area = np.array(cell_area)    
        values = values/cell_area
    
    # Make the plot
    if ax is None:
        figsize = [figsize]*2 if isinstance(figsize, (int,float)) else figsize
        fig, ax = plt.subplots(figsize=figsize)
    
    # Settings for the limits
    ax.set_xlim((np.min(coord[:,0]), np.max(coord[:,0])))
    ax.set_ylim((np.min(coord[:,1]), np.max(coord[:,1])))
    
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    ax.set_title(title)

    if aspect_equal:
        ax.set_aspect('equal', adjustable='box')
        
    if cbar_range[0] is None:
        val_min = np.min(values) #- max_colorbar
    else:
        val_min = cbar_range[0]
        
    if cbar_range[1] is None:
        val_max = np.max(values)
    else:
        val_max = cbar_range[1]
    abs_max = max(abs(val_min), abs(val_max))
    
    if cbar_log:
        norm = mpl.colors.SymLogNorm(linthresh=abs_max/1e3, linscale=1e-3, 
                                     vmin = - abs_max, vmax = abs_max)
    else:
        norm = mpl.colors.Normalize(vmin=val_min, vmax=val_max)
    
    scalarmappable = plt.cm.ScalarMappable(norm=norm, cmap=cmap)
    scalarmappable.set_array(len(values))

    # Makes sure the colorbar is equally high as the figure
    divider = make_axes_locatable(ax)
    cax = divider.append_axes("right", size="5%", pad=0.05)
    #cax = divider.new_horizontal(size="5%", pad=0.5, pack_start=True)
    #fig.add_axes(cax)
 
    if cbar:
        cbar = plt.colorbar(scalarmappable, cax=cax) #cax=cax extra as argument
        cbar.set_label(cbar_label)
    
    # Plotting of the data:
    if imshow:
        data, extent = cmap_data(coord, values)
        ax.imshow(data, origin='lower', interpolation=interpolation, cmap=cmap, 
                  norm=norm, extent=extent)
    else:
        ax.scatter(coord[:, 0], coord[:, 1], c=cmap(norm(values)), s=10)