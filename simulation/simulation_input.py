# -*- coding: utf-8 -*-
"""
Created on Sat May 21 11:21:56 2022

@author: helle
"""
from mpi4py import MPI
import numpy as np
import scipy.constants as c

from qpc_exp2sim.simulation import builder, solver
from qpc_exp2sim.pescado_kwant import pescado_kwant, extension_pes


comm = MPI.COMM_WORLD

# Setting the device parameters
p_geom = {'grid_fine' : [50, 50, 10],                # Mesh in xyz direction in device (Pescado mesh + Kwant lattice)
          'grid_coarse' : [40, 40, 10],                # Mesh in xyz direction around device (Pescado mesh)
          'section' : [None, None, None],              # None if you don't take a cross section for this ax
          'L_kwant' : 1500,
          'W_kwant' : 1500,
          'L_kpm': 2500,
          'W_kpm': 2500,
          'L_pes' : 1500,                              # Lenth total device in x direction = transport direction
          'W_pes' : 1500,                              # Width total device in y direction = gate direction
          'L_narrow_gate' : 50,                        # Length of the narrow gate 
          'qpc_center' : [0,400],                      # Widthh of the narrow gate
          'qpc_name' : 'b3',
          'd0' : 20,                                   # Thickness GaAs layer bottom
          'd_2DEG' : 10,                               # Thickness 2DEG
          'd1' : 30,                                   # Thickness AlGaAs above 2DEG
          'd2' : 60,                                   # Thickness AlGaAs, Si doped
          'd3' : 10,                                   # Thickness AlGaAs 
          'd4' : 10,                                   # Thickness GaAs
          'd_contact' : 10,                            # Thickness contact layer
          'eps_GaAs' : 12.93,                          # Dielectric constant GaAs
          'eps_AlGaAs' : 11.93,                        # Dielectric constant AlGaAs
          'eps_air' : 1,                               # Dielectric constant air/vacuum (around gates)
          'eps_gates' : 1e4,                           # Dielectric constant gates (metal)
          'effective_mass' : 0.067,                    # Effective mass for 2DEG DOS calculation
          'calibration_device' : False}                # Can not be filtered out because a requirement for the pk_problem is 
                                                       # that the data from the kpm system can be extended towards all flexible sites

p_geom = builder.extend_params(p_geom)

# Configuration
p_sim = {# Boundary conditions
         'V_gates_list' : np.array([-0.002]), 
         'energy_list' : np.array([0]),                    
         'DEG_dens' : 1e13,                # Electron density in 2DEG [/m²]
         'num_moments' : 25, 
         'iterations' : 5,
         'temp' : 0,
    
         # Simulation parameters
         # Setting of the pk_problem solver that will be used: 
         # 'lhh' = linear helmholtz
         # 'pi_bulk' = piecewise bulk 2D system ildos¸
         # 'pi_lat' = piecewise lattice ildos
         # 'nr_lat' = newton raphson lattice ildos
         # 'fsc' = full self consistent
         'setting_solver' : 'fsc_nr',       
         'calibration' : True, 
         'calibration_method' : 'v2',
         'calc_V3' : True, 
         'calc_cond' : True,
         'save_poisson' : True, 
         'save_iteration' : True}

if comm.rank == 0:
    print(p_geom)
    print(p_sim)

pes_builder = builder.make_pescado_system(**p_geom)
pp_problem = extension_pes.finalized(pes_builder, parallelize='mpi', 
                                     comm=comm)

kwant_builder = builder.make_kwant_system(**p_geom)
kpm_builder = builder.make_kpm_system(**p_geom)

conversion_index = pescado_kwant.conversion_index(
        pp_problem, kwant_builder, kpm_builder, extra_dim=[2], 
        coord_extra_dim=[0])

# This creates a pk_problem and writes it to the geometry file
pk_problem = pescado_kwant.PescadoKwantProblem(
        poisson_problem = pp_problem, 
        kwant_builder = kwant_builder, 
        kpm_builder = kpm_builder,
        conversion_unit = c.elementary_charge/p_geom['t'], 
        conversion_index = conversion_index,
        extra_dim = [2], 
        coord_extra_dim = [0])

# Solve the pk_problem
# Data is only returned correctly on rank 0
data = solver.solve(pk_problem, p_geom, p_sim, 
                    file_data = 'files_data/' + 'second_result')

print('pk_problem solved!')