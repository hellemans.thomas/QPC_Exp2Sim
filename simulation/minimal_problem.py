#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul  5 15:02:22 2022

@author: thellemans
"""

# Minimal problem creating a segmentation fault when increasing the number of
# mesh points
from pescado import poisson
from pescado.mesher import shapes, patterns

L =400
W = 400
d1 = 10
d2 = 10
grid1 = [10, 10, 1]
grid2 = [10, 10, 1]


pp_builder = poisson.PoissonProblemBuilder()

# Creating shapes
layer1 = shapes.Box(lower_left=[-L/2, -W/2, 0], 
                    size=[L, W, d1])

layer2 = shapes.Box(lower_left=[-L/2, -W/2, d1], 
                    size=[L, W, d2])

# Creating patterns
pattern1 = patterns.Rectangular.constant(element_size = grid1, 
                                         center = [0, 0, 0.5])

pattern2 = patterns.Rectangular.constant(element_size = grid2, 
                                         center = [0,0, d1+0.5])

# Adding submeshes
pp_builder.add_sub_mesh(shape_inst = layer1, 
                        pattern_inst = pattern1)

pp_builder.add_sub_mesh(shape_inst = layer2, 
                        pattern_inst = pattern2)


inters = shapes.Intersection([layer1, layer2])
#-> you get a plane as intersection, z values in bbox are equal
print(inters.bbox) 

# pp_builder.finalized(parallelize=True)