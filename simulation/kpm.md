---
jupyter:
  jupytext:
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.13.8
  kernelspec:
    display_name: Python 3 (ipykernel)
    language: python
    name: python3
---

# KPM: Attention points and convergence
**Table of contents**

0. Making small kwant system 
1. Oscillations in the LDOS coming from the boarder of the system
2. Convergence ifo the number of moments
3. Comparison of kpm and kwant ldos
4. Analytical integration vs Gauss-Chebyshev integration
5. Non uniform potential
    * Convergence ifo the number of moments
    * Reducing the KPM system
        * Convergence
        * Influence of the threshold


**Note:** Mention in the thesis that we use local vectors approach to obtain a result as exact as possible for every vector

```python
import kwant
import numpy as np
from copy import deepcopy
import matplotlib.pyplot as plt
import matplotlib as mpl
from qpc_exp2sim.pescado_kwant import plotting_pes, pescado_kwant, extension_pes, exact_ldos
import matplotlib.transforms as mtransforms

%matplotlib notebook
```

## 1) Making kwant system and calculations


### 1.0) Input parameters

```python
num_moments = 50
grid = [10, 10]
L = 1000
W = 1000
center = [0, 0]
energy_val = 1
energies = np.linspace(-0.1, 8.1, 300)
```

### 1.1) Making system 

```python
def _make_system(grid, L, W):
    """
    returns a builder object without leads
    """
    # First, define the tight-binding system
    sys = kwant.Builder()
    lat = kwant.lattice.Monatomic(np.diag(grid), norbs=1)
    
    def scat_reg(pos):
        x,y = pos
        return -L/2 <= x <= L/2 and -W/2 <= y <= W/2
        
    ## On site hamiltonian
    sys[lat.shape(scat_reg, [0,0])] = 4

    ## Hopping in x and y direction
    sys[lat.neighbors(1)] = -1

    return sys, (lat, scat_reg)


def make_kwant_system(grid, L_kwant, W_kwant, **kwargs):
    sys, (lat, scat_reg) = _make_system(grid, L_kwant, W_kwant)
    
    # Adding the leads 
    ## Lead on the left    
    sym_left = kwant.TranslationalSymmetry((-grid[0], 0))
    lead_left = kwant.Builder(sym_left)
    lead_left[lat.shape(scat_reg, (0,0))] = 4
    lead_left[lat.neighbors()] = -1
    sys.attach_lead(lead_left)
    
    ## Lead on the right
    sys.attach_lead(lead_left.reversed())
    
    return sys


def make_kpm_system(grid, L_kpm, W_kpm):
    sys, _ = _make_system(grid, L_kpm, W_kpm)
    return sys

builder_kwant = make_kwant_system(grid, L, W)
builder_kpm = make_kpm_system(grid, L, W)

sys_kwant = builder_kwant.finalized()
sys_kpm = builder_kpm.finalized()
# Get the coordinates of the kwant system in a numpy array, convenient for later plotting
coord = np.array([site.pos for site in sys_kwant.sites])
index_p = extension_pes.indices_from_coordinates(coord, [center])[0]
index_e = np.where(np.min(np.abs(energies - energy_val)) == np.abs(energies - energy_val))[0][0]


# Plot the kwant system
fig, (ax1, ax2) = plt.subplots(1,2)
kwant.plot(sys_kwant, ax=ax1)
kwant.plot(sys_kpm, ax=ax2)
ax1.set_aspect('equal')
ax2.set_aspect('equal')
ax1.set_title('kwant system')
ax2.set_title('kpm system')
plt.show()
```

```python
fig, ax = plt.subplots(figsize=(4,4))


#kwant.plot(sys_kwant, ax=ax)
#ax.set_title('Kwant system')

kwant.plot(sys_kpm, ax=ax)
ax.set_title('KPM system')

ax.set_aspect('equal')
ax.set_xticks(list(range(-500, 501, 250)))
ax.set_yticks(list(range(-500, 501, 250)))

# Creating the inset
left, bottom, width, height = [0.55, 0.55, 0.25, 0.25]
ax_inset = fig.add_axes([left, bottom, width, height])

kwant.plot(sys_kpm, ax = ax_inset) 

ax_inset.set_xlim([-205, -125])
ax_inset.set_ylim([-205, -125])

ax_inset.set_xticks([])
ax_inset.set_yticks([])

ax.indicate_inset_zoom(ax_inset, edgecolor="black")

ax.set_aspect('equal')
ax_inset.set_aspect('equal')

plt.tight_layout()
plt.show()
```

### 1.2) Kwant calculation ldos

```python
# Kwant calculation (this might take some time, depending on the input parameters)
ldos_kwant = []
for e in energies:
    # Factor 2 to account for spin degeneracy, in Kwant we only have one orbital per site
    ldos_kwant.append(2*kwant.ldos(sys_kwant, e))
ldos_kwant = np.array(ldos_kwant)
```

### 1.3) KPM calculation ldos

```python
# KPM calculation
where = sys_kpm.sites
vector_factory = kwant.kpm.LocalVectors(sys_kpm, where)

spectral_dens = kwant.kpm.SpectralDensity(sys_kpm, 
                                     num_vectors=None, 
                                     num_moments=num_moments,
                                     vector_factory=vector_factory,
                                     mean=False, 
                                     accumulate_vectors=False)
energies_kpm, ldos_kpm = spectral_dens()
ldos_kpm_fixed_energy = spectral_dens(energy = energy_val)

# Factor 2 to account for spin degeneracy, in Kwant we only have one orbital per site
ldos_kpm *= 2
ldos_kpm_fixed_energy *= 2
```

## 2) LDOS kwant vs LDOS KPM & Convergence ifo the number of moments

Fundamental difference between kwant and kpm is that kwant treats infinite systems, while kpm treats finite systems.

The peaks occuring in the ldos for kwant are due to the discretization. You have a peaks corresponding to the band bottoms and tops (de/dk = 0), but the number of bands is equal to the number of transverse orbitals in a lead.

```python
def plot_convergence_moments(sys, moments, center=[0,0], ax=None, params=None):
    where = lambda s: s.pos == [0,0]
    vector_factory = kwant.kpm.LocalVectors(sys, where)

    ldos2 = kwant.kpm.SpectralDensity(sys, 
                                     num_vectors=None, 
                                     num_moments=moments[0],
                                     vector_factory=vector_factory,
                                     mean=False,
                                     params = params)
    if ax is None:
        fig, ax = plt.subplots()

    # Factor 2 to account for spin degeneracy, in Kwant we only have one orbital per site
    energies2, ldos_ifo_energy2 = ldos2()
    ax.plot(energies2, 2*ldos_ifo_energy2, label = 'kpm: {} moments'.format(moments[0]))

    for moment in moments[1:]:
        ldos2.add_moments(moment - ldos2.num_moments)

        # Factor 2 to account for spin degeneracy, in Kwant we only have one orbital per site
        energies2, ldos_ifo_energy2 = ldos2()
        ax.plot(energies2, 2*ldos_ifo_energy2, label = 'kpm: {} moments'.format(ldos2.num_moments))

    plt.show()
```

```python
fig, ax = plt.subplots()

# Main plot
moments = [25, 50, 100, 150]
ax.plot(energies, ldos_kwant[:, index_p], linewidth = 0.5, color='grey', label='kwant')
plot_convergence_moments(sys_kpm, moments, center=center, ax=ax)
ax.set_xlabel('energy [t]')
ax.set_ylabel(r'LDOS $[(t~site)^{-1}]$')

ymin, ymax = ax.get_ylim()
xmin, xmax = ax.get_xlim()
ax.plot([xmin, 0, 0, xmax], [0, 0, 1/(2*np.pi), 1/(2*np.pi)], linestyle='--', color='k', label='continuum limit')

ax.set_yticks(np.append(ax.get_yticks(), 1/(2*np.pi)))

yticklabels = ax.get_yticklabels()
yticklabels[-1] = mpl.text.Text(0, 1/(2*np.pi), r'$1/2\pi$')
ax.set_yticklabels(yticklabels)

ax.set_ylim(ymin, ymax)
ax.set_xlim(xmin, xmax)

# Inset
left, bottom, width, height = [0.15, 0.59, 0.25, 0.25]
ax_inset = fig.add_axes([left, bottom, width, height])

ax_inset.plot(energies, ldos_kwant[:, index_p], linewidth = 0.5, color='grey', label='kwant')
plot_convergence_moments(sys_kpm, moments, center=center, ax=ax_inset)

max_x = 0.3
max_y = 1.1*np.max(ldos_kwant[np.where(energies < max_x), index_p])
ax_inset.set_xlim([-0.15, max_x])
ax_inset.set_ylim([-0.01, max_y])

ax_inset.set_xticks([])
ax_inset.set_yticks([])
ax_inset.plot([xmin, 0, 0, xmax], [0, 0, 1/(2*np.pi), 1/(2*np.pi)], linestyle='--', color='k')


ax.legend()
ax.indicate_inset_zoom(ax_inset, edgecolor="black")
plt.show()
```

```python
fig.savefig('kpm_num_moments.pdf')
```

### 3.1) Peaks in kwant LDOS explained

The advantage of the small system is that 
* it shows that the peaks occur at band openings/closings
* We can easily increase the number of moments above the system size

Peaks are part of the reason why we don't use kwant to calculate the ldos, second reason is that 
kpm provides the complete ldos as polynomial expansion

```python
sys_kwant_small = make_kwant_system([1,1], 3, 5)
sys_kpm_small = make_kpm_system([1,1], 3, 5)

# Kwant calculation
ldos_kwant_small = []
energies_small = np.linspace(-0.1, 8.1, 300)
for e in energies_small:
    ldos_kwant_small.append(kwant.ldos(sys_kwant_small, e))
    
coord_small = np.array([site.pos for site in sys_kwant_small.sites])
center_index_small = extension_pes.indices_from_coordinates(coord_small, [center])[0]

# KPM calculation
where = lambda s: s.pos in [[0,0], [0,2]]
vector_factory = kwant.kpm.LocalVectors(sys_kpm_small, where)

ldos_kpm_small = kwant.kpm.SpectralDensity(sys_kpm_small, 
                                 num_vectors=None, 
                                 num_moments=250,
                                 vector_factory=vector_factory,
                                 mean=False)
```

```python
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec

time = np.arange(5)
weight = np.arange(5)
height = weight
score =weight

fig = plt.figure(figsize=(8, 4))
gs = gridspec.GridSpec(nrows=2, ncols=4, width_ratios= [2, 2, 1, 1])

# Kwant system
ax11 = fig.add_subplot(gs[0, 0])
def sites_color(site):
    if site == center_index_small:
        return 'tab:green'
    if site == center_index_small+2:
        return 'tab:orange'
    return 'tab:blue'

kwant.plot(sys_kwant_small, ax=ax11, site_color=sites_color)
ax11.set_aspect('equal')
ax11.set_xticklabels([])
ax11.set_ylabel('y [nm]')


# KPM system
ax12 = fig.add_subplot(gs[1, 0])
def sites_color(site):
    if site == center_index_small:
        return 'tab:green'
    if site == center_index_small+2:
        return 'tab:orange'
    return 'tab:blue'

kwant.plot(sys_kpm_small, ax=ax12, site_color=sites_color)
ax12.set_aspect('equal')
ax12.set_xlabel('x [nm]')
ax12.set_ylabel('y [nm]')
ax12.set_xlim(ax11.get_xlim())

# Bandstructure kwant system
ax2 = fig.add_subplot(gs[:, 1])
kwant.plotter.bands(sys_kwant_small.leads[0], ax=ax2)
ax2.set_xlabel(r'$k\cdot a$')
ax2.set_ylabel('energy [t]')
for line in ax2.lines:
    line.set_color('tab:blue')

# LDOS at the center
ax3_kpm = fig.add_subplot(gs[:, 2])
ax3_kwant = ax3_kpm.twiny()

# Factor 2 to account for spin degeneracy, in Kwant we only have one orbital per site
ax3_kwant.plot(2*np.array(ldos_kwant_small)[:,center_index_small], energies_small, 'tab:green')
ax3_kwant.set_xlabel('LDOS Kwant\n'r'$[(t~site)^{-1}]$')
ax3_kwant.set_ylim(ax2.get_ylim())
ax3_kwant.set_yticklabels([])


ax3_kpm.plot(2 * ldos_kpm_small.densities[:,0], ldos_kpm_small.energies, '--', color='tab:green', linewidth = 0.7)
ax3_kpm.set_xlabel('LDOS KPM\n'r'$[(t~site)^{-1}]$')


# LDOS at edge site
ax4_kpm = fig.add_subplot(gs[:, 3])
ax4_kwant = ax4_kpm.twiny()

# Factor 2 to account for spin degeneracy, in Kwant we only have one orbital per site
ax4_kwant.plot(2*np.array(ldos_kwant_small)[:,center_index_small+2], energies_small, 'tab:orange')
ax4_kwant.set_xlabel('LDOS Kwant\n'r'$[(t~site)^{-1}]$')
ax4_kwant.set_ylim(ax2.get_ylim())
ax4_kwant.set_yticklabels([])

ax4_kpm.plot(2 * ldos_kpm_small.densities[:,1], ldos_kpm_small.energies, '--', color='tab:orange', linewidth = 0.7)
ax4_kpm.set_xlabel('LDOS KPM\n'r'$[(t~site)^{-1}]$')

# Create abcde labels of the figures
axs = ax11, ax12, ax2, ax3_kpm, ax4_kpm
labels = 'a)', 'b)', 'c)', 'd)', 'e)'

for ax, label in zip(axs, labels):
    # label physical distance to the left and up:
    trans = mtransforms.ScaledTranslation(-20/72, 7/72, fig.dpi_scale_trans)
    ax.text(0.0, 1.0, label, transform=ax.transAxes + trans,
            fontsize='medium', va='bottom', fontfamily='latin modern')

plt.tight_layout()
plt.show()


# Dotted line in 2 left graphs is the ldos in kpm, full line ldos in kwant
# Depending on the position in the system some modes may not contribute, e.g. in the center
# Peaks not at the same location for kwant & kpm, difference between infinite and finite system
```

```python
fig.savefig('kpm_ldos_kwant_vs_kpm.pdf')
```

## 3) Oscillations in the LDOS coming from the boarder of the system

```python
# Get general information on the areas in which we can not trust the data from kpm due to effects
# from the boundary
min_coord = np.min(coord[:,0])
max_coord = np.max(coord[:,0])
boundary_l = min_coord + grid[0] * num_moments/2
boundary_r = max_coord - grid[0] * num_moments/2

cbar_range = [np.min([ldos_kpm_fixed_energy, ldos_kwant[index_e]]), 
              np.max([ldos_kpm_fixed_energy, ldos_kwant[index_e]])]

# Start making the figure
fig, ((ax1, ax2), (ax3, ax4)) = plt.subplots(2,2, figsize = (8,6), gridspec_kw={'width_ratios': [2, 1]})
colors = ['tab:blue', 'tab:orange', 'tab:cyan']

# Ax1: ldos ifo energy for 3 different locations
points = np.array([[0,0], [220, 0], [450, 0]])
indices = extension_pes.indices_from_coordinates(coord, points)
for i in range(len(indices)):
    ax1.plot(energies_kpm, ldos_kpm[:,indices[i]], color=colors[i])
ax1.set_xlabel('energy [t]')
ax1.set_ylabel(r'LDOS KPM $[(t~site)^{-1}]$')
asp = np.diff(ax1.get_xlim())[0] / np.diff(ax1.get_ylim())[0]
ax1.set_aspect(asp/2)
ax1.axvline(1, color='k', linestyle = '--', linewidth = 0.7)
ax1.legend([str(elem) for elem in points])


# Ax2: overview of the used points in plot on ax1
#kwant.plot(sys_kpm, ax=ax2)
plotting_pes.colormap(coord, ldos_kwant[index_e], ax = ax2, cbar_range=cbar_range,
                      cbar_label = r'LDOS Kwant $[(t~site)^{-1}]$', xlabel = 'x [nm]', ylabel = 'y [nm]')
ax2.set_aspect('equal')
ax2.set_xlabel('x [nm]')
ax2.set_ylabel('y [nm]')


# Ax3: cross section ldos at energy equal to 1*t
# Make a cross section of the data as obtained from kpm
coord_1D, ldos_1D = plotting_pes.data_cross_section(coord, [ldos_kpm_fixed_energy], section = [None, 0])
coord_1D = coord_1D.flatten()
ldos_1D = ldos_1D.flatten()

# Plot the data as obtained from kpm
ax3.plot(coord_1D, ldos_1D, color='tab:red', linestyle = '--', linewidth = 0.7)
ax3.set_xlabel('x [nm]')
ax3.set_ylabel(r'LDOS KPM $[(t~site)^{-1}]$')

# Indicate the areas in which we can not trust the data from kpm
ax3.fill_between([min_coord, boundary_l], -1, 1, 
                 color='grey', alpha=0.3)
ax3.fill_between([boundary_r, max_coord], -1, 1, 
                 color='grey', alpha=0.3)

# Setting the limits of the y axis
margin = 1.2
min_ = np.min(ldos_1D)
max_ = np.max(ldos_1D)
av = (min_+max_)/2
d = (max_-min_)/2
ax3.set_ylim([av - margin*d, av + margin*d])

# Create the reduced data, which we will use in the simulator
index_red = np.where(np.all([boundary_l <= coord_1D, coord_1D <= boundary_r], axis=0))[0]
ax3.plot(coord_1D[index_red], ldos_1D[index_red], color='tab:red')
    
asp = np.diff(ax3.get_xlim())[0] / np.diff(ax3.get_ylim())[0]
ax3.set_aspect(asp/2)
ax3.scatter(points[:,0], ldos_kpm_fixed_energy[indices], color =colors)

ax3.set_xlim(min_coord, max_coord)

# Ax4: colormap of the ldos at energy equal to 1*t
plotting_pes.colormap(coord, ldos_kpm_fixed_energy, ax = ax4, cbar_range = cbar_range,
                      cbar_label = r'LDOS KPM $[(t~site)^{-1}]$', xlabel = 'x [nm]', ylabel = 'y [nm]')
ax4.scatter(points[:,0], points[:,1], color = colors)
ax4.plot([min(coord[:,0]), max(coord[:,0])], [0,0], '--', color = 'tab:red', linewidth=0.7)
ax4.plot([boundary_l, boundary_r], [0,0], color = 'tab:red')
ax4.scatter(points[:,0], points[:,1], color = colors)

# Create abcde labels of the figures
axs = ax1, ax2, ax3, ax4
labels = 'a)', 'b)', 'c)', 'd)'

for ax, label in zip(axs, labels):
    # label physical distance to the left and up:
    trans = mtransforms.ScaledTranslation(-20/72, 7/72, fig.dpi_scale_trans)
    ax.text(0.0, 1.0, label, transform=ax.transAxes + trans,
            fontsize='medium', va='bottom', fontfamily='latin modern')
    
    
for ax in ax2, ax3, ax4: 
    ax.set_xticks([min_coord, boundary_l, 0, boundary_r, max_coord])

for ax in ax2, ax4: 
    ax.set_yticks([min_coord, boundary_l, 0, boundary_r, max_coord])

plt.tight_layout()
plt.show()

# ax2: Note periodicity of kwant system since infinite in the x-direction
# ax3: Note the boundary effects for the ldos in kpm
```

```python
fig.savefig('kpm_boundary_oscillations.pdf')
```

## 4) Analytical integration of the polynomial expansion

```python
# Plotting the integration error by Gauss-Chebyshev integration of the points
where = lambda s: s.pos == [0,0]
vector_factory = kwant.kpm.LocalVectors(sys_kpm, where)

ldos4 = kwant.kpm.SpectralDensity(sys_kpm, 
                                 num_vectors=None, 
                                 num_moments=50,
                                 vector_factory=vector_factory,
                                 mean=False)

ldos4_exact = exact_ldos.ExactLdos(ldos4)

bottom_band =  -ldos4._a + ldos4._b
top_band =  ldos4._a + ldos4._b
energy = np.linspace(bottom_band, top_band, 600)

ildos4= []
for e in energy:
    fermi = lambda E: kwant.kpm.fermi_distribution(energy=E, mu=e, 
                                                   temperature=0)
    
    ildos4.append(2*ldos4.integrate(fermi))

fig, ax = plt.subplots()
# Main plot
ax.plot(energy, ildos4, '-', label='Gauss Chebyshev')
ax.plot(energy, 2*ldos4_exact.integrate(energy), '-', label='Analytical')
ax.legend(loc = 'lower right')
ax.set_xlabel('energy [t]')
ax.set_ylabel('ILDOS [1/site]')

# Inset
left, bottom, width, height = [0.16, 0.5, 0.33, 0.33]
ax_inset = fig.add_axes([left, bottom, width, height])
ax_inset.plot(energy, ildos4, '.-', linewidth = 0.5)
ax_inset.plot(energy, 2*ldos4_exact.integrate(energy), '-')

max_x = 0.3
max_y = np.max(np.array(ildos4)[np.where(energy < max_x)])
ax_inset.set_xlim([0, max_x])
ax_inset.set_ylim([0, max_y])

ax_inset.set_xticks([])
ax_inset.set_yticks([])

ax.indicate_inset_zoom(ax_inset, edgecolor="black")

plt.show()
```

```python
fig.savefig('kpm_analytical_integration.pdf')
```

## 5) Real potential, depleted sites
### 5.1) Influence reducing the kpm system

```python
onsite_potential = []
for i in range(len(sys_kpm_v.sites)):
    onsite_potential.append(sys_kpm_v.hamiltonian(i, i, params = params))
    
potential = dict(zip([site.pos for site in sys_kpm_v.sites], np.array(onsite_potential) - 4))
```

```python
# Calculate the ldos in kwant for a given energy, better to do this on cluster such that
# all the energies are calculated on separate ranks.
ldos_kwant = pk_problem.ldos_kwant(potential[index_v][iteration], energy=0, params=params)
```

```python
kwant.plotter.density(sys_kpm_v, onsite_potential)
plt.show()
```

```python
from qpc_exp2sim.tools import data_handling as dh
from qpc_exp2sim.pescado_kwant import extension_pes
data = dh.read_file('files_data/a2_grid10_V4_a2_fsc_nr_m100_it10')
```

```python
def reduce_builder(coord_pes, potential_pes, builder_kpm, threshold):

    coord_kpm = [site.pos for site in builder_kpm.sites()]
    coord_kpm_ext = np.insert(coord_kpm, 2, [0], axis=1)

    indices_kpm = extension_pes.indices_from_coordinates(coord_pes, coord_kpm_ext)
    
    conversion = dict(zip(coord_kpm, indices_kpm))
        
    builder = deepcopy(builder_kpm)
    for site in list(builder.sites()):
        if potential_pes[conversion[site.pos]] > threshold:
            del builder[site]
    
    return builder.finalized()
```

```python
from qpc_exp2sim.pescado_kwant import plotting_pes
```

```python
sys_kpm_reduced = reduce_builder(data.coord, data.voltage[2][-1]*(-1), builder_kpm, 0.1)
```

```python
fig, (ax1, ax2, ax3) = plt.subplots(1,3, figsize=(10,4), sharey=True)
kwant.plot(sys_kpm, ax=ax1)
ax1.set_title('Full KPM system')
ax1.set_ylabel(r'$y~[nm]$')


kwant.plot(sys_kpm_reduced, ax=ax2)
ax2.set_title('Reduced KPM system')


coord_2D, potential_2D = plotting_pes.data_cross_section(data.coord,  data.voltage[2][-1]*(-1e3), section=[None, None, 0])
plotting_pes.colormap(coord_2D, potential_2D, ax=ax3, cbar_label = r'$U~[meV]$')
ax3.set_title('Potential')

for ax in (ax1, ax2, ax3):
    ax.set_aspect('equal')
    ax.set_xlim([-500, 500])
    ax.set_ylim([-500, 500])
    ax.set_xticks(range(-500, 501, 250))
    ax.set_yticks(range(-500, 501, 250))
    ax.set_xlabel(r'$x~[nm]$')
    
plt.show()
```
