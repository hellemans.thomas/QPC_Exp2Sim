#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May 24 10:33:06 2022

@author: thellemans
"""
from mpi4py import MPI
from qpc_exp2sim.simulation import builder, solver
from qpc_exp2sim.pescado_kwant import pescado_kwant, extension_pes
from qpc_exp2sim.tools import data_handling as dh
import sys
import pickle
import scipy.constants as c

comm = MPI.COMM_WORLD

###############################################################################
#                           Reading of the input                              #
###############################################################################

assert len(sys.argv) == 3, (
    'Please provide one geometry and one simulation input file.')

file_geom = sys.argv[1]
file_sim = sys.argv[2]
if '/' in file_geom:
    split = '/' #Unix
else:
    split = '\\' #Windows
file_data = 'files_data/' + file_geom[file_geom.rfind(split)+1:] + \
             '_' + file_sim[file_sim.rfind(split)+1:]

# Avoid that all the ranks want to acces the same file at the same time
if comm.rank == 0:
    with open(file_geom, 'rb') as inp:
        inp = pickle.load(inp)
else:
    inp = None

inp = comm.bcast(inp, root=0)
    

###############################################################################
#           Initialisation, Creation of the PescadoKwantProblem               #
###############################################################################


if isinstance(inp, (tuple, list)):
    p_geom = inp[0]
    pk_problem = inp[1]
    
    if comm.rank == 0:
        print('pk_problem used from file')
        print(p_geom)
else:
    p_geom = inp
    
    # pp_problem is only returned correctly on rank 0!
    pes_builder = builder.make_pescado_system(**p_geom)
    pp_problem = extension_pes.finalized(pes_builder, parallelize='mpi', 
                                         comm=comm)

    if comm.rank == 0:
        print(p_geom)
        kwant_builder = builder.make_kwant_system(**p_geom)
        kpm_builder = builder.make_kpm_system(**p_geom)
        
        conversion_index = pescado_kwant.conversion_index(
                pp_problem, kwant_builder, kpm_builder, extra_dim=[2], 
                coord_extra_dim=[0])
    
        # This creates a pk_problem and writes it to the geometry file
        pk_problem = pescado_kwant.PescadoKwantProblem(
                poisson_problem = pp_problem, 
                kwant_builder = kwant_builder, 
                kpm_builder = kpm_builder,
                conversion_unit = c.elementary_charge/p_geom['t'], 
                conversion_index = conversion_index,
                file=file_geom, 
                extra_dim = [2], 
                coord_extra_dim = [0])


###############################################################################
#               Configuration, Setting up the input parameters                #
###############################################################################


# Avoid that all the ranks want to acces the same file at the same time
if comm.rank == 0:
    with open(file_sim, 'rb') as inp:
        p_sim = pickle.load(inp)
else:
    p_sim = None
p_sim = comm.bcast(p_sim, root=0)

if comm.rank == 0:
    print(p_sim)


###############################################################################
#                    Solve the PescadoKwantProblem                            #
###############################################################################


# Data is only returned correctly on rank 0
data = solver.solve(file_geom=file_geom, 
                    file_sim=file_sim,
                    file_data=file_data)
