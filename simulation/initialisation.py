#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Created on Mon Apr  4 18:25:29 2022

@author: thellemans
"""
from qpc_exp2sim.simulation.builder import extend_params
import qpc_exp2sim.tools.data_handling as dh


# Setting the device parameters
p_geom = {'grid' : [10, 10],                  # Mesh in xy direction in device (Pescado mesh + Kwant lattice)
          'L_kwant' : 500,
          'W_kwant' : 500,
          'L_kpm': 2500,
          'W_kpm': 2500,
          'L_pes' : 500,                              # Lenth total device in x direction = transport direction
          'W_pes' : 500,                              # Width total device in y direction = gate direction
          'L_narrow_gate' : 50,                        # Length of the narrow gate 
          'qpc_center' : [0,240],                      # Widthh of the narrow gate
          'qpc_name' : 'a2',
          'd0' : 25,                                   # Thickness GaAs layer bottom
          'd_2DEG' : 10,                               # Thickness 2DEG
          'd1' : 30,                                   # Thickness AlGaAs above 2DEG
          'd2' : 60,                                   # Thickness AlGaAs, Si doped
          'd3' : 10,                                   # Thickness AlGaAs 
          'd4' : 10,                                   # Thickness GaAs
          'd_contact' : 10,                            # Thickness contact layer
          'eps_GaAs' : 12.93,                          # Dielectric constant GaAs
          'eps_AlGaAs' : 11.93,                        # Dielectric constant AlGaAs
          'eps_gates' : 1e4,                           # Dielectric constant gates (metal)
          'effective_mass' : 0.067,                    # Effective mass for 2DEG DOS calculation
          'calibration_device' : False, 
          'zones' : [False, True, False, False, True, True, True],
          'w' : 10,
          'size' : 10, 
          'test_07' : False}

# Introduce a first assertion function here to check if all the variables are 
# compatible
p_geom = extend_params(p_geom)

file = 'files_geom/'+ p_geom['qpc_name'] + '_grid' + str(p_geom['grid'][0]) +\
        '_' + str(p_geom['L_kwant']) + '_' + str(p_geom['L_kpm'])
dh.write_file(p_geom, file)