---
jupyter:
  jupytext:
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.13.8
  kernelspec:
    display_name: Python 3 (ipykernel)
    language: python
    name: python3
---

```python
import kwant
import numpy as np
import matplotlib.pyplot as plt
%matplotlib notebook

# For the calculations below we set the value of h_bar**2/2m* equal to:
cst = 1
```

## 1D chain

```python
a = 1
L = 4
t = cst/a**2
V0 = -1

sys = kwant.Builder()
lat = kwant.lattice.Monatomic(np.diag([a,a]), norbs=1)

def scat_reg(pos):
    x,y = pos
    return 0 <= x < L and y == 0

## On site hamiltonian
sys[lat.shape(scat_reg, (0,0))] = 2*t + V0

## Hopping in x and y direction
sys[lat.neighbors(1)] = -t

# Adding the leads 
## Lead on the right
sym_right = kwant.TranslationalSymmetry((a, 0))
lead_right = kwant.Builder(sym_right)
lead_right[lat.shape(scat_reg, (0,0))] = 2*t + V0
lead_right[lat.neighbors()] = -t
sys.attach_lead(lead_right)

## Lead on the left    
sys.attach_lead(lead_right.reversed())

# Finalize the system.
sys = sys.finalized()
```

```python
kwant.plot(sys)
plt.show()
```

```python
fig, ax = plt.subplots()
kwant.plotter.bands(sys.leads[0], ax=ax)

ka = np.linspace(-np.pi, np.pi)
band_analytical = V0 + 2*t - 2*t*np.cos(ka) # Onsite_potential - 2*cos(k) * hopping_potential
ax.plot(ka, band_analytical, linestyle = '--')
plt.xlabel(r'$k\cdot a$')
plt.ylabel('energy')
plt.legend(['band_kwant', 'band_analytical'])
plt.show()
```

```python
energy = np.linspace(-5, 5, 100)

ldos_kwant = []
for i, energy_val in enumerate(energy):
    ldos_kwant.append(kwant.ldos(sys, energy=energy_val))
    
ldos_analytical = 1/(np.pi*np.sqrt((energy-V0)*(4*t+V0-energy)))
```

```python
fig, ax = plt.subplots()
ax.plot(energy, np.array(ldos_kwant)[:,0], label = 'kwant') # All sites give the same result for a 1D system
ax.plot(energy, ldos_analytical, '--', label = 'analytical') # All sites give the same result for a 1D system
plt.legend()
plt.show()
```

## 2D lattice

```python
a = 1
b = 2
L = 5
W = 11
t = cst * (1/a**2 + 1/b**2) /2
V0 = 0

sys = kwant.Builder()
lat = kwant.lattice.Monatomic(np.diag([a,b]), norbs=1)

def scat_reg(pos):
    x,y = pos
    return 0 <= x < L and 0 <= y < W

## On site hamiltonian
sys[lat.shape(scat_reg, (0,0))] = 4*t + V0

## Hopping in x direction
sys[kwant.builder.HoppingKind((1, 0), lat, lat)] = -2*t/(1+(a/b)**2)

## Hopping in y direction
sys[kwant.builder.HoppingKind((0, 1), lat, lat)] = -2*t/(1+(b/a)**2)

# Adding the leads 
## Lead on the right
sym_right = kwant.TranslationalSymmetry((a, 0))
lead_right = kwant.Builder(sym_right)
lead_right[lat.shape(scat_reg, (0,0))] = 4*t + V0
lead_right[kwant.builder.HoppingKind((1, 0), lat, lat)] = -2*t/(1+(a/b)**2)
lead_right[kwant.builder.HoppingKind((0, 1), lat, lat)] = -2*t/(1+(b/a)**2)
sys.attach_lead(lead_right)

## Lead on the left    
sys.attach_lead(lead_right.reversed())

# Finalize the system.
sys = sys.finalized()

steps = W//b
display(steps)
```

```python
kwant.plot(sys)
plt.show()
```

```python
band = kwant.physics.Bands(sys.leads[0])
step = np.arange(len(band(0)))
fig, ax = plt.subplots()
ax.plot(step, band(0)-np.average(band(0)), '.')

plt.plot(step, -2*np.cos(np.pi*step/W))

```

```python
fig, ax = plt.subplots()
kwant.plotter.bands(sys.leads[0], ax=ax)

k = np.linspace(-np.pi, np.pi)

band_analytical = V0 + 4*t - 4*t*np.cos(k)/(1+(a/b)**2)
ax.plot(ka, band_analytical, linestyle = '--', label='band_analytical')

band_analytical2 = V0 + 4*t - 4*t*np.cos(k)/(1+(a/b)**2) - 4*t*np.cos(np.pi*3/steps)/(1+(b/a)**2)
ax.plot(ka, band_analytical2, linestyle = '--', label='band_analytical')


plt.xlabel(r'$k$')
plt.ylabel('energy')
plt.legend()
plt.show()
```

```python
energy = np.linspace(0, 8, 60)

ldos_kwant = []
for i, energy_val in enumerate(energy):
    ldos_kwant.append(kwant.ldos(sys, energy=energy_val))
    
#ldos_analytical = 1/(np.pi*np.sqrt((energy-V0)*(4*t+V0-energy)))
```

```python
center = np.where((np.array([site.tag for site in sys.sites])==(int(L/(2*a)), int(W/(2*b)))).all(axis=1))[0][0]

fig, ax = plt.subplots()
ax.plot(energy, np.array(ldos_kwant)[:,center], label = 'kwant') # All sites give the same result for a 1D system
#ax.plot(energy, ldos_analytical, '--', label = 'analytical') # All sites give the same result for a 1D system
#ax.plot(energy, 0.04*f(energy) + 0.13, label='fit')
plt.legend()
plt.show()
```

```python
energy = np.linspace(0, 8, 60)

ldos_kwant = []
for i, energy_val in enumerate(energy):
    ldos_kwant.append(kwant.ldos(sys, energy=energy_val))
    
center = np.where((np.array([site.tag for site in sys.sites])==(int(L/(2*a)), int(W/(2*b)))).all(axis=1))[0][0]

fig, ax = plt.subplots()
ax.plot(energy, np.array(ldos_kwant)[:,center], label = 'kwant') # All sites give the same result for a 1D system
#ax.plot(energy, ldos_analytical, '--', label = 'analytical') # All sites give the same result for a 1D system
#ax.plot(energy, 0.04*f(energy) + 0.13, label='fit')
plt.legend()
plt.show()
```

```python
center = np.where((np.array([site.tag for site in sys.sites])==(int(L/(2*a)), int(W/(2*b)))).all(axis=1))[0][0]
```

```python
len(sys.sites)
```

```python
import pickle
with open('files_sim/V0_energy', 'rb') as inp:
    p_sim = pickle.load(inp)
```

```python
def f(x):
    shift = V0 + 4*t
    result = np.array([-np.log(x_val-shift) if (x_val > shift) else -np.log(-x_val+shift) for x_val in x])
    return result
```

```python
f(np.linspace(-3, 3))
```

```python

```

```python

```
