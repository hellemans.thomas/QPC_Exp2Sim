#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May 23 15:33:00 2022

@author: thellemans
"""
import numpy as np
import qpc_exp2sim.tools.data_handling as dh

p_sim = {# Boundary conditions
         'V_gates_list' : np.array([-1.9, -1.8, -1.4,  0. ]), 
         'energy_list' : np.array([0]),                    
         'DEG_dens' : 2.8e15,                # Electron density in 2DEG [/m²]
         'num_moments' : 300, 
         'iterations' : 5,
         'temp' : 0,
    
         # Simulation parameters
         # Setting of the pk_problem solver that will be used: 
         # 'lhh' = linear helmholtz
         # 'pi_bulk' = piecewise bulk 2D system ildos¸
         # 'pi_lat' = piecewise lattice ildos
         # 'nr_lat' = newton raphson lattice ildos
         # 'fsc' = full self consistent
         'setting_solver' : 'fsc_nr',       
         'calibration' : True, 
         'calibration_method' : 'v2',
         'calc_V3' : True, 
         'calc_cond' : True,
         'save_poisson' : True, 
         'save_iteration' : False}

dh.write_file(p_sim, 'files_sim/' + 'V{}_a2_{}_m{}_it{}'.format(
                  len(p_sim['V_gates_list']), 
                  p_sim['setting_solver'],
                  p_sim['num_moments'], 
                  p_sim['iterations']))

# dh.write_file(p_sim, 'files_sim/' + 'V{}_a2_{}'.format(
#               len(p_sim['V_gates_list']), 
#               p_sim['setting_solver']))

