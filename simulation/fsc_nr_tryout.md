---
jupyter:
  jupytext:
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.3'
      jupytext_version: 1.13.8
  kernelspec:
    display_name: Python 3 (ipykernel)
    language: python
    name: python3
---

```python
import pickle
import kwant
import scipy.constants as c
import numpy as np
import matplotlib.pyplot as plt
import qpc_exp2sim.tools.plotting_sim_old as plotting_sim
from qpc_exp2sim.pescado_kwant import plotting_pes, PescadoKwantProblem, extension_pes, ReducedPoissonProblem
from qpc_exp2sim.simulation import builder, solver
from copy import deepcopy
from pescado.tools import SparseVector
import time
from functools import partial
%matplotlib notebook
```

```python
geom_file = 'b3_grid50'
sim_file = 'V3_nr_lat_low_dens'

with open('files_geom/'+ geom_file, 'rb') as inp:
    p_geom, pk_problem = pickle.load(inp)
    
with open('files_data/' + geom_file + '_' + sim_file, 'rb') as inp:
    data = pickle.load(inp)
    
# unpacking the data for compatibility with old plotter
p_geom = data.p_geom
p_sim = data.p_sim
file_geom = data.file_geom
file_sim = data.file_sim
V_gates_list = p_sim['V_gates_list']
energy_list = p_sim['energy_list']
coord = data.coord
coord_kwant = data.coord_kwant
coord_kpm = data.coord_kpm
voltage = data.voltage
charge = data.charge
conductance = data.conductance
voltage_shift = data.voltage_shift
dopant_dens = data.dopant_dens
V3 = data.V3

cell_area = p_geom['grid_fine'][0] * p_geom['grid_fine'][1] * 1e-18

x_coord_kwant = [site.pos[0] for site in pk_problem.kwant_sys.sites]
params = {'lead_left': min(x_coord_kwant), 
          'lead_right': max(x_coord_kwant)}
```

```python
index_v = 0
voltage_res = voltage[index_v]
charge_res = charge[index_v]

threshold_ldos = p_sim['helmholtz_coef']/3
num_moments = 100
voltage = {'gates': V_gates_list[index_v] + voltage_shift}
charge_density = {'dopant': dopant_dens}
charge_electrons = {}
initial_potential = {'2DEG': 0}
helmholtz_coef = {}
```

```python
ldos = pk_problem._spectral_density(num_moments=num_moments, 
                              where = lambda s: s.pos == np.array([0,0]), 
                              params=params)

quantum_functional_lat_f = partial(PescadoKwantProblem.quantum_functional_lat, 
                                   moments = ldos._moments(),
                                   a = ldos._a, 
                                   b = ldos._b,
                                   t = pk_problem.t)
```

```python
fig, ax = plt.subplots()
ax.plot(ldos.energies, ldos.densities)
plt.show()
```

```python
flexible_index = pk_problem.rpp_problem.linear_problem_inst.regions_index[3]
sites_ildos = SparseVector(indices = flexible_index, 
                           values = np.zeros(len(flexible_index)))

# Set the treshold for the ldos based filtering 
threshold_ldos = SparseVector(
    indices = flexible_index, 
    values = threshold_ldos + np.zeros(len(flexible_index)))
```

```python
# Calculation of the potential in kwant energy units
potential = voltage_res * (c.elementary_charge / pk_problem.t)    

# Based on this potential we reduce the kpm/kwant system
pk_problem.reduce_system(potential, threshold=-4)
```

```python
kwant.plot(pk_problem.kwant_sys)
plt.show()
kwant.plot(pk_problem.kpm_sys)
plt.show()
```

```python
# With the reduced system we calculate the ldos using kpm       
ldos = pk_problem._spectral_density(potential = potential,
                              num_moments = num_moments*2, 
                              where = pk_problem.kwant_sys.sites, 
                              params = params)
ldos_kpm = ldos(energy=0).real
```

```python
fig, ax = plt.subplots()
ax.plot(ldos.energies, ldos.densities)
plt.show()
```

```python
flexible_index, moments = pk_problem.recext_data(ldos._moments().transpose())
moments = moments.transpose()

initial_pot = {}
for region in pk_problem.rpp_problem.regions['flexible']:
    region_index = pk_problem.rpp_problem.sub_region_index[region]
    initial_pot[region] = voltage_res[region_index]

# Creating the quantum functional for the newly calculated ildos

# Due to the calculation of the SpectralDensity at a certain 
# potential, the ldos is shifted in energy. We need to take this
# into account when evaluating the quantum functional.
mu_ref = voltage_res[sites_ildos.indices]
#moments = moments[filter_moments.values].transpose()

quantum_functional_kwant_f = partial(PescadoKwantProblem.quantum_functional_kwant, 
                             mu_ref = mu_ref,
                             moments = moments,
                             a = ldos._a, 
                             b = ldos._b,
                             t = pk_problem.t)
```

```python
# Solving the nr problem
# pk_problem.rpp_problem.freeze()

print(pk_problem.coord[sites_ildos.indices], len(sites_ildos.indices))

voltage_res, charge_res = pk_problem.rpp_problem.solve_sc_continuous(
        voltage = voltage,
        charge_electrons = charge_electrons,
        charge_density = charge_density,
        helmholtz_coef = helmholtz_coef,
        initial_potential = initial_pot,
        quantum_functional = [quantum_functional_kwant_f],
        sites_ildos = sites_ildos)
```

```python
sites_ildos.__dict__
```

```python
len(sites_ildos.values)
```

```python
coord_2D, voltage_2D = plotting_pes.data_cross_section(coord, voltage_res, section=[None, None, 0])
coord_2D, charge_2D = plotting_pes.data_cross_section(coord, charge_res, section=[None, None, 0])

fig, (ax1, ax2) = plt.subplots(1,2, figsize=(10,5))
plotting_pes.colormap(coord_2D, voltage_2D, aspect_equal=True, ax=ax1)
plotting_pes.colormap(coord_2D, charge_2D/cell_area, aspect_equal=True, ax=ax2)

plt.tight_layout()
plt.show()
```

```python
ldos._moments().shape
```

```python
21**2
```

```python
r = SparseVector(indices = np.array([1,2]), 
             values = np.array([[1,1], [2,2]], dtype=object))
```

```python
r[1]
```

```python

```
