GENERAL
• Documentation especially for the self_consistent problem needs to be improved
∘ e.g. parameter `sites_ildos`
∘ e.g. poisson_problem_input in NaiveSchrodinger.initialize() can take a key 'method', this was by default set to scipy, but it's more interesting that this is mumps
∘ Clearer discription of tolerances, precisions (relative or absolute), 
• use uniform notation for 'potential' or 'voltage'
∘ e.g. Hard to claim: 'You set the voltage (dirichlet sites) as initial condition and I will return you the potential (energy) in the flexible sites' What is the energy scale in this case?
• High and low level API as in tkwant?

TOOLS
• You can multiply a sparse vector by a number but not divide it by a number

MESH
• The ordering of coordinates and sites is different in Pescado and Kwant: 
∘ kwant: 
‣ array([[-400., -400.],
‣        [-400., -200.],
‣        [-400.,    0.],
‣        [-400.,  200.],
‣        [-400.,  400.],
‣        [-200., -400.],
‣        [-200., -200.],
‣        [-200.,    0.],
‣        [-200.,  200.],
‣        [-200.,  400.],
‣        [   0., -400.],
‣        [   0., -200.],
‣        [   0.,    0.],
‣        [   0.,  200.],
‣        [   0.,  400.],
‣        [ 200., -400.],
‣        [ 200., -200.],
‣        [ 200.,    0.],
‣        [ 200.,  200.],
‣        [ 200.,  400.],
‣        [ 400., -400.],
‣        [ 400., -200.],
‣        [ 400.,    0.],
‣        [ 400.,  200.],
‣        [ 400.,  400.]])
∘ Pescado
‣ array([[-400., -400.,  -20.],
‣        [-200., -400.,  -20.],
‣        [   0., -400.,  -20.],
‣        ...,
‣        [   0.,  400.,  120.],
‣        [ 200.,  400.,  120.],
‣        [ 400.,  400.,  120.]])
• mesher.mesh._indices_from_coordinates consumes quite a lot of memory. e.g. when the indices from 201**2 = 40401 sites are requested  for a system with 606015 sites, a total memory of 182 GiB will be necessary to create a 40401*606015 matrix:
∘ np.arange(1, all_coord.shape[0] + 1, dtype=int)[:, None]).ravel()  numpy.core._exceptions.MemoryError: Unable to allocate 182. GiB for an array with shape (40401, 606015) and data type int64

POISSON
• Using MPI instead of fork, e.g. useful function is MPI.allreduce Moreover fork is not available on windows systems -> personally changed to spawn on windows.
• The function poisson.problem.group_matrix() still needs to be parallelized, parallellize is a parameter that is just not used here
• Is the mesh_inst truly necessary in the pp_problem? (Now it is if you want to use reset later)
• Finalising a system: by parallellizing the volume calculation I won a factor of 3 in finalisation time and still half of the time of finalisation is spend by calculating the volume This means that initially this was 5/6th of the time! Extra note: in my case the volume is everywhere constant due to the fact that I use a single pattern, this might be an interesting shortcut to avoid excessive calculations -> e.g. shortcut for user that only uses a single pattern
• why in pp_problem.reset a call to pp_problem._arrange_region_index() ? linear_problem_inst has regions_index stored away and regions_index is not changed during operation.

SELF CONSISTENT
• For the NaiveSchrodingerPoisson solver you can only enter a continuous ildos function.  In my case I obtain first the ldos function and put effort in integrating this to obtain an ildos. It's very inefficient to derive the ildos than again to obtain an ldos.
• test_nr_vs_pescado.py: convergence of NaiveSolver and PescadoSolver to the same results? (I can't obtain a better accuracy than a 1% difference on site 822)
• absolute errors are harder to specify as user than relative errors
• NaiveSolver and SchrodingerPoisson solver don't converge to same result? e.g. for my oscillations this is very important
• NaiveSolver -> default solver set to scipy, can be changed to mumps?

KWANT
• For kwant.kpm, when trying to evaluate the SpectralDensity at an energy outside the min max region of the band structure you get:
∘ /home/hellemans/.local/lib/python3.7/site-packages/kwant/kpm.py:281: RuntimeWarning: invalid value encountered in sqrt g_e = (np.pi * np.sqrt(1 - e) * np.sqrt(1 + e)) /home/hellemans/.local/lib/python3.7/site-packages/kwant/kpm.py:287: RuntimeWarning: invalid value encountered in true_divide return np.transpose(chebval(e, moments) / g_e)
∘ If the asked energy is outside of the min max region of the band structure, it is larger than 1 or smaller than -1 after the internal rescaling therefore the warning. You get as result an array of nans. Wouldn't the same behaviour as kwant.ldos be preferable -> returning an  array of zeros?
• When creating an onsite potential for a lead, the sites given to the onsite potential function are not the same sites as mentioned in kwant_sys.lead_interfaces. This would be usefull to expand for example an onsite potential calculated for the system towards the leads
• Does kwant provide an interface for cross section plots of data?
