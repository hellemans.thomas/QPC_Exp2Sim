<!-- #region -->
# QPC_Exp2Sim

Simulate g(Vg) for actual quantum point contacts belonging to the Juliang experimental data set.
Comparing with experiments.

## (Un)Installing package

### Using pip

For installation go to the qpc_exp2sim (top) directory and run the following command for and installation in development mode:

	pip install -e .

This allows qpc_exp2sim to be easily imported when doing simulations or data analysis in other python files.

For removing run:

	pip uninstall qpc_exp2sim

This doesn't remove the existing qpc_exp2sim.egg_info directory

**Note**: installation using conda seems to fail.

### Dependency on Kwant and Pescado

Kwant and pescado are necessary for qpc_exp2sim to function:
Installing Kwant can be done by following the instructions in the kwant documentation.
Installing Pescado on Windows (using conda) can be done as follows:

* Clone the pescado repository from gitlab into the Users/user_name/miniconda3/pkgs directory
* In the pescado (top) directory run the commands:

        python setup.py build_ext --inplace
	
        conda develop .

**Note**: In pescado\pescado\poisson\builder.py: line 78 in sparse_capa_matrix_parallel

    with multiprocessing.get_context('fork')

causes and error due to the fact that 'fork' is only available for Unix systems, changed to None.

## Data files

The data files as used in the master thesis (see QPC_Exp2sim/report) can be found on cluster nano (CEA/Pheliqs) at `/home/hellemans/qpc_exp2sim_data` or via ... (include link towards data files). 

The data files in the `qpc_exp2sim_data` folder are of two types, data files originating from experiments or simulations. The experimental results are obtained by Chatzikyriakou et al. in the paper 'Unveiling the charge distribution of a GaAs based nanoelectronic device: A large experimental data set approach'. A separate README file regarding these data files, written by Chatzikyriakou et al. is included in  `qpc_exp2sim_data/experiment/files_data`.

For easy analysis of the data files the `qpc_exp2sim_data` folder has to be downloaded (be aware that it contains some large files) and line 17 in `QPC_Exp2sim/qpc_exp2sim/tools/data_handling.py` has to be changed to the path where the data folder is stored. Note that this path might be different on your local machine and on a computing cluster. This repository contains an extensive set of Jupyter notebooks (saved in markdown format using Jupytext) allowing to interpret the stored data in these files.

### 1) Creation and use of the simulation data files

The folder `qpc_exp2sim_data/simulation` contains three subfolders. Each of these three subfolders contains a specific type of data file created by the `pickle` module in `python`.

1. `qpc_exp2sim_data/simulation/files_geom` contains files describing the geometry of a certain device. These files are used to initiate a simulation (in combination with a simulation file). A geometry file can be created via the `simulation/initialisation.py` python file in the `QPC_Exp2Sim` repository. When initialised the file will contain a pickled dictionary with geometry parameters (e.g. type qpc, length device, width device, etc.). Whenever the geometry file is used for a simulation a `PescadoKwantProblem` (`qpc_exp2sim\pescado_kwant\pescado_kwant.py`) object might be added in the geometry file. This means that at that point the geometry file contains a pickled list with on position 0 a dictionary containing geometry parameters and on position 1 the corresponding `PescadoKwantProblem`. Whenever there's a `PescadoKwantProblem` stored away in the geometry file, the simulation will use that `PescadoKwantProblem`. This allows to reduce computation time whenever the same device should be simulated for different simulation parameters e.g. different gate voltages, dopant concentrations etc.

2. `qpc_exp2sim_data/simulation/files_sim` contains files describing the simulation parameters used for a certain simulation. These files are used to initiate a simulation (in combination with a geometry file). A simulation file can be created via the `simulation/configuration.py` python file in the the `QPC_Exp2Sim` repository. When initialised the file will contain a pickled dictionary with simulation parameters (e.g. the used gate voltage, dopant concentration, wheter iteration data should be stored etc.).

3. `qpc_exp2sim_data/simulation/files_data` contains files describing the results of simulations. These data files are a direct output of the simulations (if not disabled by the user). A data file contains a pickled `Data` (`qpc_exp2sim/tools/data_handling.py`) object that can be analysed using the Jupyter notebooks in this repository.  

### 2) Naming of the data files

The naming of the geometry and simulation files can be done arbitrarely by the user. At this point some conventions are used. 

**Geometry files** are in general named as follows: **(qpc type)(qpc number)_grid(grid size)**.
- qpc type: The type of the quantum point contact: 'a' or 'b'
- qpc number: The number of the quantum point contact: '1', '2', '3' or '4'. Increasing number indicates increasing size.
- grid size: The length of the side of a square unit cell in the used lattice expressed in nm.

**Simulation files** are in general named as follows: **V(n)_(method)**, however variations exist.
- n: The number of used gate voltages, sometimes this is accompanied by a qpc type and qpc number, e.g. 'a4'. In this case 'a4' indicates that the gate voltages are taken such that the results of the simulation are meaningfull for qpc 'a4'. More information on the meaningfull gate voltage intervals can be found in the QPC_Exp2Sim repository in `analysis/overview_v3_simexp.ods`.
- method: The method used for the simulation. 
	- 'lhh': Short for Linear HelmHoltz, for solving a classical Poisson Problem. 
	- 'pi_bulk': For working in the continuum limit, i.e. with the bulk density of states. 
	- 'nr_lat': For calculating the lattice LDOS at the center site and using this LDOS for all flexible sites to subsequently solve the problem using a Newton-Raphson solver. In this case extra information might be added in the file name in the form of 'm(a)_it(b)', where a indicates the number of moments used for the KPM calculations and b is totally meaningless. 
	- 'fsc_nr': For doing a full self consistent calculation implemented with the Newton-Raphson solver. In this case extra information might be added at the end of the file name in the form of 'm(a)_it(b)', where a indicates the number of moments used for the KPM calculations and b indicates the number of performed iterations in the full self-consistent loop. 


**Note on memory usage**: The biggest chunk of memory in the stored data is consumed by storing the LDOS (ExactLdos object) for each gate voltage in the self consistent quantum electrostatic frame. This is due to the fact that for each site the potential at which the LDOS is calculated and M moments are stored. When using 100 moments at 10.000 sites and 100 gate voltages this results in 10^9 stored values. 