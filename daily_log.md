# Daily log (extensive version)

## Week 2

### Friday 04/03/2022
- Implementation 3D - system (pescado 3D, kwant 2DEG 2D)
- Cuboid lattice is no problem anymore since 2DEG can be describred in 2D, no hoppings to GaAs/AlGaAs
- Update plotter functions such that they can handle 3D
- First results conductance (see jupyter notebook file data\_analysis/da\_system\_3D.ipynb

### Thursday 03/03/2022
- Pheliqs day
- Adaption shapes 3D system
- Creating easy interface for plotting data in ipynb using interact package
- Problems with cuboid lattice (e.g. [50, 50, 5]) ->defining hoppings and correct values

### Wednesday 02/03/2022
- Improved plotter functions 2D 
- Working on 3D_system

### Tuesday 01/03/2022
- Security day
- Meeting Xavier, discussion of first results conductances
- Cleaning up code 2D_system 

### Monday 28/01/2022
- First calculations conductance 2D_system ifo V_gates
- Start defining geometry 3D_system, some problems

## Week 1
### Friday 25/02/2022
- Coding 2D_system.py as try out for the kwant - pescado interface

### Thursday 25/02/2022
- Github/lab tutorial
- Setting up latex report to keep overview of information
- Rereading Kwant paper
- Kwant tutorials 2.1 - 2.3
- Following live webinar of Vlaams Supercomputer Center on Quantum Machine Learning

### Wednesday 25/02/2022
- Continue own try_out of pescado
- Asking Antonio pescado questions

### Tuesday 25/02/2022
- Discuss questions Eleni paper with Xavier
- Installing Kwant & Pescado
- Pescado tutorials sphere & nanowire
- Start own try out of pescado: pescado_try.py

### Monday 25/02/2022
- Get badge
- Subscription safety day (1st of March)
- Introduction to topic from Xavier
- Introduction to Pescado from Antonio
- Read Eleni paper
